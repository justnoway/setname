package com.example.project.setname.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.project.setname.MainActivity.MainActivity;
import com.example.project.setname.R;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class ActivitySetup extends AppCompatActivity {

    //views
    private EditText setupName;
    private Button setupBtn;
    private CircleImageView setupImage;

    //service
    private StorageReference storageReference;
    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore firebaseFirestore;
    private String user_id;

    //vars
    private Uri mainImageURI = null;
    private boolean isChanged = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        Log.i("ActivitySetupThis", FirebaseAuth.getInstance().getCurrentUser().getUid()+"");

        setFirebase();

        setViews();

        loadUserInf();

        setListeners();

    }

    private void setListeners() {

        setupBtn.setOnClickListener(v -> {

            setupBtn.setClickable(false);

            final String user_name = setupName.getText().toString();

            if (!TextUtils.isEmpty(user_name) && mainImageURI != null) {

                if (isChanged) {

                    user_id = firebaseAuth.getCurrentUser().getUid();

                    StorageReference image_path = storageReference.child("profile_images").child(user_id + ".jpg");
                    image_path.putFile(mainImageURI).addOnCompleteListener(task -> {

                        if (task.isSuccessful()) {

                            storeFirestore(task, user_name);

                        } else {

                            String error = task.getException().getMessage();
                            Toast.makeText(ActivitySetup.this, "(IMAGE Error) : " + error, Toast.LENGTH_LONG).show();

                        }
                    });

                } else {

                    storeFirestore(null, user_name);

                }

            }

            setupBtn.setClickable(true);

        });

        setupImage.setOnClickListener(v -> {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                if (ContextCompat.checkSelfPermission(ActivitySetup.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(ActivitySetup.this, "Permission Denied", Toast.LENGTH_LONG).show();
                    ActivityCompat.requestPermissions(ActivitySetup.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

                } else {

                    BringImagePicker();

                }

            } else {

                BringImagePicker();

            }

        });

    }

    private void loadUserInf() {

        firebaseFirestore.collection("Users").document(user_id).get().addOnCompleteListener(task -> {

            if (task.isSuccessful()) {

                if (task.getResult().exists()) {

                    String name = task.getResult().getString("name");
                    String image = task.getResult().getString("image");

                    mainImageURI = Uri.parse(image);

                    setupName.setText(name);

                    RequestOptions placeholderRequest = new RequestOptions();
                    placeholderRequest.placeholder(R.drawable.default_image);

                    Glide.with(ActivitySetup.this).setDefaultRequestOptions(placeholderRequest).load(image).into(setupImage);


                }

            } else {

                String error = task.getException().getMessage();
                Toast.makeText(ActivitySetup.this, "(FIRESTORE Retrieve Error) : " + error, Toast.LENGTH_LONG).show();

            }

            setupBtn.setEnabled(true);

        });

    }

    private void setViews() {

        setupImage = findViewById(R.id.setup_image);
        setupName = findViewById(R.id.setup_name);
        setupBtn = findViewById(R.id.setup_btn);

        setupBtn.setEnabled(false);

    }

    private void setFirebase() {

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();
        user_id = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

    }

    private void storeFirestore(@NonNull Task<UploadTask.TaskSnapshot> task, String user_name) {

        Uri download_uri;

        if (task != null) {

            download_uri = task.getResult().getDownloadUrl();

        } else {

            download_uri = mainImageURI;

        }

        String user_name_search = String.valueOf(user_name.toLowerCase().split(" "));

        firebaseFirestore.collection("Users")
                .whereEqualTo("name", user_name_search)
                .addSnapshotListener(((queryDocumentSnapshots, e) -> {

                    assert queryDocumentSnapshots != null;
                    if (!(queryDocumentSnapshots.getDocuments().size() > 0)) {

                        assert download_uri != null;
                        Map<String, String> userMap = new HashMap<>();
                        userMap.put("name", user_name_search);
                        userMap.put("image", download_uri.toString());

                        Map<String, Object> map = new HashMap<>();
                        map.put("wallet", "0");
                        map.put("rank", "Cleaner");
                        map.put("about", "Nothing");

                        firebaseFirestore.collection("Users")
                                .document(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                                .set(map).addOnCompleteListener(task1 -> {


                        });

                        firebaseFirestore.collection("Users").document(user_id).set(userMap).addOnCompleteListener(task1 -> {

                            if (task1.isSuccessful()) {

                                Toast.makeText(ActivitySetup.this, "The user Settings are updated.", Toast.LENGTH_LONG).show();
                                Intent mainIntent = new Intent(ActivitySetup.this, MainActivity.class);
                                startActivity(mainIntent);
                                finish();

                            } else {

                                String error = Objects.requireNonNull(task1.getException()).getMessage();
                                Toast.makeText(ActivitySetup.this, "(FIRESTORE Error) : " + error, Toast.LENGTH_LONG).show();

                            }

                        });

                    } else {

                        Toast.makeText(getApplicationContext(), "This name has already been used", Toast.LENGTH_SHORT).show();

                    }

                }));


    }

    private void BringImagePicker() {

        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .start(ActivitySetup.this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                mainImageURI = result.getUri();
                setupImage.setImageURI(mainImageURI);

                isChanged = true;

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();

            }
        }

    }
}
