package com.example.project.setname.Fragments.Jobs.ShowStatus;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.setname.Fragments.Jobs.Reconciliations.ModelJobReconciliation;
import com.example.project.setname.Interfaces.JobOpenChat;
import com.example.project.setname.Interfaces.GetFragment;
import com.example.project.setname.Interfaces.JobSendCode;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;
import java.util.List;

public class AdapterShowStatus extends RecyclerView.Adapter<AdapterShowStatus.ViewHolder> {

    private List<ModelShowStatus> status_list;
    public Context context;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    public AdapterShowStatus(List<ModelShowStatus> status_list) {
        this.status_list = status_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_show_status, parent, false);
        context = parent.getContext();
        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        String status_data = status_list.get(position).getText();
        holder.setStatus(status_data);

    }
    @Override
    public int getItemCount() {
        return status_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private View mView;

        private TextView statusTV;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        void setStatus(String str){
            statusTV = mView.findViewById(R.id.item_show_status_tv);
            statusTV.setText(str);
        }
    }
}
