package com.example.project.setname.Fragments.Jobs.Requests;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.example.project.setname.Interfaces.GetFragment;
import com.example.project.setname.Interfaces.JobAvailableOpenDialog;
import com.example.project.setname.Interfaces.JobDeleteItemFromList;
import com.example.project.setname.Interfaces.OpenProfileInfo;
import com.example.project.setname.Interfaces.RecyclerViewClickListener;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterRequests extends RecyclerView.Adapter<AdapterRequests.ViewHolder> {

    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();

    private List<ModelRequests> requests_list;
    public Context context;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    private GetFragment mGetFragment;
    private JobAvailableOpenDialog mJobAvailableOpenDialog;
    private OpenProfileInfo mOpenProfileInfo;
    private JobDeleteItemFromList mJobDeleteItemFromList;

    public AdapterRequests(List<ModelRequests> requests_list,
                           GetFragment GetFragment,
                           OpenProfileInfo openProfileInfo) {
        this.requests_list = requests_list;
        this.mGetFragment = GetFragment;
        this.mOpenProfileInfo = openProfileInfo;
    }

    public AdapterRequests(List<ModelRequests> requests_list,
                           GetFragment GetFragment,
                           JobAvailableOpenDialog jobAvailableOpenDialog,
                           OpenProfileInfo openProfileInfo,
                           JobDeleteItemFromList jobDeleteItemFromList) {
        this.requests_list = requests_list;
        this.mGetFragment = GetFragment;
        this.mJobAvailableOpenDialog = jobAvailableOpenDialog;
        this.mOpenProfileInfo = openProfileInfo;
        this.mJobDeleteItemFromList = jobDeleteItemFromList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_jobs_request, parent, false);
        context = parent.getContext();
        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        viewBinderHelper.setOpenOnlyOne(true);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.setSwipeRevealLayout();

        String maker = requests_list.get(position).getMaker();

        String current_user = firebaseAuth.getCurrentUser().getUid();

        String id_data = requests_list.get(position).ModelComeRequestId;

        String price_data = requests_list.get(position).getPrice();
        holder.setPrice(price_data);

        try {
            long millisecond = requests_list.get(position).getDeadline().getTime();
            String dateString = DateFormat.format("dd MMMM", new Date(millisecond)).toString();
            holder.setDeadline(dateString);
        } catch (Exception e) {
            Toast.makeText(context, "Exception : " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        if (mGetFragment.getCustomFragment().equals("FRAGMENT_JOB_FROM_EMPLOYER_COME_REQUESTS")){

            try{
                long millisecond = requests_list.get(position).getDeadline().getTime();
                String dateStringForDialog = DateFormat.format("MM/dd/yyyy", new Date(millisecond)).toString();
                if (mGetFragment.getCustomFragment().equals("FRAGMENT_JOB_FROM_EMPLOYER_COME_REQUESTS")) {

                    holder.setSwipeMenu();

                    if(isConnected(context.getApplicationContext())) {

                        holder.confirm.setOnClickListener(v -> {

                            mJobAvailableOpenDialog.openDialog(id_data, maker, price_data, dateStringForDialog, position);

                        });

                        holder.delete.setOnClickListener(v -> {

                            firebaseFirestore.collection("Users")
                                    .document(current_user)
                                    .collection("ReceivedRequestsForJob(Employer)")
                                    .document(id_data)
                                    .delete()
                                    .addOnCompleteListener(task -> {

                                    });

                            firebaseFirestore.collection("Users")
                                    .document(maker)
                                    .collection("SentRequestsForJob(Maker)")
                                    .document(id_data)
                                    .delete()
                                    .addOnCompleteListener(task -> {

                                    });

                            mJobDeleteItemFromList.deleteItemFromList(position);

                        });
                    }else {

                        holder.confirm.setOnClickListener(v -> {

                            Toast.makeText(context.getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();

                        });

                        holder.delete.setOnClickListener(v -> {

                            Toast.makeText(context.getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();

                        });

                    }

                }
            }
            catch (Exception ignored){

            }

            firebaseFirestore.collection("Users").document(maker).get().addOnCompleteListener(task -> {

                if (task.isSuccessful()) {

                    String userName = task.getResult().getString("name");
                    String userRank = task.getResult().getString("rank");
                    String userImage = task.getResult().getString("image");

                    holder.setUser(userName, userRank, userImage, maker);

                }
            });

        }
        else if (mGetFragment.getCustomFragment().equals("FRAGMENT_JOB_FROM_MAKER_SENT_REQUESTS")){

            holder.swipeRevealLayout.setLockDrag(true);

            firebaseFirestore.collection("Users").document(current_user).get().addOnCompleteListener(task -> {

                if (task.isSuccessful()) {

                    String userName = task.getResult().getString("name");
                    String userRank = task.getResult().getString("rank");
                    String userImage = task.getResult().getString("image");

                    holder.setUser(userName, userRank, userImage);

                }
            });


        }



    }

    public static boolean isConnected(@NonNull Context context) {
        ConnectivityManager
                cm = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public int getItemCount() {
        return requests_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private View mView;

        private SwipeRevealLayout swipeRevealLayout;

        private CircleImageView userImage;
        private TextView username;
        private TextView userRank;

        private TextView price;
        private TextView deadline;

        private View confirm;
        private View delete;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        void setUser(String name, String rank, String imageURL, String userForProfileInfo){
            username = mView.findViewById(R.id.item_jobs_request_username);
            username.setText(name);
            username.setOnClickListener(v->{
                mOpenProfileInfo.openProfile(userForProfileInfo);
            });
            userRank = mView.findViewById(R.id.item_jobs_request_rank);
            userRank.setText(rank);
            userRank.setOnClickListener(v->{
                mOpenProfileInfo.openProfile(userForProfileInfo);
            });
            userImage = mView.findViewById(R.id.item_jobs_request_circleImageView);
            userImage.setOnClickListener(v->{
                mOpenProfileInfo.openProfile(userForProfileInfo);
            });
            RequestOptions placeholderOption = new RequestOptions();
            placeholderOption.placeholder(R.drawable.profile_placeholder);
            Glide.with(context).applyDefaultRequestOptions(placeholderOption).load(imageURL).into(userImage);
        }

        void setUser(String name, String rank, String imageURL){
            username = mView.findViewById(R.id.item_jobs_request_username);
            username.setText(name);
            userRank = mView.findViewById(R.id.item_jobs_request_rank);
            userRank.setText(rank);
            userImage = mView.findViewById(R.id.item_jobs_request_circleImageView);
            RequestOptions placeholderOption = new RequestOptions();
            placeholderOption.placeholder(R.drawable.profile_placeholder);
            Glide.with(context).applyDefaultRequestOptions(placeholderOption).load(imageURL).into(userImage);
        }

        void setPrice(String str){
            price = mView.findViewById(R.id.item_jobs_request_price);
            price.setText(str);
        }

        void setDeadline(String str){
            deadline = mView.findViewById(R.id.item_jobs_request_deadline);
            deadline.setText(str);
        }

        void setSwipeMenu(){

            confirm = mView.findViewById(R.id.item_jobs_request_view_confirm);
            delete = mView.findViewById(R.id.item_jobs_request_view_delete);

        }

        void setSwipeRevealLayout(){

            swipeRevealLayout = mView.findViewById(R.id.item_jobs_request_srl);
            swipeRevealLayout.close(false);

        }

    }
}
