package com.example.project.setname.Fragments.Jobs.FragmentJobAvailable;

import android.support.annotation.NonNull;

import com.google.firebase.firestore.Exclude;

public class ModelJobAvailableId {

    @Exclude
    public String ConstructorJobAvaiableId;

    public <T extends ModelJobAvailableId> T withId(@NonNull final String id){
        this.ConstructorJobAvaiableId = id;
        return (T) this;
    }

}
