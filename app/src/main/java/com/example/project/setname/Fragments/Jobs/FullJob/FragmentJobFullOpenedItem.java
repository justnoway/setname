package com.example.project.setname.Fragments.Jobs.FullJob;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.text.method.KeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.dpizarro.uipicker.library.picker.PickerUI;
import com.dpizarro.uipicker.library.picker.PickerUISettings;
import com.example.project.setname.Dialogs.DialogDatePicker;
import com.example.project.setname.Dialogs.DialogSendCode;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class FragmentJobFullOpenedItem extends Fragment {

    private static final String TAG = "FragmentJFOI";

    /**
     * Пояснение логики:
     * <p>
     * Статичные переменные date, strDeadline, dataTypeOfJob нужны для хранения данных для сервера
     * То есть если у меня есть TypeOfJob - FixPrice, то данные я буду брать не из TextView, а из dataTypeOfJob чтобы не нарушать логику БД
     * <p>
     * fromMaker, fromEmployer - сравнение согласований
     * <p>
     * fragmentTag - текущий открытый фрагмент
     * <p>
     * options - списки из папки arrays
     * <p>
     * typeOfJobPosition - позиция в виджете
     */

    //service
    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    //views
    private View view;

    private SwipeRevealLayout swipeRevealLayout;
    private View acceptView;
    private View dismissView;
    private View editView;
    private TextView editView_text;

    private View colorView;

    private TextView typeOfJob;
    private EditText price;
    private TextView deadline;
    private EditText description;
    private PickerUI mPickerUI;

    //var
    public static AtomicReference<Date> date = new AtomicReference<>();//для отправки на сервер
    public static AtomicReference<String> strDeadline = new AtomicReference<>();//для DatePicker
    private static AtomicReference<String> dataTypeOfJob = new AtomicReference<>();//для typeOfJob

    private AtomicReference<String> strDeadlineFJFOI = new AtomicReference<>();//для сравнения
    private AtomicReference<Object> fromMaker = new AtomicReference<>();//для сравнения
    private AtomicReference<Object> fromEmployer = new AtomicReference<>();//для сравнения

    private List<String> options;

    private String id;
    private String companion;
    private String fragmentTag;
    private String currentUser;

    private boolean confirmSend = false;

    private int count = 0;
    private int typeOfJobPosition = 0;

    @SuppressLint("CommitTransaction")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_job_full_rec_and_mak_item, container, false);

        setViews();

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        typeOfJob.setClickable(false);
        price.setTag(price.getKeyListener());
        price.setKeyListener(null);
        deadline.setTag(deadline.getKeyListener());
        deadline.setKeyListener(null);
        description.setTag(description.getKeyListener());
        description.setKeyListener(null);

        assert getArguments() != null;
        id = getArguments().getString("id");
        companion = getArguments().getString("companion");
        fragmentTag = getArguments().getString("fragmentTag");

        currentUser = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        assert fragmentTag != null;
        assert companion != null;
        assert id != null;


        if (firebaseAuth.getCurrentUser() != null) {

            firebaseFirestore.collection("Jobs")
                    .document(id)
                    .get().addOnCompleteListener(task -> {

                switch (Objects.requireNonNull(task.getResult().getString("language"))) {
                    case "Java":
                        colorView.setBackgroundColor(Color.parseColor("#E91E63"));
                        break;
                    case "Swift":
                        colorView.setBackgroundColor(Color.parseColor("#FF8A65"));
                        break;
                    case "Python":
                        colorView.setBackgroundColor(Color.parseColor("#03A9F4"));
                        break;
                    case "Web":
                        colorView.setBackgroundColor(Color.parseColor("#BA68C8"));
                        break;
                    case "Mobile":
                        colorView.setBackgroundColor(Color.parseColor("#4DB6AC"));
                        break;
                }

            });


            if (fragmentTag.equals("FRAGMENT_FROM_MAKER_RECONCILIATIONS")) {

                setClickListeners("FRAGMENT_FROM_MAKER_RECONCILIATIONS");

                firebaseFirestore.collection("Users")
                        .document(currentUser)
                        .collection("Reconciliations(Maker)")
                        .document(id)
                        .get().addOnCompleteListener(task -> {

                    if (task.isComplete()) {

                        Map<String, Object> map = task.getResult().getData();
                        map.remove("confirm");
                        map.put("moneyWereSend", false);
                        fromMaker.set(map);

                        price.setText(task.getResult().getString("price"));
                        description.setText(task.getResult().getString("mustToDo"));
                        dataTypeOfJob.set(task.getResult().getString("type"));
                        switch (task.getResult().getString("type")) {
                            case "p/h":
                                typeOfJob.setText("Per hour");
                                break;
                            case "fix":
                                typeOfJob.setText("Per hour");
                                break;
                            case "trade":
                                typeOfJob.setText("Exchange(beta)");
                                break;
                        }

                        try {
                            long millisecond = Objects.requireNonNull(task.getResult().getDate("deadline")).getTime();
                            String dateStringForDialog = DateFormat.format("MMMM dd", new Date(millisecond)).toString();
                            deadline.setText(dateStringForDialog);

                            long millisecond1 = Objects.requireNonNull(task.getResult().getDate("deadline")).getTime();
                            String dateStringForDialog1 = DateFormat.format("dd MM yyyy", new Date(millisecond1)).toString();
                            strDeadline.set(dateStringForDialog1);
                        } catch (Exception ignored) {

                        }

                        if (!task.getResult().getBoolean("confirm")) {

                            firebaseFirestore.collection("Users")
                                    .document(companion)
                                    .collection("Reconciliations(Employer)")
                                    .document(id)
                                    .get().addOnCompleteListener(task1 -> {

                                if (task1.isComplete()) {

                                    Map<String, Object> map1 = task1.getResult().getData();
                                    assert map1 != null;
                                    map1.remove("confirm");
                                    map1.put("moneyWereSend", false);
                                    fromEmployer.set(map1);
                                }
                            });

                            acceptView.setOnClickListener(v -> {

                                acceptView.setClickable(false);
                                dismissView.setClickable(false);

                                try {
                                    firebaseFirestore.collection("Users")
                                            .document(currentUser)
                                            .collection("Reconciliations(Maker)")
                                            .document(id).addSnapshotListener((documentSnapshot, e) -> {

                                        assert documentSnapshot != null;
                                        if (documentSnapshot.exists()) {

                                            if (documentSnapshot.getBoolean("confirm")) {

                                                checkFromMaker();

                                            } else {

                                                HashMap<String, Object> mp = new HashMap<>();
                                                mp.put("confirm", true);

                                                firebaseFirestore.collection("Users")
                                                        .document(currentUser)
                                                        .collection("Reconciliations(Maker)")
                                                        .document(id).update(mp).addOnCompleteListener(task1 -> {

                                                    if (task1.isComplete()) {

                                                        Toast.makeText(getContext(), "Successful send", Toast.LENGTH_SHORT).show();

                                                        checkFromMaker();

                                                        Map<String, Object> mapForStatus = new HashMap<>();
                                                        mapForStatus.put("timestamp", FieldValue.serverTimestamp());
                                                        mapForStatus.put("text", "Accept from maker");

                                                        firebaseFirestore.collection("Users")
                                                                .document(currentUser)
                                                                .collection("Reconciliations(Maker)")
                                                                .document(id)
                                                                .collection("Status")
                                                                .add(mapForStatus).addOnCompleteListener(task2 -> {

                                                        });

                                                        firebaseFirestore.collection("Users")
                                                                .document(companion)
                                                                .collection("Reconciliations(Employer)")
                                                                .document(id)
                                                                .collection("Status")
                                                                .add(mapForStatus).addOnCompleteListener(task2 -> {

                                                        });

                                                    } else {

                                                        acceptView.setClickable(true);
                                                        dismissView.setClickable(true);

                                                    }


                                                });

                                            }
                                        }

                                    });
                                } catch (NullPointerException ignored) {
                                }


                            });

                            dismissView.setOnClickListener(v -> {

                                dismissView.setClickable(false);

                                if (count == 3) {

                                    deleteDataFromMaker();
                                    Toast.makeText(getContext(), "Бывает ¯\\_(ツ)_/¯", Toast.LENGTH_SHORT).show();

                                    count = 0;

                                } else {

                                    count++;

                                    Toast.makeText(getContext(), "Уверен ?", Toast.LENGTH_SHORT).show();
                                }

                                dismissView.setClickable(true);

                            });

                        }
                    }

                });

            } else if (fragmentTag.equals("FRAGMENT_FROM_EMPLOYER_RECONCILIATIONS")) {

                setClickListeners("FRAGMENT_FROM_EMPLOYER_RECONCILIATIONS");

                firebaseFirestore.collection("Users")
                        .document(currentUser)
                        .collection("Reconciliations(Employer)")
                        .document(id)
                        .get().addOnCompleteListener(task -> {

                    if (task.isComplete()) {

                        Map<String, Object> map = task.getResult().getData();
                        map.remove("confirm");
                        map.put("moneyWereSend", false);
                        fromEmployer.set(map);

                        price.setText(task.getResult().getString("price"));
                        description.setText(task.getResult().getString("mustToDo"));
                        dataTypeOfJob.set(task.getResult().getString("type"));
                        switch (Objects.requireNonNull(task.getResult().getString("type"))) {
                            case "p/h":
                                typeOfJob.setText("Per hour");
                                break;
                            case "fix":
                                typeOfJob.setText("Fixed price");
                                break;
                            case "trade":
                                typeOfJob.setText("Exchange(beta)");
                                break;
                        }

                        try {

                            long millisecond = Objects.requireNonNull(task.getResult().getDate("deadline")).getTime();
                            String dateStringForDialog = DateFormat.format("MMMM dd", new Date(millisecond)).toString();
                            deadline.setText(dateStringForDialog);

                            long millisecond1 = Objects.requireNonNull(task.getResult().getDate("deadline")).getTime();
                            String dateStringForDialog1 = DateFormat.format("dd MM yyyy", new Date(millisecond1)).toString();
                            strDeadline.set(dateStringForDialog1);

                        } catch (Exception e1) {

                        }

                        if (!task.getResult().getBoolean("confirm")) {

                            firebaseFirestore.collection("Users")
                                    .document(companion)
                                    .collection("Reconciliations(Maker)")
                                    .document(id)
                                    .get().addOnCompleteListener(task1 -> {

                                if (task1.isComplete()) {

                                    Map<String, Object> map1 = task1.getResult().getData();
                                    assert map1 != null;
                                    map1.remove("confirm");
                                    map1.put("moneyWereSend", false);
                                    fromMaker.set(map1);
                                }
                            });

                            acceptView.setOnClickListener(v -> {

                                acceptView.setClickable(false);
                                dismissView.setClickable(false);

                                firebaseFirestore.collection("Users")
                                        .document(currentUser)
                                        .collection("Reconciliations(Employer)")
                                        .document(id).get().addOnSuccessListener(documentSnapshot -> {

                                    if (documentSnapshot.exists()) {

                                        if (documentSnapshot.getBoolean("confirm")) {

                                            checkFromEmployer();

                                        } else {

                                            HashMap<String, Object> mp = new HashMap<>();
                                            mp.put("confirm", true);

                                            firebaseFirestore.collection("Users")
                                                    .document(currentUser)
                                                    .collection("Reconciliations(Employer)")
                                                    .document(id).update(mp).addOnCompleteListener(task1 -> {

                                                if (task1.isComplete()) {
                                                    Toast.makeText(getContext(), "Successful send", Toast.LENGTH_SHORT).show();

                                                    checkFromEmployer();

                                                    Map<String, Object> mapForStatus = new HashMap<>();
                                                    mapForStatus.put("timestamp", FieldValue.serverTimestamp());
                                                    mapForStatus.put("text", "Accept from employer");

                                                    firebaseFirestore.collection("Users")
                                                            .document(companion)
                                                            .collection("Reconciliations(Maker)")
                                                            .document(id)
                                                            .collection("Status")
                                                            .add(mapForStatus).addOnCompleteListener(task2 -> {


                                                    });

                                                    firebaseFirestore.collection("Users")
                                                            .document(currentUser)
                                                            .collection("Reconciliations(Employer)")
                                                            .document(id)
                                                            .collection("Status")
                                                            .add(mapForStatus).addOnCompleteListener(task2 -> {


                                                    });

                                                } else {

                                                    acceptView.setClickable(true);
                                                    dismissView.setClickable(true);

                                                }


                                            });

                                        }
                                    }

                                });


                            });

                            dismissView.setOnClickListener(v -> {

                                dismissView.setClickable(false);

                                if (count == 3) {

                                    deleteDataFromEmployer();
                                    Toast.makeText(getContext(), "Бывает ¯\\_(ツ)_/¯", Toast.LENGTH_SHORT).show();

                                    count = 0;

                                } else {

                                    count++;

                                    Toast.makeText(getContext(), "Уверен ?", Toast.LENGTH_SHORT).show();


                                }

                                dismissView.setClickable(true);

                            });

                        }
                    }

                });

            } else if (fragmentTag.equals("FRAGMENT_FROM_MAKER_MAKING")) {

                setSwipeMaking();

                acceptView.setOnClickListener(v -> {

                    DialogSendCode dialogSendCode = new DialogSendCode();
                    Bundle bundle = new Bundle();
                    bundle.putString("companion", companion);
                    bundle.putString("id", id);
                    dialogSendCode.setArguments(bundle);
                    assert getFragmentManager() != null;
                    dialogSendCode.show(getFragmentManager(), "DIALOG_SEND_CODE");


                });

                firebaseFirestore.collection("Users")
                        .document(currentUser)
                        .collection("Making(Maker)")
                        .document(id)
                        .get().addOnCompleteListener(task -> {

                    if (task.isComplete()) {

                        price.setText(task.getResult().getString("price"));
                        description.setText(task.getResult().getString("mustToDo"));

                        try {

                            long millisecond = Objects.requireNonNull(task.getResult().getDate("deadline")).getTime();
                            String dateStringForDialog = DateFormat.format("dd MMMM", new Date(millisecond)).toString();
                            deadline.setText(dateStringForDialog);

                        } catch (Exception e1) {

                        }
                    }

                });


            } else if (fragmentTag.equals("FRAGMENT_FROM_EMPLOYER_WAITING_OF_MAKING")) {

                setSwipeMakingEmp();

                firebaseFirestore.collection("Users")
                        .document(currentUser)
                        .collection("WaitingOfMaking(Employer)")
                        .document(id)
                        .get().addOnCompleteListener(task -> {

                    if (task.isComplete()) {

                        price.setText(task.getResult().getString("price"));
                        description.setText(task.getResult().getString("mustToDo"));

                        try {

                            long millisecond = Objects.requireNonNull(task.getResult().getDate("deadline")).getTime();
                            String dateStringForDialog = DateFormat.format("dd MMMM", new Date(millisecond)).toString();
                            deadline.setText(dateStringForDialog);

                        } catch (Exception ignored) {
                        }
                    }

                    acceptView.setOnClickListener(v -> {

                        acceptView.setClickable(false);
                        dismissView.setClickable(false);

                        firebaseFirestore.collection("Users")
                                .document(currentUser)
                                .collection("Making(Maker)")
                                .document(id)
                                .get().addOnCompleteListener(task5 -> {

                            if (task5.isComplete()) {

                                if (task5.getResult().getBoolean("moneyWereSend")) {

                                    if (confirmSend) {

                                        firebaseFirestore.collection("Users")
                                                .document(companion).get().addOnCompleteListener(task1 -> {

                                            if (task1.isComplete()) {

                                                firebaseFirestore.collection("Users")
                                                        .document(companion).update("wallet", String.valueOf((Integer.parseInt(task.getResult().getString("price"))
                                                        + Integer.parseInt(task1.getResult().getString("wallet"))))).addOnCompleteListener(task2 -> {

                                                    if (task2.isComplete()) {

                                                        firebaseFirestore.collection("Users")
                                                                .document(currentUser).update("wallet", String.valueOf(Integer.parseInt(task1.getResult().getString("wallet")) -
                                                                (Integer.parseInt(task.getResult().getString("price"))))).addOnCompleteListener(task7 -> {

                                                            if (task7.isComplete()) {

                                                                firebaseFirestore.collection("Users")
                                                                        .document(companion)
                                                                        .collection("Making(Maker)")
                                                                        .document(id)
                                                                        .collection("CodeList")
                                                                        .addSnapshotListener((documentSnapshots, e) -> {

                                                                            assert documentSnapshots != null;
                                                                            if (!documentSnapshots.isEmpty()) {

                                                                                for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {

                                                                                    Map<String, Object> map1 = new HashMap<>();
                                                                                    map1.put("link", doc.getDocument().getString("link"));
                                                                                    map1.put("timestamp", doc.getDocument().getDate("timestamp"));

                                                                                    firebaseFirestore.collection("Users")
                                                                                            .document(currentUser)
                                                                                            .collection("Complete(Employer)")
                                                                                            .document(id)
                                                                                            .collection("CodeList")
                                                                                            .document(doc.getDocument().getId())
                                                                                            .set(map1).addOnCompleteListener(task3 -> {

                                                                                    });

                                                                                    firebaseFirestore.collection("Users")
                                                                                            .document(companion)
                                                                                            .collection("Complete(Maker)")
                                                                                            .document(id)
                                                                                            .collection("CodeList")
                                                                                            .document(doc.getDocument().getId())
                                                                                            .set(map1).addOnCompleteListener(task3 -> {

                                                                                    });

                                                                                }
                                                                            }

                                                                        });

                                                                Map<String, Object> map1 = new HashMap<>();
                                                                map1.put("language", task.getResult().getString("language"));
                                                                map1.put("maker_id", task.getResult().getString("maker_id"));
                                                                map1.put("mustToDo", task.getResult().getString("mustToDo"));
                                                                map1.put("price", task.getResult().getString("price"));
                                                                map1.put("deadline", task.getResult().getDate("deadline"));

                                                                firebaseFirestore.collection("Users")
                                                                        .document(currentUser)
                                                                        .collection("Complete(Employer)")
                                                                        .document(id)
                                                                        .set(map1).addOnCompleteListener(task3 -> {

                                                                });

                                                                firebaseFirestore.collection("Users")
                                                                        .document(companion)
                                                                        .collection("Complete(Maker)")
                                                                        .document(id)
                                                                        .set(map1).addOnCompleteListener(task3 -> {

                                                                });

                                                                firebaseFirestore.collection("Users")
                                                                        .document(currentUser)
                                                                        .collection("WaitingOfMaking(Employer)")
                                                                        .document(id)
                                                                        .delete().addOnCompleteListener(task3 -> {

                                                                });

                                                                firebaseFirestore.collection("Users")
                                                                        .document(companion)
                                                                        .collection("Making(Maker)")
                                                                        .document(id)
                                                                        .delete().addOnCompleteListener(task3 -> {
                                                                });

                                                                Toast.makeText(getContext(), "Complete send money for employer", Toast.LENGTH_SHORT).show();


                                                            } else {

                                                                Toast.makeText(getContext(), "Some error\n+Try again late", Toast.LENGTH_SHORT).show();

                                                            }

                                                        });
                                                    }

                                                });

                                            }


                                        });
                                    } else {
                                        acceptView.setClickable(true);
                                        confirmSend = true;
                                        Toast.makeText(getContext(), "Press again to confirm the deal", Toast.LENGTH_SHORT).show();
                                    }

                                } else {

                                    Toast.makeText(getContext(), "Send money", Toast.LENGTH_SHORT).show();

                                }
                            }
                        });

                    });

                });


            }
        }

        return view;
    }

    private void setSwipeMaking() {

        editView.setVisibility(View.GONE);
        TextView tv = view.findViewById(R.id.fragment_job_bull_rec_and_mak_item_edit_tv);
        tv.setVisibility(View.GONE);

        TextView hideIt = view.findViewById(R.id.fragment_job_bull_rec_and_mak_item_dismiss_tv);
        hideIt.setVisibility(View.GONE);

        View hideitView = view.findViewById(R.id.fragment_job_bull_rec_and_mak_item_dismiss_v);
        hideitView.setVisibility(View.GONE);

        TextView acceptSendCode = view.findViewById(R.id.fragment_job_bull_rec_and_mak_item_accept_tv);
        acceptSendCode.setText("Send code");


    }

    private void setSwipeMakingEmp() {

        editView.setVisibility(View.GONE);
        TextView tv = view.findViewById(R.id.fragment_job_bull_rec_and_mak_item_edit_tv);
        tv.setVisibility(View.GONE);

        TextView sendMoney = view.findViewById(R.id.fragment_job_bull_rec_and_mak_item_dismiss_tv);
        sendMoney.setText("SendMoney");

        AtomicInteger count = new AtomicInteger();

        View sendMoneyView = view.findViewById(R.id.fragment_job_bull_rec_and_mak_item_dismiss_v);
        sendMoneyView.setOnClickListener(v -> {

            if (count.get() != 3) {

                Toast.makeText(getContext(), "Press " + (3 - count.get()) + " more times to emulate money sending", Toast.LENGTH_SHORT).show();

                count.getAndIncrement();

            } else {

                firebaseFirestore.collection("Users")
                        .document(currentUser)
                        .collection("WaitingOfMaking(Employer)")
                        .document(id).update("moneyWereSend", true)
                        .addOnCompleteListener(task -> {


                        });

                firebaseFirestore.collection("Users")
                        .document(companion)
                        .collection("Making(Maker)")
                        .document(id).update("moneyWereSend", true)
                        .addOnCompleteListener(task -> {


                        });

                Map<String, Object> map = new HashMap<>();
                map.put("text", "Money were send by employer");
                map.put("timestamp", FieldValue.serverTimestamp());

                firebaseFirestore.collection("Users")
                        .document(companion)
                        .collection("Making(Maker)")
                        .document(id)
                        .collection("Status")
                        .add(map).addOnCompleteListener(task -> {});

                firebaseFirestore.collection("Users")
                        .document(currentUser)
                        .collection("WaitingOfMaking(Employer)")
                        .document(id)
                        .collection("Status")
                        .add(map).addOnCompleteListener(task -> {});

                Toast.makeText(getContext(), "Money were send", Toast.LENGTH_SHORT).show();

            }

        });

    }

    private void setClickListeners(String fragmentTag) {

        AtomicReference<String> strTypeOfJob = new AtomicReference<>();
        AtomicReference<String> strCosts = new AtomicReference<>();
        AtomicReference<String> strDescription = new AtomicReference<>();

        editView_text = view.findViewById(R.id.fragment_job_bull_rec_and_mak_item_edit_tv);

        editView.setOnClickListener(v -> {

            if (editView_text.getText().toString().equals("Edit")) {

                acceptView.setClickable(false);
                dismissView.setClickable(false);

                typeOfJob.setOnClickListener(v1 -> {

                    options = Arrays.asList(getResources().getStringArray(R.array.type_of_job_rec));

                    PickerUISettings pickerUISettings = new PickerUISettings.Builder()
                            .withItems(options)
                            .withAutoDismiss(true)
                            .withItemsClickables(false)
                            .withUseBlur(false)
                            .build();

                    mPickerUI.setSettings(pickerUISettings);

                    mPickerUI.slide(typeOfJobPosition);

                    mPickerUI.setOnClickItemPickerUIListener((which, position, valueResult) -> {

                        typeOfJob.setText(valueResult);

                        switch (valueResult) {
                            case "Per hour":
                                dataTypeOfJob.set("p/h");
                                break;
                            case "Fixed price":
                                dataTypeOfJob.set("fix");
                                break;
                        }

                        typeOfJobPosition = position;

                    });


                });

                strCosts.set(price.getText().toString());
                strDescription.set(description.getText().toString());
                strTypeOfJob.set(typeOfJob.getText().toString());
                strDeadlineFJFOI.set(deadline.getText().toString());

                typeOfJob.setClickable(true);
                price.setKeyListener((KeyListener) price.getTag());
                deadline.setKeyListener((KeyListener) deadline.getTag());
                description.setKeyListener((KeyListener) description.getTag());

                deadline.setOnClickListener(v1 -> {

                    DialogDatePicker dateDialog = new DialogDatePicker();
                    Bundle bundle = new Bundle();
                    bundle.putString("view", "FRAGMENT_JOB_FULL_OPENED_ITEM");
                    dateDialog.setArguments(bundle);
                    dateDialog.show(getFragmentManager(), "DATA_PICKER");

                });


                editView_text.setText("Confirm");

            } else {

                typeOfJob.setClickable(false);
                price.setTag(price.getKeyListener());
                price.setKeyListener(null);
                deadline.setTag(deadline.getKeyListener());
                deadline.setKeyListener(null);
                description.setTag(description.getKeyListener());
                description.setKeyListener(null);

                if (customEquals(strTypeOfJob, typeOfJob.getText().toString()) &&
                        customEquals(strCosts, price.getText().toString()) &&
                        customEquals(strDeadlineFJFOI, deadline.getText().toString()) &&
                        customEquals(strDescription, description.getText().toString())) {

                    Toast.makeText(getContext(), "No edition", Toast.LENGTH_SHORT).show();

                } else {

                    Map<String, Object> map = new HashMap<>();
                    map.put("confirm", false);
                    if (!customEquals(strTypeOfJob, typeOfJob.getText().toString())) {
                        map.put("type", dataTypeOfJob.get());
                    }
                    if (!customEquals(strDeadlineFJFOI, deadline.getText().toString())) {
                        map.put("deadline", date.get());
                    }
                    if (!customEquals(strCosts, price.getText().toString())) {
                        map.put("price", price.getText().toString());
                    }
                    if (!customEquals(strDescription, description.getText().toString())) {
                        map.put("mustToDo", description.getText().toString());
                    }

                    if (fragmentTag.equals("FRAGMENT_FROM_EMPLOYER_RECONCILIATIONS")) {

                        firebaseFirestore.collection("Users")
                                .document(currentUser)
                                .collection("Reconciliations(Employer)")
                                .document(id)
                                .update(map).addOnCompleteListener(task -> {
                        });

                        firebaseFirestore.collection("Users")
                                .document(companion)
                                .collection("Reconciliations(Maker)")
                                .document(id)
                                .update(map).addOnCompleteListener(task -> {
                        });

                        Map<String, Object> mapForStatus = new HashMap<>();
                        mapForStatus.put("timestamp", FieldValue.serverTimestamp());
                        mapForStatus.put("text", "Edited from employer");

                        firebaseFirestore.collection("Users")
                                .document(companion)
                                .collection("Reconciliations(Maker)")
                                .document(id)
                                .collection("Status")
                                .add(mapForStatus).addOnCompleteListener(task2 -> {


                        });

                        firebaseFirestore.collection("Users")
                                .document(currentUser)
                                .collection("Reconciliations(Employer)")
                                .document(id)
                                .collection("Status")
                                .add(mapForStatus).addOnCompleteListener(task2 -> {


                        });


                    } else if (fragmentTag.equals("FRAGMENT_FROM_MAKER_RECONCILIATIONS")) {


                        firebaseFirestore.collection("Users")
                                .document(currentUser)
                                .collection("Reconciliations(Maker)")
                                .document(id)
                                .update(map).addOnCompleteListener(task -> {
                        });

                        firebaseFirestore.collection("Users")
                                .document(companion)
                                .collection("Reconciliations(Employer)")
                                .document(id)
                                .update(map).addOnCompleteListener(task -> {
                        });

                        Map<String, Object> mapForStatus = new HashMap<>();
                        mapForStatus.put("timestamp", FieldValue.serverTimestamp());
                        mapForStatus.put("text", "Edited from maker");

                        firebaseFirestore.collection("Users")
                                .document(currentUser)
                                .collection("Reconciliations(Maker)")
                                .document(id)
                                .collection("Status")
                                .add(mapForStatus).addOnCompleteListener(task2 -> {


                        });

                        firebaseFirestore.collection("Users")
                                .document(companion)
                                .collection("Reconciliations(Employer)")
                                .document(id)
                                .collection("Status")
                                .add(mapForStatus).addOnCompleteListener(task2 -> {


                        });

                    }

                    Toast.makeText(getContext(), "Edition", Toast.LENGTH_SHORT).show();

                }

                editView_text.setText("Edit");

                acceptView.setClickable(true);
                dismissView.setClickable(true);

            }

        });

    }

    boolean customEquals(AtomicReference<String> atomicReference, String string) {

        return atomicReference.get().equals(string);

    }

    private void setViews() {

        mPickerUI = (PickerUI) view.findViewById(R.id.picker_ui_view_rec_and_mak);

        colorView = view.findViewById(R.id.fragment_job_full_rec_and_mak_item_color_view);

        acceptView = view.findViewById(R.id.fragment_job_bull_rec_and_mak_item_accept_v);
        dismissView = view.findViewById(R.id.fragment_job_bull_rec_and_mak_item_dismiss_v);
        editView = view.findViewById(R.id.fragment_job_bull_rec_and_mak_item_edit_v);

        typeOfJob = view.findViewById(R.id.fragment_job_full_rec_and_mak_item_type_of_job);
        price = view.findViewById(R.id.fragment_job_full_rec_and_mak_item_price);
        deadline = view.findViewById(R.id.fragment_job_full_rec_and_mak_item_deadline);
        description = view.findViewById(R.id.fragment_job_full_rec_and_mak_item_descrition);

        swipeRevealLayout = view.findViewById(R.id.fragment_job_full_rec_and_mak_item_srl);

    }

    private void checkFromEmployer() {

        firebaseFirestore.collection("Users")
                .document(companion)
                .collection("Reconciliations(Maker)")
                .document(id).get().addOnSuccessListener(documentSnapshot1 -> {

            if (documentSnapshot1.exists()) {

                if (documentSnapshot1.getBoolean("confirm")) {

                    updateDataFromEmployer();

                } else {

                    Toast.makeText(getContext(), "Maker not confirm you request", Toast.LENGTH_SHORT).show();

                }

            }

        });

    }

    private void checkFromMaker() {

        firebaseFirestore.collection("Users")
                .document(companion)
                .collection("Reconciliations(Employer)")
                .document(id).get().addOnSuccessListener(documentSnapshot1 -> {

            if (documentSnapshot1.exists()) {

                if (documentSnapshot1.getBoolean("confirm")) {

                    updateDataFromMaker();

                }

            }
        });

    }

    void updateDataFromEmployer() {

        deleteDataFromEmployer();

        firebaseFirestore.collection("Users")
                .document(companion)
                .collection("Making(Maker)")
                .document(id)
                .set(fromMaker.get()).addOnCompleteListener(task -> {
        });

        firebaseFirestore.collection("Users")
                .document(currentUser)
                .collection("WaitingOfMaking(Employer)")
                .document(id)
                .set(fromEmployer.get()).addOnCompleteListener(task -> {
        });

        setDisable();

        Map<String, Object> map = new HashMap<>();
        map.put("text", "Start making");
        map.put("timestamp", FieldValue.serverTimestamp());

        firebaseFirestore.collection("Users")
                .document(companion)
                .collection("Making(Maker)")
                .document(id)
                .collection("Status")
                .add(map).addOnCompleteListener(task -> {});

        firebaseFirestore.collection("Users")
                .document(currentUser)
                .collection("WaitingOfMaking(Employer)")
                .document(id)
                .collection("Status")
                .add(map).addOnCompleteListener(task -> {});

    }

    void deleteDataFromEmployer() {

        firebaseFirestore.collection("Users")
                .document(companion)
                .collection("Reconciliations(Maker)")
                .document(id)
                .delete().addOnCompleteListener(task -> {
        });

        firebaseFirestore.collection("Users")
                .document(currentUser)
                .collection("Reconciliations(Employer)")
                .document(id)
                .delete().addOnCompleteListener(task -> {
        });

    }

    void updateDataFromMaker() {

        deleteDataFromMaker();

        firebaseFirestore.collection("Users")
                .document(currentUser)
                .collection("Making(Maker)")
                .document(id)
                .set(fromMaker.get()).addOnCompleteListener(task -> {
        });

        firebaseFirestore.collection("Users")
                .document(companion)
                .collection("WaitingOfMaking(Employer)")
                .document(id)
                .set(fromEmployer.get()).addOnCompleteListener(task -> {
        });

        setDisable();

        Map<String, Object> map = new HashMap<>();
        map.put("text", "Start making");
        map.put("timestamp", FieldValue.serverTimestamp());

        firebaseFirestore.collection("Users")
                .document(currentUser)
                .collection("Making(Maker)")
                .document(id)
                .collection("Status")
                .add(map).addOnCompleteListener(task -> {});

        firebaseFirestore.collection("Users")
                .document(companion)
                .collection("WaitingOfMaking(Employer)")
                .document(id)
                .collection("Status")
                .add(map).addOnCompleteListener(task -> {});


    }

    void deleteDataFromMaker() {

        firebaseFirestore.collection("Users")
                .document(currentUser)
                .collection("Reconciliations(Maker)")
                .document(id)
                .delete().addOnCompleteListener(task -> {
        });

        firebaseFirestore.collection("Users")
                .document(companion)
                .collection("Reconciliations(Employer)")
                .document(id)
                .delete().addOnCompleteListener(task -> {
        });

    }

    void setDisable() {

        Map<String, Object> map = new HashMap<>();
        map.put("available", false);

        firebaseFirestore.collection("Jobs")
                .document(id)
                .update(map).addOnCompleteListener(task -> {
        });
    }
}
