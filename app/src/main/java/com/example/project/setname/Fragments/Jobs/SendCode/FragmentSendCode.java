package com.example.project.setname.Fragments.Jobs.SendCode;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.project.setname.Fragments.AdditionalViews.FragmentEmptyList;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

public class FragmentSendCode extends Fragment {

    private RecyclerView sendCode_list_view;
    private List<ModelSendCode> sendCode_list;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    private AdapterSendCode adapterSendCode;

    private AtomicBoolean emptyIsLoaded = new AtomicBoolean(false);

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_send_code_view, container, false);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        String current_user = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        sendCode_list = new ArrayList<>();
        sendCode_list_view = view.findViewById(R.id.fragment_send_code_view_rv);

        adapterSendCode = new AdapterSendCode(sendCode_list);

        sendCode_list_view.setLayoutManager(new LinearLayoutManager(container.getContext()));
        sendCode_list_view.setAdapter(adapterSendCode);

        assert getArguments() != null;
        String id = getArguments().getString("id");
        String user = getArguments().getString("companion");
        String fragmentTag = getArguments().getString("fragmentTag");

        if (firebaseAuth.getCurrentUser() != null) {

            assert id != null;
            assert user != null;

            assert fragmentTag != null;
            if (fragmentTag.equals("FRAGMENT_FROM_MAKER_MAKING")) {
                firebaseFirestore
                        .collection("Users")
                        .document(current_user)
                        .collection("Making(Maker)")
                        .document(id)
                        .collection("CodeList")
                        .addSnapshotListener((documentSnapshots, e) -> {

                            assert documentSnapshots != null;
                            if (documentSnapshots.size() > 0) {

                                for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {

                                    if (doc.getType() == DocumentChange.Type.ADDED) {

                                        ModelSendCode modelSendCode = doc.getDocument().toObject(ModelSendCode.class);
                                        sendCode_list.add(modelSendCode);
                                        adapterSendCode.notifyDataSetChanged();

                                    }
                                }

                            } else {

                                setEmptyList();

                            }

                        });
            } else {

                firebaseFirestore
                        .collection("Users")
                        .document(user)
                        .collection("Making(Maker)")
                        .document(id)
                        .get().addOnCompleteListener(task -> {

                    if (task.isComplete()) {

                        if (task.getResult().getBoolean("moneyWereSend")) {

                            firebaseFirestore
                                    .collection("Users")
                                    .document(user)
                                    .collection("Making(Maker)")
                                    .document(id).collection("CodeList")
                                    .addSnapshotListener((documentSnapshots, e) -> {

                                        assert documentSnapshots != null;
                                        if (documentSnapshots.size() > 0) {

                                            if (emptyIsLoaded.get()) {

                                                assert getFragmentManager() != null;
                                                Objects.requireNonNull(getFragmentManager().findFragmentByTag("FRAGMENT_EMPTY_LIST_SEND_CODE").getView()).setVisibility(View.GONE);

                                            }


                                            for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {

                                                if (doc.getType() == DocumentChange.Type.ADDED) {

                                                    ModelSendCode modelSendCode = doc.getDocument().toObject(ModelSendCode.class);
                                                    sendCode_list.add(modelSendCode);
                                                    adapterSendCode.notifyDataSetChanged();

                                                }
                                            }

                                        } else {

                                            setEmptyList();

                                        }
                                    });

                        } else {

                            sendCode_list_view.setVisibility(View.GONE);
                            Toast.makeText(getContext(), "Firstly you must send money to server" +
                                    "\nCheck rules", Toast.LENGTH_SHORT).show();

                        }
                    }

                });

            }
        }

        return view;
    }

    void setEmptyList() {

        if (!emptyIsLoaded.get()) {

            FragmentEmptyList fragmentEmptyList = new FragmentEmptyList();
            assert getFragmentManager() != null;
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment_send_code_view_cl, fragmentEmptyList, "FRAGMENT_EMPTY_LIST_SEND_CODE");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

            emptyIsLoaded.set(true);

        }

    }

}
