package com.example.project.setname.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.project.setname.MainActivity.MainActivity;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import in.shadowfax.proswipebutton.ProSwipeButton;
import me.yokeyword.fragmentation.SwipeBackLayout;
import me.yokeyword.fragmentation_swipeback.SwipeBackActivity;

public class ActivityLogin extends AppCompatActivity {

    //log
    private static final String TAG = "ActivityLoginClass";

    //views
    private EditText loginEmailText;
    private EditText loginPassText;
    private ProSwipeButton loginBtn;
    private TextView loginRegBtn;

    //service
    public static FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        setViews();

        setListeners();


    }

    private void setListeners() {

        loginRegBtn.setOnClickListener(v -> {

            loginRegBtn.setClickable(false);

            Intent regIntent = new Intent(ActivityLogin.this, ActivityRegister.class);
            startActivity(regIntent);

            loginRegBtn.setClickable(true);

        });


        loginBtn.setOnSwipeListener(() -> {
            new Handler().postDelayed(() -> {

                String loginEmail = loginEmailText.getText().toString();
                String loginPass = loginPassText.getText().toString();

                if (!TextUtils.isEmpty(loginEmail) && !TextUtils.isEmpty(loginPass)) {

                    mAuth.signInWithEmailAndPassword(loginEmail, loginPass).addOnCompleteListener(task -> {

                        if (task.isSuccessful()) {

                            loginBtn.showResultIcon(true);

                            sendToMain();

                        } else {

                            loginBtn.showResultIcon(false);

                            String errorMessage = task.getException().getMessage();

                            Log.e(TAG, "Error: + " + errorMessage);



                        }

                    });

                }
            }, 2000);
        });


    }

    private void setViews() {

        loginEmailText = findViewById(R.id.reg_email);
        loginPassText = findViewById(R.id.reg_confirm_pass);
        loginBtn = findViewById(R.id.login_btn);
        loginRegBtn = findViewById(R.id.login_reg_btn);

    }

    /*@Override
    protected void onStart() {
        super.onStart();

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if(currentUser != null){

            sendToMain();

        }

    }*/

    private void sendToMain() {

        ActivityLogin.this.finish();
        Intent mainIntent = new Intent(ActivityLogin.this, MainActivity.class);
        startActivity(mainIntent);

    }
}
