package com.example.project.setname.ProfileInfo;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.project.setname.Fragments.News.FragmentOpenLink;
import com.example.project.setname.Fragments.News.NewsFragment.AdapterNews;
import com.example.project.setname.Fragments.News.NewsFragment.ModelNews;
import com.example.project.setname.Fragments.News.NewsFull.FragmentNewsFull;
import com.example.project.setname.Interfaces.GetFragment;
import com.example.project.setname.Interfaces.NewsOpenFull;
import com.example.project.setname.Interfaces.NewsOpenWeb;
import com.example.project.setname.Interfaces.OpenProfileInfo;
import com.example.project.setname.ProfileInfo.Achievement.AchievementAdapter;
import com.example.project.setname.ProfileInfo.Achievement.ModelAchievement;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import me.yokeyword.fragmentation_swipeback.SwipeBackFragment;

public class FragmentProfileInfo extends SwipeBackFragment implements NewsOpenFull, NewsOpenWeb, OpenProfileInfo{

    private View view;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;
    private String currentUser;

    private CircleImageView userImage;
    private TextView username;
    private TextView userRank;
    private TextView userAbout;
    private TextView subscribersCount;
    private TextView subscriptionsCount;
    private TextView projectsCount;
    private TextView completeJobCount;
    private View subscribe;
    private TextView subscribeText;

    //posts
    private RecyclerView posts;
    private AdapterNews adapterNews;
    private List<ModelNews> newsList;

    //achieves
    private RecyclerView recyclerView;
    private AchievementAdapter achievementHorizontalAdapter;
    private List<ModelAchievement> achievementHorizontalList;

    //hide
    private View hideAll;

    private Map<String, Object> emptyMap;
    private FragmentNewsFull fragmentNewsFull;

    private String user;

    private boolean canLoadMore = true;
    private DocumentSnapshot lastVisible;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_profile_info, container, false);

        setFirebase();

        setViews();

        emptyMap = new HashMap<>();
        emptyMap.put("timestamp", FieldValue.serverTimestamp());

        user = getArguments().getString("user");

        fragmentNewsFull = new FragmentNewsFull();

        if (isConnected(getContext())) {

            setUserInformation();

            if (user.equals(firebaseAuth.getCurrentUser().getUid())) {

                subscribeText.setVisibility(View.GONE);
                subscribe.setVisibility(View.GONE);

            } else {

                setSubscribeSystem();

            }

            setCounts();

            setAchieves();

            setPosts();

        }else {

            hideAll = view.findViewById(R.id.fragment_profile_info_hide_all);
            hideAll.setVisibility(View.VISIBLE);

            Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();

        }

        return attachToSwipeBack(view);
    }

    public static boolean isConnected(@NonNull Context context) {
        ConnectivityManager
                cm = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
    }

    void setScrollListeners(){

        posts.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                //метод проверяет достиг ли пользователь bottom, если да, то подгружает еще данные(нужно чтобы не сгрузить всю БД за раз)
                if (!recyclerView.canScrollVertically(1)) {

                    if (canLoadMore) {

                        loadMore();

                    }

                }

            }
        });

    }

    void loadMore(){

        canLoadMore = false;

        firebaseFirestore.collection("News")
                .whereEqualTo("user_id", user)
                .startAfter(lastVisible)
                /*.orderBy("timestamp", Query.Direction.DESCENDING)*/
                .limit(10)
                .addSnapshotListener((documentSnapshots1, e1) -> {

                    if (documentSnapshots1.size()>0) {

                        for (DocumentChange doc1 : documentSnapshots1.getDocumentChanges()) {
                            if (doc1.getType() == DocumentChange.Type.ADDED) {

                                String newsId = doc1.getDocument().getId();

                                ModelNews publicNews = doc1.getDocument().toObject(ModelNews.class).withId(newsId);
                                newsList.add(publicNews);
                                adapterNews.notifyDataSetChanged();

                            }
                        }

                        lastVisible = documentSnapshots1.getDocuments().get(documentSnapshots1.size() - 1);

                        canLoadMore = true;

                    }

                });

    }

    void setCounts(){

        firebaseFirestore.collection("Users")
                .document(user)
                .collection("Subscriptions")
                .addSnapshotListener((documentSnapshots, e) -> {

                    assert documentSnapshots != null;

                    if (!documentSnapshots.isEmpty()) {

                        subscriptionsCount.setText(String.valueOf(documentSnapshots.size()));

                    }else {

                        subscriptionsCount.setText("0");

                    }

                });

        firebaseFirestore.collection("Users")
                .document(user)
                .collection("Subscribers")
                .addSnapshotListener((documentSnapshots, e) -> {

                    assert documentSnapshots != null;

                    if (!documentSnapshots.isEmpty()) {

                        subscribersCount.setText(String.valueOf(documentSnapshots.size()));

                    }else {

                        subscribersCount.setText("0");

                    }

                });

        firebaseFirestore.collection("Users")
                .document(user)
                .collection("Projects")
                .addSnapshotListener((documentSnapshots, e) -> {

                    assert documentSnapshots != null;

                    if (!documentSnapshots.isEmpty()) {

                        projectsCount.setText(String.valueOf(documentSnapshots.size()));

                    }else {

                        projectsCount.setText("0");

                    }

                });

        firebaseFirestore.collection("Users")
                .document(user)
                .collection("Complete(Maker)")
                .addSnapshotListener((documentSnapshots, e) -> {

                    assert documentSnapshots != null;

                    if (!documentSnapshots.isEmpty()) {

                        completeJobCount.setText(String.valueOf(documentSnapshots.size()));

                    }else {

                        completeJobCount.setText("0");

                    }

                });

    }

    private void setPosts() {

        posts = view.findViewById(R.id.fragment_profile_info_rv);

        setScrollListeners();

        newsList = new ArrayList<>();
        adapterNews = new AdapterNews(newsList, this, this, this);

        posts.setLayoutManager(new LinearLayoutManager(getContext()));
        posts.setAdapter(adapterNews);

        firstLoad();

    }

    private void firstLoad(){

        firebaseFirestore.collection("News")
                .whereEqualTo("user_id", user)
                /*.orderBy("timestamp", Query.Direction.DESCENDING)*/
                .limit(10)
                .addSnapshotListener((documentSnapshots1, e1) -> {

                    if (documentSnapshots1.size()>0) {

                        for (DocumentChange doc1 : documentSnapshots1.getDocumentChanges()) {
                            if (doc1.getType() == DocumentChange.Type.ADDED) {

                                String newsId = doc1.getDocument().getId();

                                ModelNews publicNews = doc1.getDocument().toObject(ModelNews.class).withId(newsId);
                                newsList.add(publicNews);
                                adapterNews.notifyDataSetChanged();

                            }
                        }

                        lastVisible = documentSnapshots1.getDocuments().get(documentSnapshots1.size() - 1);

                    }

                });

    }

    private void setAchieves() {

        achievementHorizontalList = new ArrayList<>();
        achievementHorizontalAdapter = new AchievementAdapter(achievementHorizontalList);
        recyclerView = view.findViewById(R.id.activity_opened_horizontal);
        firebaseFirestore.collection("Users")
                .document(user)
                .collection("Achievement")
                .addSnapshotListener((documentSnapshots, e) -> {

                    for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {
                        if (doc.getType() == DocumentChange.Type.ADDED) {
                            ModelAchievement achievementHorizontal = doc.getDocument().toObject(ModelAchievement.class);
                            achievementHorizontalList.add(achievementHorizontal);
                            achievementHorizontalAdapter.notifyDataSetChanged();

                        }
                    }

                    recyclerView.setLayoutManager(new GridLayoutManager(getContext(), achievementHorizontalList.size()));
                    recyclerView.setAdapter(achievementHorizontalAdapter);

                });

    }

    private void setSubscribeSystem() {

        firebaseFirestore.collection("Users")
                .document(currentUser)
                .collection("Subscriptions")
                .document(user)
                .get()
                .addOnCompleteListener(task3 -> {

                    if (!task3.getResult().exists()) {

                        subscribeText.setText("Subscribe");
                        subscribe.setBackgroundResource(R.drawable.fragment_profile_info_sub);

                        subscribeText.setOnClickListener(v -> {

                            subscribeText.setClickable(false);
                            subscribe.setClickable(false);

                            firebaseFirestore.collection("Users")
                                    .document(currentUser)
                                    .collection("Subscriptions")
                                    .document(user)
                                    .set(emptyMap)
                                    .addOnCompleteListener(task -> {

                                        if (task.isComplete()) {

                                            firebaseFirestore.collection("Users")
                                                    .document(user)
                                                    .collection("Subscribers")
                                                    .document(currentUser)
                                                    .set(emptyMap)
                                                    .addOnCompleteListener(task1 -> {

                                                        if (task1.isComplete()) {

                                                            subscribeText.setText("Subscribed");
                                                            subscribe.setBackgroundResource(R.drawable.fragment_profile_info_unsub);

                                                        }

                                                    });

                                        }

                                        subscribeText.setClickable(true);
                                        subscribe.setClickable(true);

                                    });


                        });

                        subscribe.setOnClickListener(v1 -> {

                            subscribeText.setClickable(false);
                            subscribe.setClickable(false);

                            firebaseFirestore.collection("Users")
                                    .document(currentUser)
                                    .collection("Subscriptions")
                                    .document(user)
                                    .set(emptyMap)
                                    .addOnCompleteListener(task -> {

                                        if (task.isComplete()) {

                                            firebaseFirestore.collection("Users")
                                                    .document(user)
                                                    .collection("Subscribers")
                                                    .document(currentUser)
                                                    .set(emptyMap)
                                                    .addOnCompleteListener(task1 -> {

                                                        if (task1.isComplete()) {

                                                            subscribeText.setText("Subscribed");
                                                            subscribe.setBackgroundResource(R.drawable.fragment_profile_info_unsub);

                                                        }

                                                    });

                                        }

                                        subscribeText.setClickable(true);
                                        subscribe.setClickable(true);

                                    });


                        });

                    } else {

                        subscribeText.setText("Subscribed");
                        subscribe.setBackgroundResource(R.drawable.fragment_profile_info_unsub);

                        subscribeText.setOnClickListener(v -> {

                            subscribeText.setClickable(false);
                            subscribe.setClickable(false);

                            firebaseFirestore.collection("Users")
                                    .document(currentUser)
                                    .collection("Subscriptions")
                                    .document(user)
                                    .delete()
                                    .addOnCompleteListener(task -> {

                                        if (task.isComplete()) {

                                            firebaseFirestore.collection("Users")
                                                    .document(user)
                                                    .collection("Subscribers")
                                                    .document(currentUser)
                                                    .delete()
                                                    .addOnCompleteListener(task1 -> {

                                                        if (task1.isComplete()) {

                                                            subscribeText.setText("Subscribe");
                                                            subscribe.setBackgroundResource(R.drawable.fragment_profile_info_sub);

                                                        }

                                                    });

                                        }

                                        subscribeText.setClickable(true);
                                        subscribe.setClickable(true);


                                    });



                        });

                        subscribe.setOnClickListener(v -> {

                            subscribeText.setClickable(false);
                            subscribe.setClickable(false);

                            firebaseFirestore.collection("Users")
                                    .document(currentUser)
                                    .collection("Subscriptions")
                                    .document(user)
                                    .delete()
                                    .addOnCompleteListener(task -> {

                                        if (task.isComplete()) {

                                            firebaseFirestore.collection("Users")
                                                    .document(user)
                                                    .collection("Subscribers")
                                                    .document(currentUser)
                                                    .delete()
                                                    .addOnCompleteListener(task1 -> {

                                                        if (task1.isComplete()) {

                                                            subscribeText.setText("Subscribe");
                                                            subscribe.setBackgroundResource(R.drawable.fragment_profile_info_sub);

                                                        }

                                                    });

                                        }

                                        subscribeText.setClickable(true);
                                        subscribe.setClickable(true);


                                    });


                        });

                    }

                });

    }

    private void setUserInformation() {

        firebaseFirestore.collection("Users")
                .document(user)
                .get().addOnCompleteListener(task -> {

            if (task.isComplete()) {

                RequestOptions placeholderOption = new RequestOptions();
                placeholderOption.placeholder(R.drawable.profile_placeholder);
                Glide.with(getContext()).applyDefaultRequestOptions(placeholderOption)
                        .load(task.getResult().getString("image"))
                        .into(userImage);

                username.setText(task.getResult().getString("name"));
                userRank.setText(task.getResult().getString("rank"));
                userAbout.setText(task.getResult().getString("about"));

            }

        });

    }

    private void setFirebase() {

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();
        currentUser = firebaseAuth.getCurrentUser().getUid();

    }

    @Override
    public void openFull(Bitmap bmp,
                         String username,
                         String text,
                         String news_id) {

        assert getFragmentManager() != null;
        Bundle bundle = new Bundle();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        try {
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            bundle.putByteArray("image", byteArray);
        } catch (NullPointerException ignored) {
        }

        bundle.putString("username", username);

        bundle.putString("text", text);

        bundle.putString("news_id", news_id);

        fragmentNewsFull.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left);
        fragmentTransaction.add(R.id.main_container, fragmentNewsFull, "NEWS_FULL");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    private void setViews(){

        userImage = view.findViewById(R.id.fragment_profile_info_profile_image);
        username = view.findViewById(R.id.fragment_profile_info_username);
        userRank = view.findViewById(R.id.fragment_profile_info_rank);
        userAbout = view.findViewById(R.id.fragment_profile_info_about);
        subscribersCount = view.findViewById(R.id.fragment_profile_info_subscribers_count);
        subscriptionsCount = view.findViewById(R.id.fragment_profile_info_subcriptions_count);
        subscribe = view.findViewById(R.id.fragment_profile_info_sub_view);
        subscribeText = view.findViewById(R.id.fragment_profile_info_sub_text);
        projectsCount = view.findViewById(R.id.fragment_profile_info_projects_count);
        completeJobCount = view.findViewById(R.id.fragment_profile_info_complete_job_count);

    }

    @Override
    public void openWeb(String link) {
        FragmentOpenLink fragmentOpenLink = new FragmentOpenLink();
        assert getFragmentManager() != null;
        Bundle bundle = new Bundle();
        bundle.putString("link", link);
        fragmentOpenLink.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left);
        fragmentTransaction.add(R.id.main_container, fragmentOpenLink, "FRAGMENT_WEB");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void openProfile(String user) {
        FragmentProfileInfo fragmentProfileInfo = new FragmentProfileInfo();
        assert getFragmentManager() != null;
        Bundle bundle = new Bundle();
        bundle.putString("user", user);
        fragmentProfileInfo.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left);
        fragmentTransaction.add(R.id.main_container, fragmentProfileInfo, "FRAGMENT_PROFILE_INFO");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


}
