package com.example.project.setname.Fragments.Events.Purchased;

import android.support.annotation.NonNull;

import com.google.firebase.firestore.Exclude;

public class ModelPurchasedId {

    @Exclude
    public String ModelFragmentPurchasedId;

    public <T extends ModelPurchasedId> T withId(@NonNull final String id){
        this.ModelFragmentPurchasedId = id;
        return (T) this;
    }

}
