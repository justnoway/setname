package com.example.project.setname.Dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.project.setname.Fragments.News.NewsNew.FragmentNewNews;
import com.example.project.setname.R;

import java.util.Objects;

import fr.tvbarthel.lib.blurdialogfragment.SupportBlurDialogFragment;


public class DialogNewsNewShowImage extends SupportBlurDialogFragment {

    private ImageView image;

    private String image_url;

    private static final String BUNDLE_KEY_DOWN_SCALE_FACTOR = "bundle_key_down_scale_factor";

    private static final String BUNDLE_KEY_BLUR_RADIUS = "bundle_key_blur_radius";

    private static final String BUNDLE_KEY_DIMMING = "bundle_key_dimming_effect";

    private static final String BUNDLE_KEY_DEBUG = "bundle_key_debug_effect";

    public static DialogSendFromAvailableToRequests newInstance(int radius,
                                                                float downScaleFactor,
                                                                boolean dimming,
                                                                boolean debug) {
        DialogSendFromAvailableToRequests fragment = new DialogSendFromAvailableToRequests();
        Bundle args = new Bundle();
        args.putInt(
                BUNDLE_KEY_BLUR_RADIUS,
                radius
        );
        args.putFloat(
                BUNDLE_KEY_DOWN_SCALE_FACTOR,
                downScaleFactor
        );
        args.putBoolean(
                BUNDLE_KEY_DIMMING,
                dimming
        );
        args.putBoolean(
                BUNDLE_KEY_DEBUG,
                debug
        );

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        assert getArguments() != null;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        @SuppressLint("InflateParams") View view = Objects.requireNonNull(getActivity()).getLayoutInflater().inflate(R.layout.dialog_news_new_show_image, null);

        image = view.findViewById(R.id.dialog_news_new_show_image_image);

        builder.setView(view);
        return builder.create();

    }

    @Override
    public void onResume() {
        super.onResume();
        image_url = getArguments().getString("image_url");

        FragmentNewNews fragmentNewNews = new FragmentNewNews();
        FragmentNewNews.imageList.set(image_url);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.image_placeholder);
        Glide.with(Objects.requireNonNull(getContext())).applyDefaultRequestOptions(requestOptions)
                .load(image_url)
                .listener(new RequestListener<Drawable>() {
                              @Override
                              public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {

                                  Toast.makeText(getContext(), "Incorrect URL", Toast.LENGTH_SHORT).show();

                                  FragmentNewNews.imageList.set("None");

                                  return false;
                              }

                              @Override
                              public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                                  return false;
                              }



                          }
                )
                .into(image);

    }

    @Override
    public void onStop() {
        super.onStop();

        image_url = "";

    }

    public void hideIt(){
        getDialog().dismiss();
    }
}