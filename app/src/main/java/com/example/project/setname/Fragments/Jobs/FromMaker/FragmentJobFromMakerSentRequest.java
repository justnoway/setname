package com.example.project.setname.Fragments.Jobs.FromMaker;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.project.setname.Fragments.AdditionalViews.FragmentEmptyList;
import com.example.project.setname.Fragments.Jobs.Requests.AdapterRequests;
import com.example.project.setname.Fragments.Jobs.Requests.ModelRequests;
import com.example.project.setname.Interfaces.GetFragment;
import com.example.project.setname.Interfaces.OpenProfileInfo;
import com.example.project.setname.ProfileInfo.FragmentProfileInfo;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

public class FragmentJobFromMakerSentRequest extends Fragment implements GetFragment, OpenProfileInfo {

    private RecyclerView comeRequest_list_view;
    private List<ModelRequests> comeRequest_list;

    private AdapterRequests adapterRequests;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    private String current_user;

    private DocumentSnapshot lastVisible;

    private FragmentProfileInfo fragmentProfileInfo;

    private AtomicBoolean emptyIsLoaded = new AtomicBoolean(false);

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_from_maker_sent_requests, container, false);

        fragmentProfileInfo = new FragmentProfileInfo();

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        comeRequest_list = new ArrayList<>();
        comeRequest_list_view = view.findViewById(R.id.job_from_maker_sent_requests_cr_recyclerview);

        adapterRequests = new AdapterRequests(comeRequest_list, this, this);

        comeRequest_list_view.setLayoutManager(new LinearLayoutManager(container.getContext()));
        comeRequest_list_view.setAdapter(adapterRequests);


        current_user = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        if (firebaseAuth.getCurrentUser() != null) {

            loadData();
        }


        return view;
    }

    public void refreshData() {

        comeRequest_list.clear();
        adapterRequests.notifyDataSetChanged();
        loadData();

    }

    private void loadData() {

        firebaseFirestore.collection("Users")
                .document(current_user)
                .collection("SentRequestsForJob(Maker)")
                .addSnapshotListener((documentSnapshots, e) -> {

                    comeRequest_list.clear();
                    adapterRequests.notifyDataSetChanged();

                    assert documentSnapshots != null;
                    if (documentSnapshots.size() > 0) {

                        if (emptyIsLoaded.get()) {

                            assert getFragmentManager() != null;
                            Objects.requireNonNull(getFragmentManager().findFragmentByTag("FRAGMENT_EMPTY_LIST_FROM_MAKER_REQUESTS").getView()).setVisibility(View.GONE);

                        }

                        if (lastVisible != documentSnapshots.getDocuments().get(documentSnapshots.size() - 1)) {

                            for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {

                                if (doc.getType() == DocumentChange.Type.ADDED) {

                                    if (lastVisible != doc.getDocument()) {

                                        String ID = doc.getDocument().getId();

                                        ModelRequests modelRequests = doc.getDocument().toObject(ModelRequests.class).withId(ID);
                                        comeRequest_list.add(modelRequests);
                                        adapterRequests.notifyDataSetChanged();

                                        lastVisible = doc.getDocument();

                                    }

                                }
                            }

                            lastVisible = documentSnapshots.getDocuments().get(documentSnapshots.size() - 1);

                        }
                    }else {
                        setEmptyList();
                    }
                });

    }

    void setEmptyList() {

        if (!emptyIsLoaded.get()) {

            FragmentEmptyList fragmentEmptyList = new FragmentEmptyList();
            assert getFragmentManager() != null;
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment_from_maker_sent_requests_cl, fragmentEmptyList, "FRAGMENT_EMPTY_LIST_FROM_MAKER_REQUESTS");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

            emptyIsLoaded.set(true);

        }

    }

    @Override
    public String getCustomFragment() {
        return "FRAGMENT_JOB_FROM_MAKER_SENT_REQUESTS";
    }

    @Override
    public void openProfile(String user) {
        assert getFragmentManager() != null;
        Bundle bundle = new Bundle();
        bundle.putString("user", user);
        fragmentProfileInfo.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left);
        fragmentTransaction.add(R.id.main_container, fragmentProfileInfo, "FRAGMENT_PROFILE_INFO");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
