package com.example.project.setname.Dialogs;

import android.annotation.SuppressLint;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.example.project.setname.R;
import java.util.Objects;
import fr.tvbarthel.lib.blurdialogfragment.SupportBlurDialogFragment;

public class DialogNewsNewShowWeb extends SupportBlurDialogFragment {

    private static final String TAG = "DialogNewsShowWeb";

    private WebView webview;

    private String web_link;

    private static final String BUNDLE_KEY_DOWN_SCALE_FACTOR = "bundle_key_down_scale_factor";

    private static final String BUNDLE_KEY_BLUR_RADIUS = "bundle_key_blur_radius";

    private static final String BUNDLE_KEY_DIMMING = "bundle_key_dimming_effect";

    private static final String BUNDLE_KEY_DEBUG = "bundle_key_debug_effect";

    public static DialogSendFromAvailableToRequests newInstance(int radius,
                                                                float downScaleFactor,
                                                                boolean dimming,
                                                                boolean debug) {
        DialogSendFromAvailableToRequests fragment = new DialogSendFromAvailableToRequests();
        Bundle args = new Bundle();
        args.putInt(
                BUNDLE_KEY_BLUR_RADIUS,
                radius
        );
        args.putFloat(
                BUNDLE_KEY_DOWN_SCALE_FACTOR,
                downScaleFactor
        );
        args.putBoolean(
                BUNDLE_KEY_DIMMING,
                dimming
        );
        args.putBoolean(
                BUNDLE_KEY_DEBUG,
                debug
        );

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("SetJavaScriptEnabled")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        assert getArguments() != null;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        @SuppressLint("InflateParams") View view = Objects.requireNonNull(getActivity()).getLayoutInflater().inflate(R.layout.dialog_news_new_show_web, null);

        webview = view.findViewById(R.id.dialog_news_new_show_web_web);

        webview.setWebViewClient(new WebViewClient());
        /*webview.getSettings().setJavaScriptEnabled(true);*/
        webview.getSettings().setDomStorageEnabled(true);
        /*webview.setOverScrollMode(WebView.OVER_SCROLL_NEVER);*/

        builder.setView(view);
        return builder.create();

    }

    @Override
    public void onResume() {
        super.onResume();
        web_link = getArguments().getString("link");

        webview.loadUrl(web_link);

        Log.i(TAG, "Link is: " + web_link);

    }

    @Override
    public void onStop() {
        super.onStop();

        web_link = "";

    }

    public void hideIt() {
        getDialog().dismiss();
    }

}

