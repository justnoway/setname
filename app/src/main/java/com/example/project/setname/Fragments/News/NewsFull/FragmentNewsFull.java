package com.example.project.setname.Fragments.News.NewsFull;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.project.setname.Fragments.News.NewsFull.Comments.CommentAdapter;
import com.example.project.setname.Fragments.News.NewsFull.Comments.ModelComments;
import com.example.project.setname.Interfaces.OpenProfileInfo;
import com.example.project.setname.ProfileInfo.FragmentProfileInfo;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import me.yokeyword.fragmentation_swipeback.SwipeBackFragment;
import xyz.hanks.library.bang.SmallBangView;


public class FragmentNewsFull extends SwipeBackFragment implements OpenProfileInfo {

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    //comments
    private List<ModelComments> news_comments_list;
    private RecyclerView news_comments_list_view;
    private CommentAdapter commentAdapter;
    private FragmentProfileInfo fragmentProfileInfo;

    //like, dislike
    private TextView counterLike_view;
    private TextView counterDislike_view;

    //view
    private View hideAll;

    //button send
    private View fieldForMessage;
    private EditText textForSend;
    private ImageView send;
    private CircleImageView circleImageView;
    private TextView commentClick;
    private View hideKeyboardView;
    private View hideView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_news_full, container, false);

        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();

        fragmentProfileInfo = new FragmentProfileInfo();

        ImageView imageView = view.findViewById(R.id.item_full_news_image);

        if (isConnected(getContext())) {

            final String current_user = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

            assert getArguments() != null;
            try {
                byte[] byteArray = getArguments().getByteArray("image");
                assert byteArray != null;
                Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

                //чтобы placeholder не отоюражался:)
                //кощунство, но так как это alpha, то пойдет
                if (byteArray.length != 3518 && byteArray.length != 115861) {
                    imageView.setImageBitmap(bmp);
                } else {
                    imageView.setVisibility(View.GONE);
                }


            } catch (NullPointerException ignored) {

                imageView.setVisibility(View.GONE);

            }

            String username = getArguments().getString("username");
            String text = getArguments().getString("text");
            String newsId = getArguments().getString("news_id");

            TextView username_view = view.findViewById(R.id.item_full_news_username);
            TextView text_view = view.findViewById(R.id.item_full_news_text);
            TextView rank_view = view.findViewById(R.id.item_full_news_rank);
            CircleImageView userImage_view = view.findViewById(R.id.item_news_full_profile_photo);
            fieldForMessage = view.findViewById(R.id.fragment_news_full_enter_field);
            circleImageView = view.findViewById(R.id.fragment_news_full_imageCircle);
            commentClick = view.findViewById(R.id.fragment_news_full_tv_set_comment);
            counterLike_view = view.findViewById(R.id.item_full_news_like_score);
            counterDislike_view = view.findViewById(R.id.item_full_news_dislike_score);
            send = view.findViewById(R.id.fragment_news_full_send);
            textForSend = view.findViewById(R.id.fragment_news_full_form_for_message);
            textForSend.addTextChangedListener(loginTextWatcher);
            hideView = view.findViewById(R.id.fragment_news_full_hide);
            hideKeyboardView = view.findViewById(R.id.fragment_news_full_hide_keyboard);

            hideKeyboardView.setOnClickListener(v1 -> {
                hideView.setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager) Objects.requireNonNull(getActivity()).getSystemService(Activity.INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            });

            commentClick.setOnClickListener(v -> {
                hideView.setVisibility(View.VISIBLE);
                showSoftKeyboard(textForSend);
            });

            firebaseFirestore.collection("Users").document(current_user).get().addOnCompleteListener(task -> {

                if (task.isSuccessful()) {
                    String userImage = task.getResult().getString("image");
                    RequestOptions placeholderOption = new RequestOptions();
                    placeholderOption.placeholder(R.drawable.profile_placeholder);
                    Glide.with(container.getContext()).applyDefaultRequestOptions(placeholderOption).load(userImage).into(circleImageView);
                }
            });

            assert username != null;
            firebaseFirestore.collection("Users").document(username).get().addOnCompleteListener(task -> {

                if (task.isSuccessful()) {

                    String userName = task.getResult().getString("name");
                    String userRank = task.getResult().getString("rank");
                    String userImage = task.getResult().getString("image");

                    username_view.setText(userName);
                    rank_view.setText(userRank);
                    RequestOptions placeholderOption = new RequestOptions();
                    placeholderOption.placeholder(R.drawable.profile_placeholder);
                    Glide.with(container.getContext()).applyDefaultRequestOptions(placeholderOption).load(userImage).into(userImage_view);

                }
            });

            username_view.setText(username);
            text_view.setText(text);

            news_comments_list = new ArrayList<>();
            news_comments_list_view = view.findViewById(R.id.item_news_comments_list);

            commentAdapter = new CommentAdapter(news_comments_list, this);

            news_comments_list_view.setLayoutManager(new LinearLayoutManager(container.getContext()));

            news_comments_list_view.setAdapter(commentAdapter);

            SmallBangView like_view = view.findViewById(R.id.item_full_news_like_heart);

            SmallBangView dislike_view = view.findViewById(R.id.item_news_dislike_heart);

            assert newsId != null;

            //getters
            firebaseFirestore.collection("News").document(newsId).collection("Likes").document(current_user).addSnapshotListener((documentSnapshot, e) -> {

                if (documentSnapshot.exists()) {

                    like_view.setSelected(true);

                } else {

                    like_view.setSelected(false);

                }

            });

            firebaseFirestore.collection("News").document(newsId).collection("Dislikes").document(current_user).addSnapshotListener((documentSnapshot, e) -> {

                if (documentSnapshot.exists()) {

                    dislike_view.setSelected(true);

                } else {

                    dislike_view.setSelected(false);
                }

            });

            //counters
            firebaseFirestore.collection("News").document(newsId).collection("Likes").addSnapshotListener((documentSnapshots, e) -> {

                if (!documentSnapshots.isEmpty()) {

                    int count = documentSnapshots.size();

                    updateCount(true, count);

                } else {

                    updateCount(true, 0);

                }

            });

            firebaseFirestore.collection("News").document(newsId).collection("Dislikes").addSnapshotListener((documentSnapshots, e) -> {

                if (!documentSnapshots.isEmpty()) {

                    int count = documentSnapshots.size();

                    updateCount(false, count);

                } else {

                    updateCount(false, 0);

                }

            });

            //setters
            like_view.setOnClickListener(v -> {

                firebaseFirestore.collection("News")
                        .document(newsId)
                        .collection("Likes")
                        .document(current_user)
                        .get().addOnCompleteListener(task -> {


                    if (!like_view.isSelected()) {

                        like_view.setSelected(false);
                        firebaseFirestore.collection("News").document(newsId).collection("Likes").document(current_user).delete();

                    } else {

                        dislike_view.setSelected(false);
                        like_view.setSelected(true);
                        firebaseFirestore.collection("News").document(newsId).collection("Dislikes").document(current_user).delete();

                        HashMap<String, Object> likes = new HashMap<>();
                        likes.put("timetamps", FieldValue.serverTimestamp());
                        firebaseFirestore.collection("News").document(newsId).collection("Likes").document(current_user).set(likes);

                    }


                });


                if (like_view.isSelected()) {
                    like_view.setSelected(false);
                } else {
                    like_view.setSelected(true);
                    like_view.likeAnimation(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                        }
                    });
                }
            });

            dislike_view.setOnClickListener(v -> {


                firebaseFirestore.collection("News")
                        .document(newsId)
                        .collection("Dislikes")
                        .document(current_user)
                        .get().addOnCompleteListener(task -> {


                    if (!dislike_view.isSelected()) {


                        dislike_view.setSelected(false);
                        firebaseFirestore.collection("News").document(newsId).collection("Dislikes").document(current_user).delete();

                    } else {

                        like_view.setSelected(false);
                        dislike_view.setSelected(true);

                        firebaseFirestore.collection("News").document(newsId).collection("Likes").document(current_user).delete();
                        HashMap<String, Object> likes = new HashMap<>();
                        likes.put("timetamps", FieldValue.serverTimestamp());

                        firebaseFirestore.collection("News").document(newsId).collection("Dislikes").document(current_user).set(likes);

                    }


                });


                if (dislike_view.isSelected()) {
                    dislike_view.setSelected(false);
                } else {
                    dislike_view.setSelected(true);
                    dislike_view.likeAnimation(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                        }
                    });
                }

            });

            firebaseFirestore.collection("News")
                    .document(newsId)
                    .collection("Comments")
                    .orderBy("timestamp", Query.Direction.DESCENDING)
                    .addSnapshotListener((documentSnapshots, e) -> {

                        if (!documentSnapshots.isEmpty()) {

                            for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {

                                if (doc.getType() == DocumentChange.Type.ADDED) {

                                    String commentId = doc.getDocument().getId();

                                    ModelComments comments = doc.getDocument().toObject(ModelComments.class);
                                    news_comments_list.add(comments);
                                    commentAdapter.notifyDataSetChanged();


                                }
                            }

                        }

                    });


            send.setOnClickListener(v -> {

                send.setClickable(false);

                String comment_message = textForSend.getText().toString();

                Map<String, Object> commentsMap = new HashMap<>();
                commentsMap.put("message", comment_message);
                commentsMap.put("user_id", current_user);
                commentsMap.put("timestamp", FieldValue.serverTimestamp());

                firebaseFirestore.collection("News")
                        .document(newsId)
                        .collection("Comments")
                        .add(commentsMap)
                        .addOnCompleteListener(task -> {

                            if (task.isSuccessful()) {

                                textForSend.setText("");
                                Toast.makeText(container.getContext(), "Message was sent", Toast.LENGTH_SHORT).show();
                                hideView.setVisibility(View.GONE);

                                InputMethodManager imm = (InputMethodManager) Objects.requireNonNull(getActivity()).getSystemService(Activity.INPUT_METHOD_SERVICE);
                                assert imm != null;
                                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);


                            } else {

                            }

                        });

                send.setClickable(true);

            });
        }else {

            Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();

            hideView = view.findViewById(R.id.fragment_news_full_hide_all);
            hideView.setVisibility(View.VISIBLE);

        }

        return attachToSwipeBack(view);
    }

    void updateCount(boolean like, int count) {

        if (like) {

            counterLike_view.setText(String.valueOf(count));

        } else {

            counterDislike_view.setText(String.valueOf(count));

        }
    }

    public static boolean isConnected(@NonNull Context context) {
        ConnectivityManager
                cm = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
    }

    private TextWatcher loginTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String usernameInput = textForSend.getText().toString().trim();

            if (!usernameInput.isEmpty()) {
                send.setEnabled(true);
                send.setImageResource(R.drawable.ic_send);
            } else {
                send.setEnabled(false);
                send.setImageResource(R.drawable.ic_send_disable);
            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public void showSoftKeyboard(View view) {
        if (view.requestFocus()) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    @Override
    public void openProfile(String user) {
        assert getFragmentManager() != null;
        Bundle bundle = new Bundle();
        bundle.putString("user", user);
        fragmentProfileInfo.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left);
        fragmentTransaction.add(R.id.main_container, fragmentProfileInfo, "FRAGMENT_PROFILE_INFO");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
