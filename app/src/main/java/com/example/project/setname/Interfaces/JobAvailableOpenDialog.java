package com.example.project.setname.Interfaces;

public interface JobAvailableOpenDialog {
    void openDialog(String postId, String employer, String price, String date, long indexInList);
}
