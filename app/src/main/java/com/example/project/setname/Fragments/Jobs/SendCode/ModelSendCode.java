package com.example.project.setname.Fragments.Jobs.SendCode;

import java.util.Date;

public class ModelSendCode extends ModelIdSendCode {

    String link;
    Date timestamp;

    public ModelSendCode(String link, Date timestamp) {
        this.link = link;
        this.timestamp = timestamp;
    }

    public ModelSendCode() {
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
