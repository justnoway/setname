package com.example.project.setname.Chat;

import java.util.Date;

public class ModelChat extends ModelChatId{

    String sender, text;
    Date timestamp;

    public ModelChat(String sender, String text, Date timestamp) {
        this.sender = sender;
        this.text = text;
        this.timestamp = timestamp;
    }

    public ModelChat(String sender, String text) {
        this.sender = sender;
        this.text = text;
    }

    public ModelChat() {
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
