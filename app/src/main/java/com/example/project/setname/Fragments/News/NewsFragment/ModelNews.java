package com.example.project.setname.Fragments.News.NewsFragment;

import java.util.Date;

public class ModelNews extends ModelIdNews {

    String user_id, image_url, text, counter_views, link;
    public Date timestamp;

    public ModelNews() {
    }

    /*public ModelNews(String user_id, String counter_views, Date timestamp, String image_url, String link) {
        this.user_id = user_id;
        this.counter_views = counter_views;
        this.link = link;
        this.image_url = image_url;
        this.timestamp = timestamp;
    }*/

    public ModelNews(String user_id, String image_url, Date timestamp) {
        this.user_id = user_id;
        this.image_url = image_url;
        this.timestamp = timestamp;
    }

    public ModelNews(Date timestamp, String user_id, String image_url) {
        this.user_id = user_id;
        this.text = text;
        this.timestamp = timestamp;
    }

    public ModelNews(String user_id, String text, String counter_views, Date timestamp, String link) {
        this.user_id = user_id;
        this.text = text;
        this.counter_views = counter_views;
        this.link = link;
        this.timestamp = timestamp;
    }

    public ModelNews(String user_id, String image_url, String text, String counter_views, String link, Date timestamp) {
        this.user_id = user_id;
        this.image_url = image_url;
        this.text = text;
        this.counter_views = counter_views;
        this.link = link;
        this.timestamp = timestamp;
    }

    public ModelNews(String user_id, String image_url, String text, String link, Date timestamp) {
        this.user_id = user_id;
        this.image_url = image_url;
        this.text = text;
        this.link = link;
        this.timestamp = timestamp;
    }

    /*public ModelNews(String user_id, String image_url, String text, String counter_views, Date timestamp) {
        this.user_id = user_id;
        this.image_url = image_url;
        this.text = text;
        this.counter_views = counter_views;
        this.timestamp = timestamp;
    }

    public ModelNews(String user_id, String image_url, String text, Date timestamp) {
        this.user_id = user_id;
        this.image_url = image_url;
        this.text = text;
        this.timestamp = timestamp;
    }

    public ModelNews(Date timestamp, String link, String image_url, String text) {
        this.user_id = user_id;
        this.image_url = image_url;
        this.link = link;
        this.timestamp = timestamp;
    }

    public ModelNews(String user_id, String image_url, Date timestamp) {
        this.user_id = user_id;
        this.image_url = image_url;
        this.timestamp = timestamp;
    }

    public ModelNews(Date timestamp ,String user_id, String text) {
        this.timestamp = timestamp;
        this.user_id = user_id;
        this.text = text;
    }*/

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCounter_views() {
        return counter_views;
    }

    public void setCounter_views(String counter_views) {
        this.counter_views = counter_views;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
