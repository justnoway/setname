package com.example.project.setname.Fragments.Jobs.FromEmloyer;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.setname.Dialogs.DialogSendFromRequestsToReconciliations;
import com.example.project.setname.Fragments.AdditionalViews.FragmentEmptyList;
import com.example.project.setname.Fragments.Jobs.Requests.AdapterRequests;
import com.example.project.setname.Fragments.Jobs.Requests.ModelRequests;
import com.example.project.setname.Interfaces.GetFragment;
import com.example.project.setname.Interfaces.JobAvailableOpenDialog;
import com.example.project.setname.Interfaces.JobDeleteItemFromList;
import com.example.project.setname.Interfaces.OpenProfileInfo;
import com.example.project.setname.ProfileInfo.FragmentProfileInfo;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

public class FragmentJobFromEmployerComeRequests extends Fragment implements GetFragment, JobAvailableOpenDialog,
        OpenProfileInfo, DialogSendFromRequestsToReconciliations.DeleteListener, JobDeleteItemFromList {

    //получить состояние объекта из диалога(если true, то элемент удаляется из списка)
    @Override
    public void getState(long positionInList, boolean thisState) {
        if (thisState) {

            deleteItem(positionInList);

        }
    }

    private static final String TAG = "FragmentJobFRCR";

    private View view;

    private RecyclerView comeRequest_list_view;
    private List<ModelRequests> comeRequest_list;

    private AdapterRequests adapterRequests;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    private String current_user;

    private DocumentSnapshot lastVisible;

    private FragmentProfileInfo fragmentProfileInfo;

    private TextView tv;

    private AtomicBoolean emptyIsLoaded = new AtomicBoolean(false);

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_from_employer_come_requests, container, false);

        fragmentProfileInfo = new FragmentProfileInfo();

        setFirebase();

        setRV(getContext());

        if (firebaseAuth.getCurrentUser() != null) {

            loadData();

        }

        return view;
    }

    private void setRV(Context context) {

        comeRequest_list = new ArrayList<>();
        comeRequest_list_view = view.findViewById(R.id.fragment_from_employer_come_requests_rv);

        adapterRequests = new AdapterRequests(comeRequest_list, this, this, this, this);

        comeRequest_list_view.setLayoutManager(new LinearLayoutManager(context));
        comeRequest_list_view.setAdapter(adapterRequests);

    }

    private void setFirebase() {

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        current_user = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

    }

    private void loadData() {

        firebaseFirestore.collection("Users")
                .document(current_user)
                .collection("ReceivedRequestsForJob(Employer)")
                .addSnapshotListener((documentSnapshots, e) -> {

                    assert documentSnapshots != null;
                    if (documentSnapshots.size() > 0) {

                        if (emptyIsLoaded.get()) {

                            assert getFragmentManager() != null;
                            Objects.requireNonNull(getFragmentManager().findFragmentByTag("FRAGMENT_EMPTY_LIST_FJFECR").getView()).setVisibility(View.GONE);

                        }

                        if (lastVisible != documentSnapshots.getDocuments().get(documentSnapshots.size() - 1)) {

                            comeRequest_list.clear();
                            adapterRequests.notifyDataSetChanged();

                            for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {

                                if (doc.getType() == DocumentChange.Type.ADDED) {

                                    if (lastVisible != doc.getDocument()) {

                                        String ID = doc.getDocument().getId();

                                        ModelRequests modelRequests = doc.getDocument().toObject(ModelRequests.class).withId(ID);
                                        comeRequest_list.add(modelRequests);
                                        adapterRequests.notifyDataSetChanged();

                                        lastVisible = doc.getDocument();

                                    }

                                }
                            }
                            lastVisible = documentSnapshots.getDocuments().get(documentSnapshots.size() - 1);
                        }
                    } else {
                        setEmptyList();
                    }
                });

    }

    public void refreshData() {

        comeRequest_list.clear();
        adapterRequests.notifyDataSetChanged();
        loadData();

    }

    @Override
    public String getCustomFragment() {
        return "FRAGMENT_JOB_FROM_EMPLOYER_COME_REQUESTS";
    }

    @Override
    public void openDialog(String postId, String maker, String price, String date, long indexInList) {

        DialogSendFromRequestsToReconciliations dialogSendFromRequestsToReconciliations = new DialogSendFromRequestsToReconciliations();
        Bundle bundle = new Bundle();
        bundle.putString("id", postId);
        bundle.putString("maker", maker);
        bundle.putString("price", price);
        bundle.putString("date", date);
        bundle.putLong("position", indexInList);
        dialogSendFromRequestsToReconciliations.setArguments(bundle);
        dialogSendFromRequestsToReconciliations.setTargetFragment(FragmentJobFromEmployerComeRequests.this, 1);
        assert getFragmentManager() != null;
        dialogSendFromRequestsToReconciliations.show(getFragmentManager(), "DIALOG_FROM_REQUESTS_TO_RECONCILIATIONS");

    }

    void setEmptyList() {

        if (!emptyIsLoaded.get()) {

            FragmentEmptyList fragmentEmptyList = new FragmentEmptyList();
            assert getFragmentManager() != null;
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment_from_employer_come_requests_cl, fragmentEmptyList, "FRAGMENT_EMPTY_LIST_FJFECR");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

            emptyIsLoaded.set(true);

        }

    }

    @Override
    public void openProfile(String user) {
        assert getFragmentManager() != null;
        Bundle bundle = new Bundle();
        bundle.putString("user", user);
        fragmentProfileInfo.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left);
        fragmentTransaction.add(R.id.main_container, fragmentProfileInfo, "FRAGMENT_PROFILE_INFO");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    //удаление элемента(по нажатию на Confirm)
    private void deleteItem(long positionInList) {

        comeRequest_list.remove((int) positionInList);
        adapterRequests.notifyItemRemoved((int) positionInList);
        adapterRequests.notifyDataSetChanged();

        refreshData();

    }

    //удаление элемента(по нажатию на Delete)
    @Override
    public void deleteItemFromList(long position) {

        comeRequest_list.remove((int) position);
        adapterRequests.notifyItemRemoved((int) position);
        adapterRequests.notifyDataSetChanged();

        refreshData();

        Toast.makeText(getContext(), "Complete delete", Toast.LENGTH_SHORT).show();

    }
}
