package com.example.project.setname.Fragments.Jobs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.project.setname.Fragments.Jobs.FragmentJobAvailable.FragmentJobAvailable;
import com.example.project.setname.Fragments.Jobs.FromEmloyer.FragmentJobFromEmployer;
import com.example.project.setname.Fragments.Jobs.FromMaker.FragmentJobFromMaker;
import com.example.project.setname.R;
import com.gigamole.navigationtabstrip.NavigationTabStrip;

public class FragmentJob extends Fragment {

    private View view;

    private ViewPager mViewPager;

    private FragmentJobFromEmployer fragmentJobFromEmployer;
    private FragmentJobFromMaker fragmentJobFromMaker;
    private FragmentJobAvailable fragmentJobAvailable;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        if (view==null) {

            view = inflater.inflate(R.layout.fragment_jobs, container, false);

            fragmentJobFromEmployer = new FragmentJobFromEmployer();
            fragmentJobFromMaker = new FragmentJobFromMaker();
            fragmentJobAvailable = new FragmentJobAvailable();

            mViewPager = (ViewPager) view.findViewById(R.id.vp);

            final NavigationTabStrip navigationTabStrip = (NavigationTabStrip) view.findViewById(R.id.nts);
            navigationTabStrip.setTitles("Available job", "Maker", "Employer");

            navigationTabStrip.setStripWeight(3);

            mViewPager.setAdapter(new MyPagerAdapter(getFragmentManager()));

            navigationTabStrip.setViewPager(mViewPager);

        }

    return view;

    }

    @Override
    public void onDestroyView() {
        if (view.getParent() != null) {
            ((ViewGroup)view.getParent()).removeView(view);
        }
        super.onDestroyView();
    }

    //смена фрагмента в ViewPager
    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch(pos) {

                case 0: return fragmentJobAvailable;
                case 1: return fragmentJobFromMaker;
                case 2: return fragmentJobFromEmployer;
                default: return getTargetFragment();
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

}
