package com.example.project.setname.Fragments.Jobs.FragmentJobAvailable;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.project.setname.Dialogs.DialogSendFromAvailableToRequests;
import com.example.project.setname.Fragments.Jobs.FragmentJob;
import com.example.project.setname.Fragments.Jobs.JobNew.FragmentNewJob;
import com.example.project.setname.Interfaces.JobAvailableOpenDialog;
import com.example.project.setname.Interfaces.OpenProfileInfo;
import com.example.project.setname.ProfileInfo.FragmentProfileInfo;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FragmentJobAvailable extends Fragment implements JobAvailableOpenDialog, OpenProfileInfo, DialogSendFromAvailableToRequests.DeleteListenerAvailable {

    /**
     * Логика:
     * <p>
     * Ставим вьюшки, все подготавливаем
     * Сгружаем списочек того, на что уже были отправлены заявки, если есть совпадение, то не грузим
     * Так же есть на работе стоит флаг "available" = false, то не грузим, так как работа находится на этапе making
     */

    @Override
    public void getStateOpenedFull(long positionInList, boolean thisState) {
        if (thisState) {

            deleteItem(positionInList);

        }
    }

    private static final String TAG = "FragJobAvail";

    private View view;

    private static ArrayList<String> usedList = new ArrayList<>();

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;
    private String current_user;


    private List<ModelJobAvailable> jobs_list;
    private RecyclerView jobs_list_view;
    private AdapterJobAvailable adapterJobAvailable;

    private FragmentProfileInfo fragmentProfileInfo;

    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean loadedNotShowJobList = false;
    private boolean loadMore = true;
    private boolean isLoadMore = true;

    /*private CollectionReference collectionReference;//выборка не работает :/
    private Query isAvailable;//выборка не работает :/*/

    private DocumentSnapshot lastVisible;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {

            view = inflater.inflate(R.layout.fragment_job_available, container, false);

            setFirebase();

            if (current_user != null) {

                setViews();
                setInteractions();
                setClasses();
                setRV();
                loadPosts();

            }
        }

        return view;

    }

    private void setInteractions() {

        swipeRefreshLayout.setOnRefreshListener(() -> {

            refreshData();

            swipeRefreshLayout.setRefreshing(false);

        });

        jobs_list_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (isLoadMore) {
                    loadPosts();
                }

            }
        });

    }

    private void setRV() {

        adapterJobAvailable = new AdapterJobAvailable(jobs_list, this, this);

        jobs_list_view.setLayoutManager(new LinearLayoutManager(getContext()));
        jobs_list_view.setAdapter(adapterJobAvailable);

    }

    private void setFirebase() {

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();
        current_user = firebaseAuth.getCurrentUser().getUid();

        /*collectionReference = firebaseFirestore.collection("Jobs");
        isAvailable = collectionReference.orderBy("timestamp", Query.Direction.DESCENDING).whereEqualTo("available", true);*/

    }

    @Override
    public void onDestroyView() {
        if (view.getParent() != null) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
        super.onDestroyView();
    }

    private void refreshData() {

        jobs_list.clear();
        usedList.clear();
        loadedNotShowJobList = false;
        loadMore = true;
        isLoadMore = true;

        loadPosts();

    }

    private void loadPosts() {

        firebaseFirestore.collection("Users")
                .document(current_user)
                .collection("NeverShow(Maker)")
                .addSnapshotListener((documentSnapshots1, e1) -> {

                    if (!loadedNotShowJobList) {
                        assert documentSnapshots1 != null;
                        for (DocumentChange doc : documentSnapshots1.getDocumentChanges()) {
                            usedList.add(doc.getDocument().getId());
                        }
                        loadedNotShowJobList = true;
                    }
                    /*if (!usedList.isEmpty()) {*/

                    if (loadMore) {

                        firebaseFirestore.collection("Jobs")
                                .orderBy("timestamp", Query.Direction.DESCENDING)
                                .limit(10)
                                .addSnapshotListener((documentSnapshots, e) -> {

                                    try {
                                        if (documentSnapshots.size() > 0) {

                                            for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {

                                                if (doc.getType() == DocumentChange.Type.ADDED) {

                                                    if (!usedList.contains(doc.getDocument().getId())
                                                            && !Objects.equals(doc.getDocument().getString("user_id"), current_user)
                                                            && doc.getDocument().getBoolean("available")) {

                                                        String jobsId = doc.getDocument().getId();

                                                        ModelJobAvailable modelJobAvailable = doc.getDocument()
                                                                .toObject(ModelJobAvailable.class)
                                                                .withId(jobsId);
                                                        jobs_list.add(modelJobAvailable);
                                                        adapterJobAvailable.notifyDataSetChanged();
                                                    }

                                                }
                                            }

                                            lastVisible = documentSnapshots.getDocuments().get(documentSnapshots.size() - 1);

                                        }
                                    } catch (NullPointerException ignored) {
                                    }

                                });
                        loadMore = false;

                    } else {

                        firebaseFirestore.collection("Jobs")
                                .orderBy("timestamp", Query.Direction.DESCENDING)
                                .startAfter(lastVisible)
                                .limit(10)
                                .addSnapshotListener((documentSnapshots, e) -> {

                                    assert documentSnapshots != null;
                                    try {
                                        if (documentSnapshots.size() > 0) {

                                            if (lastVisible != documentSnapshots.getDocuments().get(documentSnapshots.size() - 1)) {

                                                lastVisible = documentSnapshots.getDocuments().get(documentSnapshots.size() - 1);

                                                for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {

                                                    if (doc.getType() == DocumentChange.Type.ADDED) {

                                                        if (!usedList.contains(doc.getDocument().getId())
                                                                && !Objects.equals(doc.getDocument().getString("user_id"), current_user) &&
                                                                doc.getDocument().getBoolean("available")) {

                                                            String jobsId = doc.getDocument().getId();

                                                            ModelJobAvailable modelJobAvailable = doc.getDocument()
                                                                    .toObject(ModelJobAvailable.class)
                                                                    .withId(jobsId);
                                                            jobs_list.add(modelJobAvailable);
                                                            adapterJobAvailable.notifyDataSetChanged();
                                                        }

                                                    }
                                                }
                                            } else {
                                                isLoadMore = false;
                                            }

                                        } else {
                                            isLoadMore = false;
                                        }
                                    } catch (NullPointerException e2) {
                                    }
                                });

                    }
                    /*}*/
                    /*else {
                        Toast.makeText(getContext(), "Empty list" + usedList.size(), Toast.LENGTH_SHORT).show();
                    }*/
                });
    }

    void setViews() {

        swipeRefreshLayout = view.findViewById(R.id.fragment_fragment_job_job_available_swipe_refresh);
        jobs_list_view = view.findViewById(R.id.job_available_view_recycler);

    }

    void setClasses() {

        fragmentProfileInfo = new FragmentProfileInfo();
        jobs_list = new ArrayList<>();

    }

    @Override
    public void openDialog(String postId, String employer, String price, String date, long idIndex) {

        DialogSendFromAvailableToRequests fragment = new DialogSendFromAvailableToRequests();
        Bundle bundle = new Bundle();
        bundle.putString("employer", employer);
        bundle.putString("price", price);
        bundle.putString("deadline", date);
        bundle.putString("postId", postId);
        bundle.putLong("position", idIndex);
        fragment.setArguments(bundle);
        fragment.setTargetFragment(FragmentJobAvailable.this, 1);
        assert getFragmentManager() != null;
        fragment.show(getFragmentManager(), "DIALOG_FROM_AVAILABLE_TO_REQUESTS");

    }

    @Override
    public void openProfile(String user) {
        assert getFragmentManager() != null;
        Bundle bundle = new Bundle();
        bundle.putString("user", user);
        fragmentProfileInfo.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left);
        fragmentTransaction.add(R.id.main_container, fragmentProfileInfo, "FRAGMENT_PROFILE_INFO");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void deleteItem(long positionInList) {

        /*int previousContentSize = jobs_list.size();
        int newContentSize = previousContentSize - 1;*/

        /*jobs_list.remove((int) positionInList);
        adapterJobAvailable.notifyItemRemoved((int) positionInList);*/

        /*adapterJobAvailable.notifyItemRangeRemoved((int)positionInList, previousContentSize);*/
        refreshData();

    }
}
