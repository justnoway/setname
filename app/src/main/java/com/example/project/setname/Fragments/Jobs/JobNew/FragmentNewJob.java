package com.example.project.setname.Fragments.Jobs.JobNew;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.dpizarro.uipicker.library.picker.PickerUI;
import com.dpizarro.uipicker.library.picker.PickerUISettings;
import com.example.project.setname.Dialogs.DialogDatePicker;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Logger;

import in.shadowfax.proswipebutton.ProSwipeButton;

public class FragmentNewJob extends Fragment {

    private static final String TAG = "FragmentNewJob";

    private View view;

    private ProSwipeButton proSwipeBtn;
    private TextView typeOfJob;
    private TextView language;
    private TextView payment;
    private TextView level;
    private EditText price;
    private EditText description;
    private TextView dateView;
    private List<String> options;
    private PickerUI mPickerUI;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    int typeOfJobPosition = 0;
    int languagePosition = 0;
    int paymentPosition = 0;
    int levelPosition = 0;

    public static AtomicReference<Date> date = new AtomicReference<>();
    public static AtomicReference<String> strDate = new AtomicReference<>();

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_job_new, container, false);

        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();

        setViews();

        setAllOnClickListener();

        return view;
    }

    boolean controlTest() {

        return testTV(typeOfJob) &&
                testTV(language) &&
                testTV(payment) &&
                testTV(level) &&
                !date.get().toString().equals("None") &&
                !description.getText().toString().equals("");


    }

    boolean testTV(TextView textView) {
        return !textView.getText().toString().equals("None");
    }

    void setViews() {

        mPickerUI = (PickerUI) view.findViewById(R.id.picker_ui_view);
        dateView = view.findViewById(R.id.deadline_select);
        typeOfJob = view.findViewById(R.id.type_of_job_select);
        language = view.findViewById(R.id.language_select);
        payment = view.findViewById(R.id.payment_select);
        level = view.findViewById(R.id.level_select);
        description = view.findViewById(R.id.description_data);
        price = view.findViewById(R.id.price_field);
        proSwipeBtn = (ProSwipeButton) view.findViewById(R.id.send_data);

        Log.i(TAG, "Complete set view");

    }

    void setAllOnClickListener() {

        setDataViewClickListener();
        setTypeOfJobClickListener();
        setLanguageClickListener();
        setPaymentClickListener();
        setLevelClickListener();
        setProSwipeBtnClickListener();

        Log.i(TAG, "Complete set all views");

    }

    void setDataViewClickListener() {

        dateView.setOnClickListener(v -> {

            DialogDatePicker dateDialog = new DialogDatePicker();
            Bundle bundle = new Bundle();
            bundle.putString("view", "FRAGMENT_NEW_JOB");
            dateDialog.setArguments(bundle);
            dateDialog.show(getFragmentManager(), "DATA_PICKER");

        });

    }

    void setTypeOfJobClickListener() {

        typeOfJob.setOnClickListener(v -> {

            options = Arrays.asList(getResources().getStringArray(R.array.type_of_job));

            PickerUISettings pickerUISettings = new PickerUISettings.Builder()
                    .withItems(options)
                    .withAutoDismiss(true)
                    .withItemsClickables(false)
                    .withUseBlur(false)
                    .build();

            mPickerUI.setSettings(pickerUISettings);

            mPickerUI.slide(typeOfJobPosition);

            mPickerUI.setOnClickItemPickerUIListener((which, position, valueResult) -> {

                typeOfJob.setText(valueResult);
                typeOfJobPosition = position;

            });


        });
    }

    void setLanguageClickListener(){
        language.setOnClickListener(v -> {

            options = Arrays.asList(getResources().getStringArray(R.array.language));

            PickerUISettings pickerUISettings = new PickerUISettings.Builder()
                    .withItems(options)
                    .withAutoDismiss(true)
                    .withItemsClickables(false)
                    .withUseBlur(false)
                    .build();

            mPickerUI.setSettings(pickerUISettings);

            mPickerUI.slide(languagePosition);

            mPickerUI.setOnClickItemPickerUIListener((which, position, valueResult) -> {

                language.setText(valueResult);
                languagePosition = position;

            });

        });
    }

    void setPaymentClickListener(){

        payment.setOnClickListener(v -> {

            options = Arrays.asList(getResources().getStringArray(R.array.payment));

            PickerUISettings pickerUISettings = new PickerUISettings.Builder()
                    .withItems(options)
                    .withAutoDismiss(true)
                    .withItemsClickables(false)
                    .withUseBlur(false)
                    .build();

            mPickerUI.setSettings(pickerUISettings);

            mPickerUI.slide(paymentPosition);

            mPickerUI.setOnClickItemPickerUIListener(new PickerUI.PickerUIItemClickListener() {
                @Override
                public void onItemClickPickerUI(int which, int position, String valueResult) {

                    payment.setText(valueResult);
                    paymentPosition = position;

                }
            });

        });

    }

    void setLevelClickListener(){

        level.setOnClickListener(v -> {

            options = Arrays.asList(getResources().getStringArray(R.array.level));

            PickerUISettings pickerUISettings = new PickerUISettings.Builder()
                    .withItems(options)
                    .withAutoDismiss(true)
                    .withItemsClickables(false)
                    .withUseBlur(false)
                    .build();

            mPickerUI.setSettings(pickerUISettings);

            mPickerUI.slide(levelPosition);

            mPickerUI.setOnClickItemPickerUIListener(new PickerUI.PickerUIItemClickListener() {
                @Override
                public void onItemClickPickerUI(int which, int position, String valueResult) {

                    level.setText(valueResult);
                    levelPosition = position;

                }
            });

            Toast.makeText(getContext(), levelPosition + "", Toast.LENGTH_SHORT).show();

        });

    }

    void setProSwipeBtnClickListener(){

        proSwipeBtn.setOnSwipeListener(() -> {
            new Handler().postDelayed(() -> {

                if (controlTest()) {

                    HashMap<String, Object> map = new HashMap<>();

                    map.put("deadline", date.get());
                    map.put("description", description.getText().toString());
                    map.put("language", language.getText().toString());
                    map.put("level", level.getText().toString());
                    map.put("payment", payment.getText().toString());
                    map.put("price", price.getText().toString());
                    map.put("user_id", Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid());
                    map.put("timestamp", FieldValue.serverTimestamp());
                    map.put("available", true);

                    switch (typeOfJob.getText().toString()) {
                        case "Per hour":
                            map.put("type", "p/h");
                            break;
                        case "Fixed price":
                            map.put("type", "fix");
                            break;
                        case "Exchange(beta)":
                            map.put("type", "trade");
                            break;
                    }


                    firebaseFirestore.collection("Jobs")
                            .add(map)
                            .addOnCompleteListener(task -> {

                                if (task.isComplete()) {

                                    proSwipeBtn.showResultIcon(true);
                                    getFragmentManager().popBackStack();

                                    Log.e(TAG, "Post was added");

                                }else {

                                    Log.e(TAG, "Post wasn't added");

                                }

                            });

                } else {

                    proSwipeBtn.showResultIcon(false);

                }

            }, 2000);
        });

    }

}
