package com.example.project.setname.Fragments.Events.AvailableEvents;

import com.google.firebase.firestore.GeoPoint;

import java.util.Date;

public class ModelEventAvailable extends ModelEventAvailableId{

    String user_id, description, link, price, languages, type, direction, headline, image_url;

    Date timeStart, timeEnd;

    GeoPoint geopoint;

    public ModelEventAvailable(){

    }

    public ModelEventAvailable(String description) {
        this.description = description;
    }

    public ModelEventAvailable(String user_id, String price, String type, String headline, Date timeStart, Date timeEnd, String image_url) {
        this.user_id = user_id;
        this.price = price;
        this.type = type;
        this.headline = headline;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.image_url = image_url;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(Date timeStart) {
        this.timeStart = timeStart;
    }

    public Date getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(Date timeEnd) {
        this.timeEnd = timeEnd;
    }

    public GeoPoint getGeopoint() {
        return geopoint;
    }

    public void setGeopoint(GeoPoint geopoint) {
        this.geopoint = geopoint;
    }
}
