package com.example.project.setname.Interfaces;

import android.view.View;

public interface RecyclerViewClickListener {
    void recyclerViewListClicked(View v, int position);
}
