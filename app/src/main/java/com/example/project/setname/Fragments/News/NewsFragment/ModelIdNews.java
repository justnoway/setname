package com.example.project.setname.Fragments.News.NewsFragment;

import android.support.annotation.NonNull;

import com.google.firebase.firestore.Exclude;

public class ModelIdNews {

    @Exclude
    public String NewsConstructorId;

    public <T extends ModelIdNews> T withId(@NonNull final String id){
        this.NewsConstructorId = id;
        return (T) this;
    }
}
