package com.example.project.setname.Dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import fr.tvbarthel.lib.blurdialogfragment.SupportBlurDialogFragment;
import in.shadowfax.proswipebutton.ProSwipeButton;

public class DialogSendFromRequestsToReconciliations extends SupportBlurDialogFragment {

    private TextView showPriceOrNot;
    private EditText priceView;
    private EditText mustToDoView;

    protected static TextView dateView; //иуда не ищет в DDP эту вьюшку, пришлось сделать ее глобальной

    public static AtomicReference<Date> dataDate = new AtomicReference<>();
    public static AtomicReference<String> saveDate = new AtomicReference<>();

    private static final String TAG = "DialogSFRTR";

    private static AtomicReference<String> typeOfJob = new AtomicReference<>();

    public interface DeleteListener {
        void getState(long positionInList, boolean thisState);
    }

    DeleteListener mDeleteListener;

    //переделать на нормальный вид
    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;
    private String current_user;

    private static final String BUNDLE_KEY_DOWN_SCALE_FACTOR = "bundle_key_down_scale_factor";

    private static final String BUNDLE_KEY_BLUR_RADIUS = "bundle_key_blur_radius";

    private static final String BUNDLE_KEY_DIMMING = "bundle_key_dimming_effect";

    private static final String BUNDLE_KEY_DEBUG = "bundle_key_debug_effect";

    public static DialogSendFromAvailableToRequests newInstance(int radius,
                                                                float downScaleFactor,
                                                                boolean dimming,
                                                                boolean debug) {
        DialogSendFromAvailableToRequests fragment = new DialogSendFromAvailableToRequests();
        Bundle args = new Bundle();
        args.putInt(
                BUNDLE_KEY_BLUR_RADIUS,
                radius
        );
        args.putFloat(
                BUNDLE_KEY_DOWN_SCALE_FACTOR,
                downScaleFactor
        );
        args.putBoolean(
                BUNDLE_KEY_DIMMING,
                dimming
        );
        args.putBoolean(
                BUNDLE_KEY_DEBUG,
                debug
        );

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {

        try {
            mDeleteListener = (DeleteListener) getTargetFragment();
        } catch (ClassCastException e) {
            Log.e(TAG, "onAttach: ClassCastException: " + e.getMessage());
        }

        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        assert getArguments() != null;


        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();
        current_user = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        @SuppressLint("InflateParams") View view = Objects.requireNonNull(getActivity()).getLayoutInflater().inflate(R.layout.dialog_from_request_to_reconciliation, null);
        showPriceOrNot = view.findViewById(R.id.dialog_from_request_to_reconciliation_tv_price);
        final ProSwipeButton proSwipeBtn = (ProSwipeButton) view.findViewById(R.id.dialog_from_request_to_reconciliation_sb_send);
        priceView = view.findViewById(R.id.dialog_from_request_to_reconciliation_et_price);
        dateView = view.findViewById(R.id.dialog_from_request_to_reconciliation_et_date);
        mustToDoView = view.findViewById(R.id.dialog_from_request_to_reconciliation_et_must_to_do);

        dateView.setOnClickListener(v -> {

            DialogDatePicker dateDialog = new DialogDatePicker();
            Bundle bundle = new Bundle();
            bundle.putString("view", "DIALOG_SEND_FROM_REQUESTS_TO_RECONCILIATIONS");
            dateDialog.setArguments(bundle);
            dateDialog.show(getFragmentManager(), "DATA_PICKER");

        });

        assert getArguments() != null;

        String id_data = getArguments().getString("id");

        AtomicReference<String> language = new AtomicReference<>();

        assert id_data != null;

        if (controlTestLanguage(getArguments().getString("price"))) {
            priceView.setText(getArguments().getString("price"));
            showPriceOrNot.setText("Exchange to");
        }

        firebaseFirestore.collection("Users")
                .document(current_user)
                .collection("ReceivedRequestsForJob(Employer)")
                .document(id_data).get().addOnCompleteListener(task -> {

            if (task.isComplete()) {

                Date time = task.getResult().getDate("deadline");

                dataDate.set(time);
                assert time != null;
                long millisecond = time.getTime();
                String dateString = android.text.format.DateFormat.format("MMMM dd", new Date(millisecond)).toString();
                dateView.setText(dateString);

                String dateStringForDialog = android.text.format.DateFormat.format("dd MM yyyy", new Date(millisecond)).toString();
                saveDate.set(dateStringForDialog);

            }

        });

        firebaseFirestore.collection("Jobs")
                .document(id_data)
                .get()
                .addOnSuccessListener(task -> {
                    mustToDoView.setText(task.getString("description"));
                    language.set(task.getString("language"));
                    typeOfJob.set(task.getString("type"));
                });

        proSwipeBtn.setOnSwipeListener(() -> {
            new Handler().postDelayed(() -> {

                assert id_data != null;

                HashMap<String, Object> time = new HashMap<>();

                time.put("deadline", dataDate.get());
                time.put("mustToDo", mustToDoView.getText().toString());
                time.put("language", language.get());
                time.put("price", priceView.getText().toString());
                time.put("name", "Enter what do you want...");
                time.put("confirm", false);
                time.put("type", typeOfJob.get());

                firebaseFirestore.collection("Users")
                        .document(current_user)
                        .collection("ReceivedRequestsForJob(Employer)")
                        .document(id_data)
                        .delete()
                        .addOnSuccessListener(task -> {

                        });

                firebaseFirestore.collection("Users")
                        .document(Objects.requireNonNull(getArguments().getString("maker")))
                        .collection("SentRequestsForJob(Maker)")
                        .document(id_data)
                        .delete()
                        .addOnSuccessListener(task -> {

                        });

                time.put("employer_id", current_user);

                firebaseFirestore.collection("Users")
                        .document(Objects.requireNonNull(getArguments().getString("maker")))
                        .collection("Reconciliations(Maker)")
                        .document(id_data)
                        .set(time)
                        .addOnSuccessListener(task -> {

                        });

                time.remove("employer_id");
                time.put("maker_id", getArguments().getString("maker"));

                firebaseFirestore.collection("Users")
                        .document(current_user)
                        .collection("Reconciliations(Employer)")
                        .document(id_data)
                        .set(time)
                        .addOnSuccessListener(task -> {

                        });

                Map<String, Object> statusMap = new HashMap<>();
                statusMap.put("timestamp", FieldValue.serverTimestamp());
                statusMap.put("text", "Confirm request from employer");

                firebaseFirestore.collection("Users")
                        .document(Objects.requireNonNull(getArguments().getString("maker")))
                        .collection("Reconciliations(Maker)")
                        .document(id_data)
                        .collection("Status")
                        .add(statusMap)
                        .addOnCompleteListener(task -> {


                });

                firebaseFirestore.collection("Users")
                        .document(current_user)
                        .collection("Reconciliations(Employer)")
                        .document(id_data)
                        .collection("Status")
                        .add(statusMap)
                        .addOnSuccessListener(task -> {

                        });

                mDeleteListener.getState(getArguments().getLong("position"), true);

                proSwipeBtn.showResultIcon(true);

                getDialog().dismiss();


            }, 2000);
        });


        builder.setView(view);
        return builder.create();
    }

    boolean controlTestLanguage(String price) {
        return price.equals("Java") || !price.equals("Web") && !price.equals("Mobile") && !price.equals("Python") && !price.equals("Swift");
    }

}

