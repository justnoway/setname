package com.example.project.setname.Chat;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.project.setname.Interfaces.OpenProfileInfo;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterChat extends RecyclerView.Adapter<AdapterChat.ViewHolder> {

    //forAdapter
    private List<ModelChat> chat_list;
    public Context context;

    //service
    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    //interfaces
    private OpenProfileInfo mOpenProfileInfo;

    public AdapterChat(List<ModelChat> chat_list,
                       OpenProfileInfo openProfileInfo) {
        this.chat_list = chat_list;
        this.mOpenProfileInfo = openProfileInfo;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
        context = parent.getContext();
        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        holder.setBothInvisible();

        String current_user = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        String text_data = chat_list.get(position).getText();

        String previousSender = chat_list.get(position).ModeChatModelChatPrevious;
        String sender_data = chat_list.get(position).getSender();
        if (sender_data.equals(current_user)) {
            holder.setText(true, text_data);
            holder.setFieldForText(true);
            firebaseFirestore.collection("Users").document(current_user).get().addOnCompleteListener(task -> {

                if (task.isSuccessful()) {

                    String userImage = task.getResult().getString("image");
                    holder.setUserImage(true, userImage, sender_data, previousSender, current_user);

                }
            });

        } else {
            holder.setText(false, text_data);
            holder.setFieldForText(false);
            firebaseFirestore.collection("Users").document(sender_data).get().addOnCompleteListener(task -> {

                if (task.isSuccessful()) {

                    String userImage = task.getResult().getString("image");
                    holder.setUserImage(false, userImage, sender_data, previousSender, sender_data  );

                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return chat_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private View mView;

        private CircleImageView userCurrent;
        private CircleImageView userCompanion;

        private ConstraintLayout fieldForText;
        private TextView text;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;

            text = mView.findViewById(R.id.chat_text);
            userCurrent = mView.findViewById(R.id.chat_image_current_user);
            userCompanion = mView.findViewById(R.id.chat_image_companion);
        }

        void setUserImage(boolean current_user, String image, String cur, String prev, String userForProfileInfo) {
            if (prev != null) {
                if (cur.equals(prev)) {
                    userCurrent.setVisibility(View.GONE);
                    userCompanion.setVisibility(View.GONE);
                } else if (current_user) {
                    userCurrent.setVisibility(View.VISIBLE);
                    userCompanion.setVisibility(View.GONE);
                    userCurrent.setOnClickListener(v -> {
                        mOpenProfileInfo.openProfile(userForProfileInfo);
                    });
                    RequestOptions placeholderOption = new RequestOptions();
                    placeholderOption.placeholder(R.drawable.profile_placeholder);
                    Glide.with(context).applyDefaultRequestOptions(placeholderOption).load(image).into(userCurrent);
                } else {
                    userCompanion.setVisibility(View.VISIBLE);
                    userCurrent.setVisibility(View.GONE);
                    userCompanion.setOnClickListener(v -> {
                        mOpenProfileInfo.openProfile(userForProfileInfo);
                    });
                    RequestOptions placeholderOption = new RequestOptions();
                    placeholderOption.placeholder(R.drawable.profile_placeholder);
                    Glide.with(context).applyDefaultRequestOptions(placeholderOption).load(image).into(userCompanion);
                }
            }else {
                if (current_user) {
                    userCurrent.setVisibility(View.VISIBLE);
                    userCompanion.setVisibility(View.GONE);
                    userCurrent.setOnClickListener(v -> {
                        mOpenProfileInfo.openProfile(userForProfileInfo);
                    });
                    RequestOptions placeholderOption = new RequestOptions();
                    placeholderOption.placeholder(R.drawable.profile_placeholder);
                    Glide.with(context).applyDefaultRequestOptions(placeholderOption).load(image).into(userCurrent);
                } else {
                    userCompanion.setVisibility(View.VISIBLE);
                    userCurrent.setVisibility(View.GONE);
                    userCompanion.setOnClickListener(v -> {
                        mOpenProfileInfo.openProfile(userForProfileInfo);
                    });
                    RequestOptions placeholderOption = new RequestOptions();
                    placeholderOption.placeholder(R.drawable.profile_placeholder);
                    Glide.with(context).applyDefaultRequestOptions(placeholderOption).load(image).into(userCompanion);
                }
            }
        }

        void setFieldForText(boolean current_user) {
            fieldForText = mView.findViewById(R.id.chat_text_field);
            if (current_user) {
                fieldForText.setBackgroundResource(R.drawable.chat_field_from_current_user);
            } else {
                fieldForText.setBackgroundResource(R.drawable.chat_field_from_companion);
            }
        }

        void setBothInvisible(){
            userCurrent.setVisibility(View.INVISIBLE);
            userCompanion.setVisibility(View.INVISIBLE);
        }

        void setText(boolean current_user, String str) {

            if (current_user) {
                text.setTextColor(Color.parseColor("#FFFFFF"));
            } else {
                text.setTextColor(Color.parseColor("#212121"));
            }
            text.setText(str);

        }
    }
}
