package com.example.project.setname.Dialogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import com.example.project.setname.Fragments.Jobs.FullJob.FragmentJobFullOpenedItem;
import com.example.project.setname.Fragments.Jobs.JobNew.FragmentNewJob;
import com.example.project.setname.R;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class DialogDatePicker extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    /**
     * Да, тут все через глобальные переменные и да так нельзя, но с интерфейсами нет времени работать
     * После второго этпапа доделаю
     * <p>
     * Логика:
     * В диалог приходит Tag вьюшки
     * Если один, то получаем данные из его фрагмента
     * Парсим на day, month, year
     * Забиваем минимум(минимальная дата) в диалог
     * Ловим дату с диалога и забивает на отображение первоначального фрагмента формат "MMMM dd"
     * Забиваем в AtomicReference<Date> для загрузки на сервер и AtomicReference<String> для последующего открытия этого диалога
     */

    private static final String TAG = "DialogDatePicker";

    private TextView tv;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        if (getArguments().getString("view").equals("FRAGMENT_NEW_JOB")) {

            if (FragmentNewJob.strDate.get() == null) {

                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog picker = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this,
                        year, month, day);

                Date today = new Date();
                c.setTime(today);
                /*c.add(Calendar.DAY_OF_MONTH, +1);*/
                long minDate = c.getTime().getTime();

                picker.getDatePicker().setMinDate(minDate);

                return picker;

            } else {

                String[] dateParts = FragmentNewJob.strDate.get().split(" ");
                String day = dateParts[0];
                String month = dateParts[1];
                String year = dateParts[2];

                Log.i(TAG, "Day: " + day + "\n Month: " + month + "\n Year: " + year);

                DatePickerDialog picker = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this,
                        Integer.valueOf(year), Integer.valueOf(month) - 1, Integer.valueOf(day));

                Date today = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(today);
                long minDate = c.getTime().getTime();


                picker.getDatePicker().setMinDate(minDate);

                return picker;
            }
        } else if (getArguments().getString("view").equals("FRAGMENT_JOB_FULL_OPENED_ITEM")) {

            Log.i(TAG, "Date: " + FragmentJobFullOpenedItem.strDeadline.get());

            String[] dateParts = FragmentJobFullOpenedItem.strDeadline.get().split(" ");
            String day = dateParts[0];
            String month = dateParts[1];
            String year = dateParts[2];

            Log.i(TAG, "Day: " + day + "\n Month: " + month + "\n Year: " + year);

            DatePickerDialog picker = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this,
                    Integer.valueOf(year), Integer.valueOf(month) - 1, Integer.valueOf(day));

            Date today = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(today);
            /*c.add(Calendar.DAY_OF_MONTH, +1);*/
            long minDate = c.getTime().getTime();


            picker.getDatePicker().setMinDate(minDate);

            return picker;
        } else if (getArguments().getString("view").equals("DIALOG_SEND_FROM_AVAILABLE_TO_RECONCILIATION")){

            String[] dateParts = DialogSendFromAvailableToRequests.saveDate.get().split(" ");
            String day = dateParts[0];
            String month = dateParts[1];
            String year = dateParts[2];

            Log.i(TAG, "Day: " + day + "\n Month: " + month + "\n Year: " + year);

            DatePickerDialog picker = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this,
                    Integer.valueOf(year), Integer.valueOf(month) - 1, Integer.valueOf(day));

            Date today = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(today);
            long minDate = c.getTime().getTime();


            picker.getDatePicker().setMinDate(minDate);

            return picker;

        }
        else /*"DIALOG_SEND_FROM_REQUESTS_TO_RECONCILIATIONS"*/ {

            String[] dateParts = DialogSendFromRequestsToReconciliations.saveDate.get().split(" ");
            String day = dateParts[0];
            String month = dateParts[1];
            String year = dateParts[2];

            Log.i(TAG, "Day: " + day + "\n Month: " + month + "\n Year: " + year);

            DatePickerDialog picker = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this,
                    Integer.valueOf(year), Integer.valueOf(month) - 1, Integer.valueOf(day));

            Date today = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(today);
            long minDate = c.getTime().getTime();


            picker.getDatePicker().setMinDate(minDate);

            return picker;

        }

    }

    @Override
    public void onStart() {
        super.onStart();
        // добавляем кастомный текст для кнопки
        Button nButton = ((AlertDialog) getDialog())
                .getButton(DialogInterface.BUTTON_POSITIVE);
        nButton.setText("Take it");


    }

    @Override
    public void onDateSet(android.widget.DatePicker datePicker, int year,
                          int month, int day) {

        assert getArguments() != null;

        Log.i(TAG, "Fragment is: " + getArguments().getString("view"));

        if (Objects.equals(getArguments().getString("view"), "FRAGMENT_NEW_JOB")) {

            tv = (TextView) Objects.requireNonNull(getActivity()).findViewById(R.id.deadline_select);

            try {
                //НЕ ТРОГАТЬ "month+1": неправильно выставляет месяц
                String str = day + " " + (month + 1) + " " + year;
                @SuppressLint("SimpleDateFormat") SimpleDateFormat dt = new SimpleDateFormat("MM dd yyyy");
                FragmentNewJob.date.set(dt.parse(str));
                String strForDate = (month + 1) + " " + day + " " + year;
                Date date = dt.parse(strForDate);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat dt1 = new SimpleDateFormat("MMMM dd yyyy");
                tv.setText(dt1.format(date));

                FragmentNewJob.strDate.set(str);

            } catch (ParseException e) {
                e.printStackTrace();
            }

        } else if (Objects.equals(getArguments().getString("view"), "FRAGMENT_JOB_FULL_OPENED_ITEM")) {

            tv = (TextView) Objects.requireNonNull(getActivity()).findViewById(R.id.fragment_job_full_rec_and_mak_item_deadline);

            try {
                //НЕ ТРОГАТЬ "month+1": неправильно выставляет месяц
                String str = day + " " + (month + 1) + " " + year;
                @SuppressLint("SimpleDateFormat") SimpleDateFormat dt = new SimpleDateFormat("dd MM yyyy");
                FragmentJobFullOpenedItem.date.set(dt.parse(str));
                FragmentJobFullOpenedItem.strDeadline.set(str);
                Date date = dt.parse(str);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat dt1 = new SimpleDateFormat("MMMM dd");
                tv.setText(dt1.format(date));

            } catch (ParseException e) {
                e.printStackTrace();
            }

        } else if (Objects.equals(getArguments().getString("view"), "DIALOG_SEND_FROM_AVAILABLE_TO_RECONCILIATION")) {

            try {
                //НЕ ТРОГАТЬ "month+1": неправильно выставляет месяц
                String str = day + " " + (month + 1) + " " + year;
                @SuppressLint("SimpleDateFormat") SimpleDateFormat dt = new SimpleDateFormat("dd MM yyyy");
                DialogSendFromAvailableToRequests.dataDate.set(dt.parse(str));
                DialogSendFromAvailableToRequests.saveDate.set(str);
                Date date = dt.parse(str);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat dt1 = new SimpleDateFormat("MMMM dd");
                DialogSendFromAvailableToRequests.dateView.setText(dt1.format(date));

            } catch (ParseException e) {
                e.printStackTrace();
            }

        } else if (Objects.equals(getArguments().getString("view"), "DIALOG_SEND_FROM_REQUESTS_TO_RECONCILIATIONS")){

            try {
                //НЕ ТРОГАТЬ "month+1": неправильно выставляет месяц
                String str = day + " " + (month + 1) + " " + year;
                @SuppressLint("SimpleDateFormat") SimpleDateFormat dt = new SimpleDateFormat("dd MM yyyy");
                DialogSendFromRequestsToReconciliations.dataDate.set(dt.parse(str));
                DialogSendFromRequestsToReconciliations.saveDate.set(str);
                Date date = dt.parse(str);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat dt1 = new SimpleDateFormat("MMMM dd");
                DialogSendFromRequestsToReconciliations.dateView.setText(dt1.format(date));

            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

    }
}
