package com.example.project.setname.Fragments.Jobs.FromEmloyer;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.project.setname.Fragments.AdditionalViews.FragmentEmptyList;
import com.example.project.setname.Fragments.Jobs.FullJob.FragmentJobFullFromMaking;
import com.example.project.setname.Fragments.Jobs.Reconciliations.AdapterJobReconciliation;
import com.example.project.setname.Fragments.Jobs.Reconciliations.ModelJobReconciliation;
import com.example.project.setname.Interfaces.JobOpenChat;
import com.example.project.setname.Interfaces.GetFragment;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

public class FragmentJobFromEmployerWaitingOfMaking extends Fragment implements JobOpenChat, GetFragment {

    private RecyclerView jobs_list_view;
    private List<ModelJobReconciliation> jobs_list;

    private AdapterJobReconciliation adapterJobs;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    private FragmentJobFullFromMaking fragmentJobFullFromMaking;

    private String current_user;

    private DocumentSnapshot lastVisible;

    private AtomicBoolean emptyIsLoaded = new AtomicBoolean(false);

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_from_employer_waiting_of_making, container, false);

        fragmentJobFullFromMaking = new FragmentJobFullFromMaking();

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        current_user = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        jobs_list = new ArrayList<>();
        jobs_list_view = view.findViewById(R.id.job_from_employer_wr_recyclerview);

        adapterJobs = new AdapterJobReconciliation(jobs_list, this, this);

        jobs_list_view.setLayoutManager(new LinearLayoutManager(container.getContext()));
        jobs_list_view.setAdapter(adapterJobs);

        if (firebaseAuth.getCurrentUser() != null) {

            loadData();

        }


        return view;
    }

    @Override
    public void openChat(String companion) {

    }

    @Override
    public void openChat(String companion, String id, String fragmentTag) {
        assert getFragmentManager() != null;
        Bundle bundle = new Bundle();
        bundle.putString("companion", companion);
        bundle.putString("id", id);
        bundle.putString("fragmentTag", fragmentTag);
        fragmentJobFullFromMaking.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left);
        if (fragmentJobFullFromMaking.isAdded()) {
            fragmentTransaction.show(fragmentJobFullFromMaking);
        } else {
            fragmentTransaction.add(R.id.main_container, fragmentJobFullFromMaking, "OPEN_FRAGMENT_JOB_FULL_FROM_MAKING");
        }
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    void loadData() {
        firebaseFirestore.collection("Users")
                .document(current_user)
                .collection("WaitingOfMaking(Employer)")
                .addSnapshotListener((documentSnapshots, e) -> {

                    jobs_list.clear();
                    adapterJobs.notifyDataSetChanged();

                    assert documentSnapshots != null;

                    if (documentSnapshots.size() > 0) {

                        if (emptyIsLoaded.get()) {

                            assert getFragmentManager() != null;
                            Objects.requireNonNull(getFragmentManager().findFragmentByTag("FRAGMENT_EMPTY_LIST_FJFEWOW").getView()).setVisibility(View.GONE);

                        }

                        if (lastVisible != documentSnapshots.getDocuments().get(documentSnapshots.size() - 1)) {
                            for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {

                                if (doc.getType() == DocumentChange.Type.ADDED) {

                                    if (lastVisible != doc.getDocument()) {

                                        String ID = doc.getDocument().getId();

                                        ModelJobReconciliation modelJobReconciliation = doc.getDocument().toObject(ModelJobReconciliation.class).withId(ID);
                                        jobs_list.add(modelJobReconciliation);
                                        adapterJobs.notifyDataSetChanged();

                                        lastVisible = doc.getDocument();

                                    }

                                }

                            }
                            lastVisible = documentSnapshots.getDocuments().get(documentSnapshots.size() - 1);
                        }
                    } else {
                        setEmptyList();
                    }
                });
    }

    void setEmptyList() {

        if (!emptyIsLoaded.get()) {

            FragmentEmptyList fragmentEmptyList = new FragmentEmptyList();
            assert getFragmentManager() != null;
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment_from_employer_making_cl, fragmentEmptyList, "FRAGMENT_EMPTY_LIST_FJFEWOW");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

            emptyIsLoaded.set(true);

        }
    }


        @Override
        public String getCustomFragment () {
            return "FRAGMENT_FROM_EMPLOYER_WAITING_OF_MAKING";
        }

        public void refreshData () {

            jobs_list.clear();
            adapterJobs.notifyDataSetChanged();
            loadData();

        }
    }
