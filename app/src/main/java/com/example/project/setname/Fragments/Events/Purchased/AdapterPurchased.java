package com.example.project.setname.Fragments.Events.Purchased;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.example.project.setname.Interfaces.OpenFull;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;
import java.util.List;

public class AdapterPurchased extends RecyclerView.Adapter<AdapterPurchased.ViewHolder> {

    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();

    public Context context;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    private List<ModelPurchased> purchased_list;
    private OpenFull mOpenFull;

    public AdapterPurchased(List<ModelPurchased> purchased_list,
                            OpenFull openFull) {
        this.purchased_list = purchased_list;
        this.mOpenFull = openFull;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_evets_purchased, parent, false);
        context = parent.getContext();
        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        viewBinderHelper.setOpenOnlyOne(true);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        holder.setSwipeRevealLayout();

        String id = purchased_list.get(position).ModelFragmentPurchasedId;

        firebaseFirestore.collection("Events")
                .document(id)
                .get().addOnCompleteListener(task -> {

                    holder.setHeadline(task.getResult().getString("headline"));

                    long millisecondDate = task.getResult().getDate("timeStart").getTime();

                    String dateStringStart = DateFormat.format("MMMM dd, HH:mm", new Date(millisecondDate)).toString();
                    holder.setDate(dateStringStart);


        });

        holder.constraintLayoutFull.setOnClickListener(v->{

            mOpenFull.openFull("FRAGMENT_PURCHASED", id);

        });

        holder.constraintLayoutPart.setOnClickListener(v->{

            mOpenFull.openFull(id);

        });

    }

    @Override
    public int getItemCount() {
        return purchased_list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private SwipeRevealLayout swipeRevealLayout;

        private View mView;

        private ConstraintLayout constraintLayoutFull;
        private ConstraintLayout constraintLayoutPart;

        private TextView headline;
        private TextView date;

        ViewHolder(View itemView) {

            super(itemView);
            mView = itemView;

            constraintLayoutFull = mView.findViewById(R.id.item_events_purchased_srl_cl2);
            constraintLayoutPart = mView.findViewById(R.id.item_events_purchased_srl_cl1);

        }

        void setHeadline(String str){

            headline = mView.findViewById(R.id.item_events_purchased_headline);
            headline.setText(str);

        }

        void setDate(String str){

            date = mView.findViewById(R.id.item_events_purchased_date);
            date.setText(str);

        }

        void setSwipeRevealLayout(){

            swipeRevealLayout = mView.findViewById(R.id.fragment_events_purchased_sl);
            swipeRevealLayout.close(false);

        }
    }

}
