package com.example.project.setname.Fragments.Events;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.project.setname.Fragments.Events.AvailableEvents.FragmentAvailableEvents;
import com.example.project.setname.Fragments.Events.AvailableEvents.FragmentAvailableForViewPager;
import com.example.project.setname.Fragments.Events.Purchased.FragmentPurchased;
import com.example.project.setname.Fragments.Events.Purchased.FragmentPurchasedForViewPager;
import com.example.project.setname.R;
import com.gigamole.navigationtabstrip.NavigationTabStrip;

public class FragmentEvents extends Fragment {

    private View view;

    private ViewPager mViewPager;

    private FragmentAvailableForViewPager fragmentAvailableEvents;
    private FragmentPurchasedForViewPager fragmentPurchased;
    private FragmentEventsNonename fragmentEventsNonename;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {

            view = inflater.inflate(R.layout.fragment_events, container, false);

            setClasses();

            mViewPager = (ViewPager) view.findViewById(R.id.fragment_events_vp);

            final NavigationTabStrip navigationTabStrip = (NavigationTabStrip) view.findViewById(R.id.fragment_events_nts);
            navigationTabStrip.setTitles("Available events", "Purchased");

            navigationTabStrip.setStripWeight(2);

            mViewPager.setAdapter(new MyPagerAdapter(getFragmentManager()));

            navigationTabStrip.setViewPager(mViewPager);

        }

        return view;
    }

    private void setClasses() {

        fragmentAvailableEvents = new FragmentAvailableForViewPager();
        fragmentPurchased = new FragmentPurchasedForViewPager();
        fragmentEventsNonename = new FragmentEventsNonename();

    }

    @Override
    public void onDestroyView() {
        if (view.getParent() != null) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
        super.onDestroyView();
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch(pos) {

                case 0: return fragmentAvailableEvents;
                case 1: return fragmentPurchased;
                default: return getTargetFragment();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
