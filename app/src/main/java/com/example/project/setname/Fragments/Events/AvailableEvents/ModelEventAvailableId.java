package com.example.project.setname.Fragments.Events.AvailableEvents;

import android.support.annotation.NonNull;

import com.google.firebase.firestore.Exclude;

public class ModelEventAvailableId {

    @Exclude
    public String ModelEventAvailableId;

    public <T extends ModelEventAvailableId> T withId(@NonNull final String id){
        this.ModelEventAvailableId = id;
        return (T) this;
    }
}
