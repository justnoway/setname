package com.example.project.setname.Dialogs;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import fr.tvbarthel.lib.blurdialogfragment.SupportBlurDialogFragment;
import in.shadowfax.proswipebutton.ProSwipeButton;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class DialogSendFromAvailableToRequests extends SupportBlurDialogFragment {

    private static final String TAG = "DialogSFATR";

    public interface DeleteListenerAvailable {
        void getStateOpenedFull(long positionInList, boolean thisState);
    }

    DeleteListenerAvailable mDeleteListener;

    protected static AtomicReference<Date> dataDate = new AtomicReference<>();
    protected static AtomicReference<String> saveDate = new AtomicReference<>();

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;
    private String current_user;

    private static final String BUNDLE_KEY_DOWN_SCALE_FACTOR = "bundle_key_down_scale_factor";

    private static final String BUNDLE_KEY_BLUR_RADIUS = "bundle_key_blur_radius";

    private static final String BUNDLE_KEY_DIMMING = "bundle_key_dimming_effect";

    private static final String BUNDLE_KEY_DEBUG = "bundle_key_debug_effect";

    public static DialogSendFromAvailableToRequests newInstance(int radius,
                                                                float downScaleFactor,
                                                                boolean dimming,
                                                                boolean debug) {
        DialogSendFromAvailableToRequests fragment = new DialogSendFromAvailableToRequests();
        Bundle args = new Bundle();
        args.putInt(
                BUNDLE_KEY_BLUR_RADIUS,
                radius
        );
        args.putFloat(
                BUNDLE_KEY_DOWN_SCALE_FACTOR,
                downScaleFactor
        );
        args.putBoolean(
                BUNDLE_KEY_DIMMING,
                dimming
        );
        args.putBoolean(
                BUNDLE_KEY_DEBUG,
                debug
        );

        fragment.setArguments(args);

        return fragment;
    }

    private EditText priceView;
    protected static TextView dateView;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mDeleteListener = (DeleteListenerAvailable) getTargetFragment();

        } catch (ClassCastException e) {
            Log.e(TAG, "onAttach: ClassCastException: " + e.getMessage());
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();
        current_user = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        @SuppressLint("InflateParams") View view = Objects.requireNonNull(getActivity()).getLayoutInflater().inflate(R.layout.dialog_send_request, null);
        TextView textViewPrice = view.findViewById(R.id.dialog_send_tv_price);
        final ProSwipeButton proSwipeBtn = (ProSwipeButton) view.findViewById(R.id.dialog_send_sb_send);
        priceView = view.findViewById(R.id.dialog_send_et_price);
        dateView = view.findViewById(R.id.dialog_send_tv_date);

        dateView.setOnClickListener(v -> {

            DialogDatePicker dateDialog = new DialogDatePicker();
            Bundle bundle = new Bundle();
            bundle.putString("view", "DIALOG_SEND_FROM_AVAILABLE_TO_RECONCILIATION");
            dateDialog.setArguments(bundle);
            dateDialog.show(getFragmentManager(), "DATA_PICKER");

        });

        assert getArguments() != null;
        String postId_data = getArguments().getString("postId");
        String price_data = getArguments().getString("price");
        String employer_data = getArguments().getString("employer");
        assert postId_data != null;

        firebaseFirestore.collection("Jobs").document(postId_data).get().addOnCompleteListener(task -> {

            if (task.isComplete()) {

                if (task.getResult().getString("type").equals("trade")) {

                    textViewPrice.setText("Exchange to");

                    priceView.setClickable(false);

                }

            }

        });

        priceView.setText(price_data);


        //надоело изворачиваться, пока dateSet останется таким
        firebaseFirestore.collection("Jobs")
                .document(postId_data)
                .get().addOnCompleteListener(task -> {

            if (task.isComplete()) {

                dataDate.set(task.getResult().getDate("deadline"));

            }

        });

        @SuppressLint("SimpleDateFormat") SimpleDateFormat dt = new SimpleDateFormat("dd MM yyyy");
        try {
            Date date = dt.parse(getArguments().getString("deadline"));
            @SuppressLint("SimpleDateFormat") SimpleDateFormat dt1 = new SimpleDateFormat("MMMM dd");
            dateView.setText(dt1.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        saveDate.set(getArguments().getString("deadline"));

        proSwipeBtn.setOnSwipeListener(() -> {
            new Handler().postDelayed(() -> {

                assert employer_data != null;

                HashMap<String, Object> time = new HashMap<>();

                time.put("price", priceView.getText().toString());
                time.put("maker", current_user);
                time.put("deadline", dataDate.get());

                firebaseFirestore.collection("Jobs")
                        .document(postId_data)
                        .get().addOnCompleteListener(task1 -> {

                    if (task1.isComplete()) {

                        if (task1.getResult().getBoolean("available")) {


                            firebaseFirestore.collection("Users")
                                    .document(current_user)
                                    .collection("SentRequestsForJob(Maker)")
                                    .document(postId_data)
                                    .set(time)
                                    .addOnSuccessListener(task -> {

                                    });

                            firebaseFirestore.collection("Users")
                                    .document(employer_data)
                                    .collection("ReceivedRequestsForJob(Employer)")
                                    .document(postId_data)
                                    .set(time)
                                    .addOnSuccessListener(task -> {

                                    });

                            firebaseFirestore.collection("Users")
                                    .document(current_user)
                                    .collection("NeverShow(Maker)")
                                    .document(postId_data)
                                    .set(time)
                                    .addOnSuccessListener(task -> {

                                    });

                            proSwipeBtn.showResultIcon(true);

                            dataDate.set(null);
                            saveDate.set(null);

                            mDeleteListener.getStateOpenedFull(getArguments().getLong("position"), true);

                            getDialog().dismiss();

                        } else {

                            Toast.makeText(getContext(), "Sorry, this not available", Toast.LENGTH_SHORT).show();
                            proSwipeBtn.showResultIcon(false);

                        }

                    }

                });


            }, 2000);
        });

        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}

