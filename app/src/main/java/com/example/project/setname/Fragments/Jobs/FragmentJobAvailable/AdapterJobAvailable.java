package com.example.project.setname.Fragments.Jobs.FragmentJobAvailable;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.project.setname.Interfaces.JobAvailableOpenDialog;
import com.example.project.setname.Interfaces.OpenProfileInfo;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.ramotion.foldingcell.FoldingCell;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterJobAvailable extends RecyclerView.Adapter<AdapterJobAvailable.ViewHolder> {


    private static final String TAG = "AdapterJobAvailable";

    private List<ModelJobAvailable> jobs_list;
    public Context context;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    private JobAvailableOpenDialog mJobAvailableOpenDialog;
    private OpenProfileInfo mOpenProfileInfo;

    AdapterJobAvailable(List<ModelJobAvailable> jobs_list,
                        JobAvailableOpenDialog jobAvailableOpenDialog,
                        OpenProfileInfo openProfileInfo) {
        this.jobs_list = jobs_list;
        this.mJobAvailableOpenDialog = jobAvailableOpenDialog;
        this.mOpenProfileInfo = openProfileInfo;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_job_available, parent, false);
        context = parent.getContext();
        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        String language_data = jobs_list.get(position).getLanguage();
        String postId = jobs_list.get(position).ConstructorJobAvaiableId;
        String typeOfJob_data = jobs_list.get(position).getType();
        String levelOfJob_data = jobs_list.get(position).getLevel();
        String description_data = jobs_list.get(position).getDescription();
        String user_data = jobs_list.get(position).getUser_id();

        holder.fc.setOnClickListener(v -> holder.fc.toggle(false));


        holder.setPartImageLanguage(language_data);
        holder.setFullLanguage(language_data);
        holder.setColor(language_data);

        String price_data = jobs_list.get(position).getPrice();
        switch (typeOfJob_data) {
            case "fix":
                typeOfJob_data = "Fixed price";
                holder.setFullPrice("$" + price_data);
                holder.setPartPrice("$" + price_data);
                break;
            case "p/h":
                typeOfJob_data = "Per hour";
                holder.setFullPrice("$" + price_data);
                holder.setPartPrice("$" + price_data);
                break;
            case "trade":
                typeOfJob_data = "Exchange";
                holder.setFullPrice(price_data);
                holder.setPartPrice(price_data);
                break;
        }
        holder.setPartTypeOfJob(typeOfJob_data);
        holder.setFullTypeOfJob(typeOfJob_data);

        try {
            long millisecond = jobs_list.get(position).getDeadline().getTime();
            String dateString = DateFormat.format("dd MMMM", new Date(millisecond)).toString();
            holder.setPartDate(dateString);
            holder.setFullDate(dateString);
        } catch (Exception e) {

            Log.e(TAG, "Time exception with deadLine: " + e.getMessage());

        }

        holder.setPartLevel(levelOfJob_data);
        holder.setFullLevel(levelOfJob_data);

        holder.setPartDescription(description_data);
        holder.setFullDescription(description_data);

        if (typeOfJob_data.equals("Exchange")) {
            holder.setFullPayment(price_data);
            holder.setFullPaymentText();
        } else {
            String payment_data = jobs_list.get(position).getPayment();
            holder.setFullPayment(payment_data);
        }

        firebaseFirestore.collection("Users").document(user_data).get().addOnCompleteListener(task -> {

            if (task.isSuccessful()) {

                String userName_data = task.getResult().getString("name");
                String userRank_data = task.getResult().getString("rank");
                String userImage_data = task.getResult().getString("image");

                holder.setFullUser(userImage_data, userName_data, userRank_data, user_data);

            }
        });


        try {
            long millisecond = jobs_list.get(position).getDeadline().getTime();
            String dateStringForDialog = DateFormat.format("dd MM yyyy", new Date(millisecond)).toString();
            holder.fullSendRequestColor.setOnClickListener(v -> mJobAvailableOpenDialog.openDialog(postId, user_data, price_data, dateStringForDialog, position));
        } catch (Exception e) {

            Log.e(TAG, "Time exception with openDialog: " + e.getMessage());

        }


    }

    @Override
    public int getItemCount() {
        return jobs_list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private View mView;
        private FoldingCell fc;

        //part view
        private ImageView partImageLanguage;
        private TextView partTypeOfJob;
        private TextView partPrice;
        private TextView partDate;
        private TextView partDescription;
        private TextView partLevel;
        private View partColorLine;

        //full view
        private TextView fullLevel;
        private TextView fullDate;
        private TextView fullTypeOfJob;
        private TextView fullPrice;
        private TextView fullDescription;
        private TextView fullLanguage;
        private TextView fullPayment;
        private TextView fullPaymentText;

        private CircleImageView userImage;
        private TextView userName;
        private TextView userRank;

        //color view going up to down
        private View fullColorLine1;
        private View fullColorLine2;
        private View fullColorLine3;
        private Button fullSendRequestColor;

        ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;

            fc = mView.findViewById(R.id.job_folding_cell);
        }


        //for part view
        void setPartTypeOfJob(String str) {
            partTypeOfJob = mView.findViewById(R.id.job_part_typeOfJob);
            partTypeOfJob.setText(str);
        }

        void setPartPrice(String str) {
            partPrice = mView.findViewById(R.id.job_part_price);
            partPrice.setText(str);
        }

        void setPartImageLanguage(String str) {
            partImageLanguage = mView.findViewById(R.id.job_part_imageOfLanguage);
            switch (str) {
                case "Java":
                    partImageLanguage.setImageResource(R.drawable.ic_java);
                    break;
                case "Swift":
                    partImageLanguage.setImageResource(R.drawable.ic_swift);
                    break;
                case "Mobile":
                    partImageLanguage.setImageResource(R.drawable.ic_smartphone);
                    break;
                case "Web":
                    partImageLanguage.setImageResource(R.drawable.ic_web);
                    break;
                case "Python":
                    partImageLanguage.setImageResource(R.drawable.ic_python);
                    break;
            }
        }

        void setPartDate(String str) {
            partDate = mView.findViewById(R.id.job_part_deadline);
            partDate.setText(str);
        }

        @SuppressLint("SetTextI18n")
        void setPartDescription(String str) {
            partDescription = mView.findViewById(R.id.job_part_description);
            if (str.length() >= 286) {
                partDescription.setText(str.substring(0, 283) + "...");
            } else {
                partDescription.setText(str);
            }
        }

        void setPartLevel(String str) {
            partLevel = mView.findViewById(R.id.job_part_level);
            partLevel.setText(str);
        }

        void setFullLevel(String str) {
            fullLevel = mView.findViewById(R.id.job_full_level_text);
            fullLevel.setText(str);
        }

        void setFullDate(String str) {
            fullDate = mView.findViewById(R.id.job_full_deadline_text);
            fullDate.setText(str);
        }

        void setFullTypeOfJob(String str) {
            fullTypeOfJob = mView.findViewById(R.id.job_full_typeOfJob);
            fullTypeOfJob.setText(str);
        }

        void setFullPrice(String str) {
            fullPrice = mView.findViewById(R.id.job_full_price);
            fullPrice.setText(str);
        }

        void setFullDescription(String str) {
            fullDescription = mView.findViewById(R.id.job_full_mustToDo_text);
            fullDescription.setText(str);
        }

        void setFullLanguage(String str) {
            fullLanguage = mView.findViewById(R.id.job_full_language);
            fullLanguage.setText(str);
        }

        void setFullPayment(String str) {
            fullPayment = mView.findViewById(R.id.job_full_payment);
            fullPayment.setText(str);
        }

        @SuppressLint("CheckResult")
        void setFullUser(String imageURL, String name, String rank, String userForProfileInfo) {

            userImage = mView.findViewById(R.id.job_full_person);
            userName = mView.findViewById(R.id.job_full_nameOfPerson);
            userRank = mView.findViewById(R.id.job_full_rank);

            userImage.setOnClickListener(v -> mOpenProfileInfo.openProfile(userForProfileInfo));

            userName.setOnClickListener(v -> mOpenProfileInfo.openProfile(userForProfileInfo));

            userRank.setOnClickListener(v -> mOpenProfileInfo.openProfile(userForProfileInfo));

            userName.setText(name);
            userRank.setText(rank);

            RequestOptions placeholderOption = new RequestOptions();
            placeholderOption.placeholder(R.drawable.profile_placeholder);
            Glide.with(context).applyDefaultRequestOptions(placeholderOption).load(imageURL).into(userImage);


        }

        void setCustomColor(View view, String string) {
            view.setBackgroundColor(Color.parseColor(string));
        }

        void setColors(String fullLineDark, String fullLineLight, String partLine) {
            setCustomColor(fullColorLine1, fullLineDark);
            setCustomColor(fullColorLine2, fullLineLight);
            setCustomColor(partColorLine, partLine);
            setCustomColor(fullColorLine3, partLine);
        }

        void setColor(String str) {
            partColorLine = mView.findViewById(R.id.job_part_rectangle);//1
            fullColorLine1 = mView.findViewById(R.id.job_full_rectangle_costs);//3 самый темный
            fullColorLine2 = mView.findViewById(R.id.job_full_rectangle_deadline);//2 более темный
            fullColorLine3 = mView.findViewById(R.id.job_full_rectangle);//1
            fullSendRequestColor = mView.findViewById(R.id.job_full_send);
            switch (str) {
                case "Java":
                    setColors("#AD1457", "#C2185B", "#D81B60");
                    fullSendRequestColor.setBackgroundResource(R.drawable.btn_send_java);
                    break;
                case "Swift":
                    setColors("#D84315", "#F4511E", "#FF7043");
                    fullSendRequestColor.setBackgroundResource(R.drawable.btn_send_swift);
                    break;
                case "Python":
                    setColors("#01579B", "#0288D1", "#03A9F4");
                    fullSendRequestColor.setBackgroundResource(R.drawable.btn_send_python);
                    break;
                case "Web":
                    setColors("#6A1B9A", "#8E24AA", "#AB47BC");
                    fullSendRequestColor.setBackgroundResource(R.drawable.btn_send_web);
                    break;
                case "Mobile":
                    setColors("#00695C", "#00897B", "#26A69A");
                    fullSendRequestColor.setBackgroundResource(R.drawable.btn_send_mobile);
                    break;
            }
        }

        @SuppressLint("SetTextI18n")
        void setFullPaymentText() {
            fullPaymentText = mView.findViewById(R.id.job_full_paymentText);
            fullPaymentText.setText("Exchange");
        }
    }
}