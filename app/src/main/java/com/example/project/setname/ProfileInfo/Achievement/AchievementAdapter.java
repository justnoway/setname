package com.example.project.setname.ProfileInfo.Achievement;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import java.util.List;


public class AchievementAdapter extends RecyclerView.Adapter<AchievementAdapter.ViewHolder> {

    private List<ModelAchievement> achievementHorizontal_list;
    public Context context;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    public AchievementAdapter(List<ModelAchievement> achievementHorizontal_list) {
        this.achievementHorizontal_list = achievementHorizontal_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.achievement_horizontal_card, parent, false);
        context = parent.getContext();
        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        String points_data = achievementHorizontal_list.get(position).getPoints_of_achievement();
        holder.setAchievementText(points_data);

        String icon_data = achievementHorizontal_list.get(position).getIcon_url();
        holder.setAchievementImage(icon_data);

    }

    @Override
    public int getItemCount() {
        return achievementHorizontal_list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private View mView;

        private ImageView achievementImage;
        private TextView achievementText;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setAchievementImage(String downloadUri){
            achievementImage = mView.findViewById(R.id.achievement_image);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.ic_second);
            Glide.with(context).applyDefaultRequestOptions(requestOptions).load(downloadUri).into(achievementImage);
        }

        public void setAchievementText(String str){
            achievementText = mView.findViewById(R.id.achievement_text_point);
            achievementText.setText("+"+str+" points");
        }

    }
}
