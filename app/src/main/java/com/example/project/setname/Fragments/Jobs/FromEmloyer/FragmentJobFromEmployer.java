package com.example.project.setname.Fragments.Jobs.FromEmloyer;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.project.setname.R;

public class FragmentJobFromEmployer extends android.support.v4.app.Fragment {

    private FragmentJobFromEmployerReconciliation fragmentJobFromEmployerReconciliationJobFromEmployerReconciliations;
    private FragmentJobFromEmployerWaitingOfMaking fragmentJobFromEmployerWaitingOfMaking;
    private FragmentJobFromEmployerComeRequests fragmentJobFromEmployerComeRequests;

    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_job_from_employer, container, false);

        swipeRefreshLayout = view.findViewById(R.id.fragment_fragment_job_from_employer_swipe_refresh);

        fragmentJobFromEmployerWaitingOfMaking = new FragmentJobFromEmployerWaitingOfMaking();
        replaceFragment(R.id.fragment_job_from_employer_frame_making, fragmentJobFromEmployerWaitingOfMaking, "FRAGMENT_JOB_FROM_EMPLOYER_WAITING_OF_MAKING");

        fragmentJobFromEmployerReconciliationJobFromEmployerReconciliations = new FragmentJobFromEmployerReconciliation();
        replaceFragment(R.id.fragment_job_from_employer_frame_reconciliations, fragmentJobFromEmployerReconciliationJobFromEmployerReconciliations, "FRAGMENT_JOB_FROM_EMPLOYER_RECONCILIATIONS");

        fragmentJobFromEmployerComeRequests = new FragmentJobFromEmployerComeRequests();
        replaceFragment(R.id.fragment_job_from_employer_frame_come_requests, fragmentJobFromEmployerComeRequests, "FRAGMENT_JOB_FROM_EMPLOYER_COME_REQUESTS");

        swipeRefreshLayout.setOnRefreshListener(() -> {

            fragmentJobFromEmployerWaitingOfMaking.refreshData();
            fragmentJobFromEmployerReconciliationJobFromEmployerReconciliations.refreshData();
            fragmentJobFromEmployerComeRequests.refreshData();

            swipeRefreshLayout.setRefreshing(false);

        });

        return view;
    }

    public void replaceFragment(int R_view, android.support.v4.app.Fragment fragment, String tag){

        assert getFragmentManager() != null;
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R_view, fragment, tag);
        fragmentTransaction.commit();

    }
}