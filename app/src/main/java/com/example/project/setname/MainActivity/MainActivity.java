package com.example.project.setname.MainActivity;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.project.setname.Activities.ActivityLogin;
import com.example.project.setname.Activities.ActivitySetup;
import com.example.project.setname.Fragments.Events.FragmentEvents;
import com.example.project.setname.Fragments.Jobs.FragmentJob;
import com.example.project.setname.Fragments.Jobs.JobNew.FragmentNewJob;
import com.example.project.setname.Fragments.News.NewsFragment.FragmentNews;
import com.example.project.setname.Fragments.News.NewsNew.FragmentNewNews;
import com.example.project.setname.Fragments.News.NewsSettings.FragmentNewsSettings;
import com.example.project.setname.Fragments.Search.FragmentSearch;
import com.example.project.setname.ProfileSettings.FragmentProfileSettings;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

import devlight.io.library.ntb.NavigationTabBar;
import me.yokeyword.fragmentation_swipeback.SwipeBackActivity;

public class MainActivity extends SwipeBackActivity {

    /**
     * Не понимаю почему после выхода из аккаунта запрос FirebaseAuth.getInstance().getCurrentUser() дает не null а пользователя
     *
     *
     *
     */


    private static final String TAG = "MainActivityClass";

    private FirebaseAuth mAuth;
    private FirebaseFirestore firebaseFirestore;

    //additional
    private FragmentNewNews fragmentNewNews;
    private FragmentNewJob fragmentNewJob;

    //main
    private FragmentNews fragmentNews;
    private FragmentJob fragmentJob;
    private FragmentEvents fragmentEvents;
    private FragmentSearch fragmentSearch;
    private FragmentNewsSettings fragmentNewsSettings;

    //settings
    private FragmentProfileSettings fragmentProfileSettings;

    //NTB
    private NavigationTabBar mainNavigationTabBar;
    private NavigationTabBar additionalNavigationTabBar;
    private NavigationTabBar settingsNavigationBar;

    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        /*setFirebaseListener();*/

        if (mAuth.getCurrentUser()!=null) {

            setClasses();

            setMainNavigationBar();

            setAddNavigationBar();

            setSettingsNavigationBar();

            setSwipeBackEnable(false);

            if (savedInstanceState == null){

                mainNavigationTabBar.setModelIndex(0);

            }


        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        /*FirebaseAuth.getInstance().addAuthStateListener(mAuthListener);*/

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        Log.i(TAG, " "+user);

        if (user==null) {

            sendToLogin();

        }else {

            firebaseFirestore.collection("Users").document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .get().addOnCompleteListener(task -> {

                        if(task.isSuccessful()){

                            if(!task.getResult().exists()){

                                Intent setupIntent = new Intent(MainActivity.this, ActivitySetup.class);
                                startActivity(setupIntent);
                                finish();

                            }

                        } else {

                            String errorMessage = task.getException().getMessage();
                            Toast.makeText(MainActivity.this, "Error : " + errorMessage, Toast.LENGTH_LONG).show();


                        }

                    });

        }

    }

    private void setFirebaseListener(){

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user==null){

                    sendToLogin();

                }else {

                    firebaseFirestore.collection("Users").document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                            .get().addOnCompleteListener(task -> {

                        if(task.isSuccessful()){

                            if(!task.getResult().exists()){

                                Intent setupIntent = new Intent(MainActivity.this, ActivitySetup.class);
                                startActivity(setupIntent);
                                finish();

                            }

                        } else {

                            String errorMessage = task.getException().getMessage();
                            Toast.makeText(MainActivity.this, "Error : " + errorMessage, Toast.LENGTH_LONG).show();


                        }

                    });

                }
            }
        };

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null){

            FirebaseAuth.getInstance().removeAuthStateListener(mAuthListener);

        }
    }

    private void setAddNavigationBar(){

        additionalNavigationTabBar = (NavigationTabBar) findViewById(R.id.ntb_add);
        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();

        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_icon_add_news),
                        Color.parseColor("#f5f5f5")
                ).title("Add news")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_icon_add_job),
                        Color.parseColor("#f5f5f5")
                ).title("Add job")
                        .build()
        );

        additionalNavigationTabBar.setModels(models);

        additionalNavigationTabBar.setOnTabBarSelectedIndexListener(new NavigationTabBar.OnTabBarSelectedIndexListener() {
            @Override
            public void onStartTabSelected(final NavigationTabBar.Model model, final int index) {

                switch (index) {

                    case 0:
                        replaceFragment(fragmentNewNews, "FRAGMENT_NEW_NEWS");
                        mainNavigationTabBar.deselect();
                        settingsNavigationBar.deselect();
                        break;
                    case 1:
                        replaceFragment(fragmentNewJob, "FRAGMENT_NEW_JOB");
                        mainNavigationTabBar.deselect();
                        settingsNavigationBar.deselect();
                        break;
                    default:
                        break;
                }

            }


            @Override
            public void onEndTabSelected(final NavigationTabBar.Model model, final int index) {
                model.hideBadge();
            }
        });

    }

    private void setMainNavigationBar(){

        mainNavigationTabBar = (NavigationTabBar) findViewById(R.id.ntb);
        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();

        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_news),
                        Color.parseColor("#EEEEEE")
                ).title("News")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_job),
                        Color.parseColor("#EEEEEE")
                ).title("Job")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_events1),
                        Color.parseColor("#EEEEEE")
                ).title("Events")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_top),
                        Color.parseColor("#EEEEEE")
                ).title("Search")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_events2),
                        Color.parseColor("#EEEEEE")
                ).title("IDK")
                        .build()
        );
        mainNavigationTabBar.setModels(models);

        mainNavigationTabBar.setOnTabBarSelectedIndexListener(new NavigationTabBar.OnTabBarSelectedIndexListener() {
            @Override
            public void onStartTabSelected(final NavigationTabBar.Model model, final int index) {

                switch (index) {

                    case 0:
                        replaceFragment(fragmentNews, "FRAGMENT_NEWS");
                        additionalNavigationTabBar.deselect();
                        settingsNavigationBar.deselect();
                        break;
                    case 1:
                        replaceFragment(fragmentJob, "FRAGMENT_JOB");
                        additionalNavigationTabBar.deselect();
                        settingsNavigationBar.deselect();
                        break;
                    case 2:
                        replaceFragment(fragmentEvents, "FRAGMENT_EVENTS");
                        additionalNavigationTabBar.deselect();
                        settingsNavigationBar.deselect();
                        break;
                    case 3:
                        replaceFragment(fragmentSearch, "FRAGMENT_SEARCH");
                        additionalNavigationTabBar.deselect();
                        settingsNavigationBar.deselect();
                        break;
                    case 4:
                        replaceFragment(fragmentNewsSettings, "FRAGMENT_NEWS_SETTINGS");
                        additionalNavigationTabBar.deselect();
                        settingsNavigationBar.deselect();
                        break;
                    default:
                        break;
                }

            }


            @Override
            public void onEndTabSelected(final NavigationTabBar.Model model, final int index) {
                model.hideBadge();
            }
        });

    }

    private void setSettingsNavigationBar(){

        settingsNavigationBar = (NavigationTabBar) findViewById(R.id.ntb_settings);
        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();

        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_user_settings  ),
                        Color.parseColor("#f5f5f5")
                ).title("Settings")
                        .build()
        );

        settingsNavigationBar.setModels(models);

        settingsNavigationBar.setOnTabBarSelectedIndexListener(new NavigationTabBar.OnTabBarSelectedIndexListener() {
            @Override
            public void onStartTabSelected(final NavigationTabBar.Model model, final int index) {

                switch (index) {

                    case 0:
                        replaceFragment(fragmentProfileSettings, "FRAGMENT_SETTINGS");
                        additionalNavigationTabBar.deselect();
                        mainNavigationTabBar.deselect();
                        break;
                    default:
                        break;
                }

            }


            @Override
            public void onEndTabSelected(final NavigationTabBar.Model model, final int index) {
                model.hideBadge();
            }

        });

    }

    public void logOut() {

        mAuth.signOut();
        MainActivity.this.finish();

        sendToLogin();

    }

    private void sendToLogin() {

        Intent loginIntent = new Intent(MainActivity.this, ActivityLogin.class);
        startActivity(loginIntent);
        finish();

    }

    private void setClasses(){

        fragmentNewNews = new FragmentNewNews();
        fragmentNewJob = new FragmentNewJob();

        fragmentNews = new FragmentNews();
        fragmentJob = new FragmentJob();
        fragmentEvents = new FragmentEvents();
        fragmentSearch = new FragmentSearch();
        fragmentNewsSettings = new FragmentNewsSettings();

        fragmentProfileSettings = new FragmentProfileSettings();

    }

    //Я знаю, что используют replace вместо add, но не понимаю как сделать сохранение данных
    public void replaceFragment(Fragment fragment, String tag){

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_container, fragment, tag);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

}
