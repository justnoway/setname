package com.example.project.setname.Fragments.Jobs.Reconciliations;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.setname.Fragments.Jobs.ShowStatus.AdapterShowStatus;
import com.example.project.setname.Fragments.Jobs.ShowStatus.ModelShowStatus;
import com.example.project.setname.Interfaces.JobOpenChat;
import com.example.project.setname.Interfaces.GetFragment;
import com.example.project.setname.Interfaces.JobSendCode;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class AdapterJobReconciliation extends RecyclerView.Adapter<AdapterJobReconciliation.ViewHolder> {

    private static final String TAG = "AdapterJR";

    private List<ModelJobReconciliation> jobs_list;
    public Context context;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    private JobSendCode mJobSendCode;
    private JobOpenChat mJobOpenChat;
    private GetFragment mGetFragment;

    public AdapterJobReconciliation(List<ModelJobReconciliation> jobs_list,
                                    JobSendCode jobSendCode,
                                    JobOpenChat jobOpenChat,
                                    GetFragment getFragment) {
        this.jobs_list = jobs_list;
        this.mJobSendCode = jobSendCode;
        this.mJobOpenChat = jobOpenChat;
        this.mGetFragment = getFragment;
    }

    public AdapterJobReconciliation(List<ModelJobReconciliation> jobs_list,
                                    JobOpenChat jobOpenChat,
                                    GetFragment getFragment) {
        this.jobs_list = jobs_list;
        this.mJobOpenChat = jobOpenChat;
        this.mGetFragment = getFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_jobs_cooking, parent, false);
        context = parent.getContext();
        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        String id_data = jobs_list.get(position).ConstructorJobFromMakerId;

        String employer = jobs_list.get(position).getEmployer_id();
        String maker = jobs_list.get(position).getMaker_id();

        //Reconciliations
        String name_data = jobs_list.get(position).getName();
        holder.setName(name_data);

        try {
            long millisecond = jobs_list.get(position).getDeadline().getTime();
            String dateStringForDialog = DateFormat.format("MMMM dd", new Date(millisecond)).toString();
            holder.setDate(dateStringForDialog);

        } catch (Exception e) {
            Toast.makeText(context, "Exception : " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        String language_data = jobs_list.get(position).getLanguage();
        switch (language_data) {
            case "Java":
                holder.setBackgroundView(R.drawable.item_jobs_field_java);
                break;
            case "Swift":
                holder.setBackgroundView(R.drawable.item_jobs_field_swift);
                break;
            case "Python":
                holder.setBackgroundView(R.drawable.item_jobs_field_python);
                break;
            case "Web":
                holder.setBackgroundView(R.drawable.item_jobs_field_web);
                break;
            case "Mobile":
                holder.setBackgroundView(R.drawable.item_jobs_field_mobile);
                break;
        }

        try {
            if (mGetFragment.getCustomFragment().equals("FRAGMENT_FROM_MAKER_RECONCILIATIONS")) {

                holder.backgroundView.setOnClickListener(v -> {
                    mJobOpenChat.openChat(employer, id_data, "FRAGMENT_FROM_MAKER_RECONCILIATIONS");
                });

                holder.setStatusMaker(id_data);

            } else if (mGetFragment.getCustomFragment().equals("FRAGMENT_FROM_MAKER_MAKING")) {

                holder.backgroundView.setOnClickListener(v -> {
                    mJobOpenChat.openChat(employer, id_data, "FRAGMENT_FROM_MAKER_MAKING");
                });

                holder.setStatusMakerMaking(id_data);


            } else if (mGetFragment.getCustomFragment().equals("FRAGMENT_FROM_EMPLOYER_WAITING_OF_MAKING")) {

                holder.backgroundView.setOnClickListener(v -> {
                    mJobOpenChat.openChat(maker, id_data, "FRAGMENT_FROM_EMPLOYER_WAITING_OF_MAKING");
                });

                holder.setStatusEmployerWOM(maker);

            } else {

                holder.backgroundView.setOnClickListener(v -> {
                    mJobOpenChat.openChat(maker, id_data, "FRAGMENT_FROM_EMPLOYER_RECONCILIATIONS");
                });

                holder.setStatusEmployer(id_data);

            }
        } catch (NullPointerException ignored) {

        }
    }

    @Override
    public int getItemCount() {
        return jobs_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        //Reconciliations
        private TextView name;
        private TextView date;
        private View backgroundView;

        private View mView;

        private RecyclerView showStatusRV;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        //Reconciliations
        void setName(String str) {
            name = mView.findViewById(R.id.item_job_multiview_name);
            name.setText(str);
        }

        void setDate(String str) {
            date = mView.findViewById(R.id.item_job_multiview_date);
            date.setText(str);
        }

        void setBackgroundView(int drawableRes) {
            backgroundView = mView.findViewById(R.id.item_job_multiview_background);
            backgroundView.setBackgroundResource(drawableRes);
        }

        void setStatusMaker(String id) {
            showStatusRV = mView.findViewById(R.id.item_jobs_cooking_status);
            List<ModelShowStatus> showStatus_list = new ArrayList<>();
            AdapterShowStatus adapterShowStatus = new AdapterShowStatus(showStatus_list);
            showStatusRV.setLayoutManager(new LinearLayoutManager(context.getApplicationContext()));
            showStatusRV.setAdapter(adapterShowStatus);

            firebaseFirestore.collection("Users")
                    .document(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                    .collection("Reconciliations(Maker)")
                    .document(id)
                    .collection("Status")
                    .orderBy("timestamp", Query.Direction.ASCENDING)
                    .limit(5)
                    .addSnapshotListener((documentSnapshots, e) -> {

                        assert documentSnapshots != null;
                        if (documentSnapshots.size()>0) {
                            for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {

                                if (doc.getType() == DocumentChange.Type.ADDED) {

                                    if (showStatus_list.size() > 4) {

                                        showStatus_list.remove(4);
                                        adapterShowStatus.notifyDataSetChanged();

                                        ModelShowStatus modelJobAvailable = doc.getDocument()
                                                .toObject(ModelShowStatus.class);
                                        showStatus_list.add(0, modelJobAvailable);
                                        adapterShowStatus.notifyDataSetChanged();


                                    } else {
                                        ModelShowStatus modelJobAvailable = doc.getDocument()
                                                .toObject(ModelShowStatus.class);
                                        showStatus_list.add(0, modelJobAvailable);
                                        adapterShowStatus.notifyDataSetChanged();
                                    }
                                }
                            }

                        }

                    });

        }

        void setStatusEmployer(String id) {

            showStatusRV = mView.findViewById(R.id.item_jobs_cooking_status);
            List<ModelShowStatus> showStatus_list = new ArrayList<>();
            AdapterShowStatus adapterShowStatus = new AdapterShowStatus(showStatus_list);
            showStatusRV.setLayoutManager(new LinearLayoutManager(context.getApplicationContext()));
            showStatusRV.setAdapter(adapterShowStatus);

            firebaseFirestore.collection("Users")
                    .document(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())//пока так
                    .collection("Reconciliations(Employer)")
                    .document(id)
                    .collection("Status")
                    .orderBy("timestamp", Query.Direction.ASCENDING)
                    .limit(5)
                    .addSnapshotListener((documentSnapshots, e) -> {

                        if (documentSnapshots.size()>0) {
                            for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {

                                if (doc.getType() == DocumentChange.Type.ADDED) {

                                    if (showStatus_list.size() > 4) {

                                        showStatus_list.remove(4);
                                        adapterShowStatus.notifyDataSetChanged();

                                        ModelShowStatus modelJobAvailable = doc.getDocument()
                                                .toObject(ModelShowStatus.class);
                                        showStatus_list.add(0, modelJobAvailable);
                                        adapterShowStatus.notifyDataSetChanged();


                                    } else {
                                        ModelShowStatus modelJobAvailable = doc.getDocument()
                                                .toObject(ModelShowStatus.class);
                                        showStatus_list.add(0, modelJobAvailable);
                                        adapterShowStatus.notifyDataSetChanged();
                                    }
                                }
                            }

                        }

                    });

        }

        void setStatusEmployerWOM(String id){

            showStatusRV = mView.findViewById(R.id.item_jobs_cooking_status);
            List<ModelShowStatus> showStatus_list = new ArrayList<>();
            AdapterShowStatus adapterShowStatus = new AdapterShowStatus(showStatus_list);
            showStatusRV.setLayoutManager(new LinearLayoutManager(context.getApplicationContext()));
            showStatusRV.setAdapter(adapterShowStatus);

            firebaseFirestore.collection("Users")
                    .document(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())//пока так
                    .collection("WaitingOfMaking(Employer)")
                    .document(id)
                    .collection("Status")
                    .orderBy("timestamp", Query.Direction.ASCENDING)
                    .limit(5)
                    .addSnapshotListener((documentSnapshots, e) -> {

                        assert documentSnapshots != null;
                        if (documentSnapshots.size()>0) {
                            for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {

                                if (doc.getType() == DocumentChange.Type.ADDED) {

                                    if (showStatus_list.size() > 4) {

                                        showStatus_list.remove(4);
                                        adapterShowStatus.notifyDataSetChanged();

                                        ModelShowStatus modelJobAvailable = doc.getDocument()
                                                .toObject(ModelShowStatus.class);
                                        showStatus_list.add(0, modelJobAvailable);
                                        adapterShowStatus.notifyDataSetChanged();


                                    } else {
                                        ModelShowStatus modelJobAvailable = doc.getDocument()
                                                .toObject(ModelShowStatus.class);
                                        showStatus_list.add(0, modelJobAvailable);
                                        adapterShowStatus.notifyDataSetChanged();
                                    }
                                }
                            }
                        }

                    });

        }

        void setStatusMakerMaking(String id){
            showStatusRV = mView.findViewById(R.id.item_jobs_cooking_status);
            List<ModelShowStatus> showStatus_list = new ArrayList<>();
            AdapterShowStatus adapterShowStatus = new AdapterShowStatus(showStatus_list);
            showStatusRV.setLayoutManager(new LinearLayoutManager(context.getApplicationContext()));
            showStatusRV.setAdapter(adapterShowStatus);

            firebaseFirestore.collection("Users")
                    .document(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                    .collection("Making(Maker)")
                    .document(id)
                    .collection("Status")
                    .orderBy("timestamp", Query.Direction.ASCENDING)
                    .limit(5)
                    .addSnapshotListener((documentSnapshots, e) -> {

                        assert documentSnapshots != null;
                        if (documentSnapshots.size()>0) {

                            for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {

                                if (doc.getType() == DocumentChange.Type.ADDED) {

                                    if (showStatus_list.size() > 4) {

                                        showStatus_list.remove(4);
                                        adapterShowStatus.notifyDataSetChanged();

                                        ModelShowStatus modelJobAvailable = doc.getDocument()
                                                .toObject(ModelShowStatus.class);
                                        showStatus_list.add(0, modelJobAvailable);
                                        adapterShowStatus.notifyDataSetChanged();


                                    } else {
                                        ModelShowStatus modelJobAvailable = doc.getDocument()
                                                .toObject(ModelShowStatus.class);
                                        showStatus_list.add(0, modelJobAvailable);
                                        adapterShowStatus.notifyDataSetChanged();
                                    }
                                }
                            }

                        }

                    });

        }

    }
}