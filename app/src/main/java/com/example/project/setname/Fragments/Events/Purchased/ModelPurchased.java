package com.example.project.setname.Fragments.Events.Purchased;

import java.util.Date;

public class ModelPurchased extends ModelPurchasedId {

    public ModelPurchased() {
    }

    Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
