package com.example.project.setname.ProfileSettings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.project.setname.Activities.ActivityLogin;
import com.example.project.setname.MainActivity.MainActivity;
import com.example.project.setname.R;
import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;

public class FragmentProfileSettings extends Fragment {

    private View view;

    private Button logoutBtn;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {

            view = inflater.inflate(R.layout.fragment_profile_settings, container, false);

            logoutBtn = view.findViewById(R.id.activity_main_log_out);
            logoutBtn.setOnClickListener(v -> {
                logOut();
            });

        }

        return view;
    }

    @Override
    public void onDestroyView() {
        if (view.getParent() != null) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
        super.onDestroyView();
    }

    private void logOut() {

        FirebaseAuth.getInstance().signOut();

        ActivityLogin.mAuth.signOut();

        sendToLogin();

        /*AuthUI.getInstance().signOut(getActivity()).addOnCompleteListener(task -> {

            sendToLogin();
            MainActivity.logged.set(false);

        });*/

        /*FirebaseAuth.getInstance().signOut();*/

    }

    private void sendToLogin() {

        Intent loginIntent = new Intent(getActivity(), ActivityLogin.class);
        startActivity(loginIntent);
        this.getActivity().finish();


    }

}
