package com.example.project.setname.Fragments.Jobs.FromMaker;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.project.setname.R;

public class FragmentJobFromMaker extends android.support.v4.app.Fragment {

    private FragmentJobFromMakerMaking fragmentJobFromMakerMaking;
    private FragmentJobFromMakerReconciliation fragmentJobFromMakerReconciliationJobFromMakerReconciliations;
    private FragmentJobFromMakerSentRequest fragmentJobFromMakerSentRequest;

    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_jobs_from_maker, container, false);

        swipeRefreshLayout = view.findViewById(R.id.fragment_fragment_job_from_maker_swipe_refresh);

        fragmentJobFromMakerMaking = new FragmentJobFromMakerMaking();
        replaceFragment(R.id.fragment_job_from_maker_frame_making, fragmentJobFromMakerMaking, "FRAGMENT_JOB_FROM_MAKER_MAKING");

        fragmentJobFromMakerReconciliationJobFromMakerReconciliations = new FragmentJobFromMakerReconciliation();
        replaceFragment(R.id.fragment_job_from_maker_frame_reconciliations, fragmentJobFromMakerReconciliationJobFromMakerReconciliations, "FRAGMENT_JOB_FROM_MAKER_RECONCILIATIONS");

        fragmentJobFromMakerSentRequest = new FragmentJobFromMakerSentRequest();
        replaceFragment(R.id.fragment_job_from_maker_frame_come, fragmentJobFromMakerSentRequest, "FRAGMENT_JOB_FROM_MAKER_SENT_REQUESTS");

        swipeRefreshLayout.setOnRefreshListener(() -> {

            fragmentJobFromMakerMaking.refreshData();
            fragmentJobFromMakerReconciliationJobFromMakerReconciliations.refreshData();
            fragmentJobFromMakerSentRequest.refreshData();

            swipeRefreshLayout.setRefreshing(false);

        });

        return view;
    }

    public void replaceFragment(int R_view, android.support.v4.app.Fragment fragment, String tag){

        assert getFragmentManager() != null;
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R_view, fragment, tag);
        fragmentTransaction.commit();

    }
}
