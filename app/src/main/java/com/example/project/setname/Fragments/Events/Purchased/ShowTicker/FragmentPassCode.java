package com.example.project.setname.Fragments.Events.Purchased.ShowTicker;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.project.setname.Fragments.Events.AvailableEvents.FragmentAvailableEvents;
import com.example.project.setname.Fragments.Events.AvailableEvents.FragmentAvailableForViewPager;
import com.example.project.setname.Fragments.Events.Purchased.FragmentPurchased;
import com.example.project.setname.Fragments.Events.Purchased.FragmentPurchasedForViewPager;
import com.example.project.setname.R;
import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.hanks.passcodeview.PasscodeView;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicReference;

import me.yokeyword.fragmentation_swipeback.SwipeBackFragment;

public class FragmentPassCode extends SwipeBackFragment {

    private View view;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    private String currentUser;

    AtomicReference<String> pass = new AtomicReference<>("");

    @Override
        public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
        Bundle savedInstanceState) {

            view = inflater.inflate(R.layout.fragment_passcode, container, false);
            PasscodeView passcodeView = (PasscodeView) view.findViewById(R.id.passcodeView);

        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();

        currentUser = firebaseAuth.getCurrentUser().getUid();

        firebaseFirestore.collection("Users")
                .document(currentUser)
                .get().addOnCompleteListener(task -> {

                    if (task.isComplete()) {

                        pass.set(task.getResult().getString("password")!=null?task.getResult().getString("password"):"");

                        if (!pass.get().equals("")){

                            passcodeView
                                    .setPasscodeLength(5)
                                    .setLocalPasscode(pass.get())
                                    .setListener(new PasscodeView.PasscodeViewListener() {
                                        @Override
                                        public void onFail() {

                                        }

                                        @Override
                                        public void onSuccess(String number) {

                                            getFragmentManager().popBackStack();

                                            openTicket();

                                        }
                                    });

                        }else {

                            passcodeView
                                    .setPasscodeLength(5)
                                    .setListener(new PasscodeView.PasscodeViewListener() {
                                        @Override
                                        public void onFail() {

                                        }

                                        @Override
                                        public void onSuccess(String number) {

                                            /*getFragmentManager().popBackStack();*/

                                            HashMap<String, Object> map = new HashMap<>();
                                            map.put("password", number);

                                            firebaseFirestore.collection("Users")
                                                    .document(currentUser)
                                                    .update(map)
                                                    .addOnCompleteListener(task1 -> {

                                                        getFragmentManager().popBackStack();

                                                        openTicket();

                                                    });

                                        }
                                    });

                        }

                    }

        });



        return attachToSwipeBack(view);

    }

    void openTicket(){

        FragmentShowTicket fragmentShowTicket = new FragmentShowTicket();
        Bundle bundle = new Bundle();
        bundle.putString("id", getArguments().getString("id"));
        fragmentShowTicket.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left);
        fragmentTransaction.add(R.id.main_container, fragmentShowTicket, "FRAGMENT_SHOW_TICKET");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }



}
