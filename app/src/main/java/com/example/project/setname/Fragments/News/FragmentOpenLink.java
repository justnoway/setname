package com.example.project.setname.Fragments.News;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.project.setname.R;

import me.yokeyword.fragmentation_swipeback.SwipeBackFragment;

public class FragmentOpenLink extends SwipeBackFragment {

    private WebView webView;
    private String web_link;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_open_link, container, false);

        webView = view.findViewById(R.id.fragment_open_link_wv);

         web_link = getArguments().getString("link");

         webView.setWebViewClient(new WebViewClient());
         /*webView.getSettings().setJavaScriptEnabled(true);*/
         webView.getSettings().setDomStorageEnabled(true);
         webView.setOverScrollMode(WebView.OVER_SCROLL_NEVER);
         webView.loadUrl(web_link);

        return attachToSwipeBack(view);
    }
}
