package com.example.project.setname.Fragments.Events.AvailableEvents;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.project.setname.Fragments.Events.AvailableEvents.AvailableEventsOpenFull.FragmentAvailableEventsOpenedFull;
import com.example.project.setname.Interfaces.EventsOpenFull;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.List;

public class FragmentAvailableEvents extends Fragment implements EventsOpenFull, FragmentAvailableEventsOpenedFull.DeleteListenerOpenedFull {

    //views
    private View view;
    private SwipeRefreshLayout swipeRefreshLayout;

    //forRV
    private List<String> neverShow_list;
    private RecyclerView event_list_rv;
    private List<ModelEventAvailable> event_list;
    private AdapterAvailableEvents adapterAvailableEvents;

    //service
    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;
    private DocumentSnapshot lastVisible;

    //vars
    private boolean canLoadMore = true;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

            view = inflater.inflate(R.layout.fragment_event_available, container, false);

            setFirebase();

            setViews();

            setRV();

            loadFirebase();

            setScrollListener();

        return view;

    }

    private void setScrollListener() {

        event_list_rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                //метод проверяет достиг ли пользователь bottom, если да, то подгружает еще данные(нужно чтобы не сгрузить всю БД за раз)
                if (!recyclerView.canScrollVertically(1)) {

                    if (canLoadMore) {

                        loadMore();

                    }

                }

            }
        });

    }

    private void setRV() {

        swipeRefreshLayout.setOnRefreshListener(() -> {

            refreshItems();

        });

        event_list = new ArrayList<>();
        neverShow_list = new ArrayList<>();

        adapterAvailableEvents = new AdapterAvailableEvents(event_list, this);

    }

    private void setViews() {

        swipeRefreshLayout = view.findViewById(R.id.fragment_event_available_srl);

        event_list_rv = view.findViewById(R.id.fragment_event_available_rv);

    }

    private void setFirebase() {

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

    }

    private void deleteItem(long positionInList) {

        event_list.remove((int) positionInList);
        adapterAvailableEvents.notifyItemRemoved((int) positionInList);
        refreshItems();

    }

    private void loadFirebase() {

        if (neverShow_list.size() == 0) {

            loadNeverShow();

        }

        firebaseFirestore.collection("Events")
                .limit(10)
                .addSnapshotListener((documentSnapshots, e) -> {

                    if (documentSnapshots.size() > 0) {

                        for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {
                            if (doc.getType() == DocumentChange.Type.ADDED) {

                                String id = doc.getDocument().getId();

                                if (!neverShow_list.contains(id)) {//пока нет выборки

                                    ModelEventAvailable modelEventAvailable = doc.getDocument().toObject(ModelEventAvailable.class).withId(id);
                                    event_list.add(modelEventAvailable);
                                    adapterAvailableEvents.notifyDataSetChanged();

                                }

                            }

                            event_list_rv.setLayoutManager(new LinearLayoutManager(getContext()));
                            event_list_rv.setAdapter(adapterAvailableEvents);

                        }

                        lastVisible = documentSnapshots.getDocuments().get(documentSnapshots.size() - 1);

                    }

                });

    }

    private void loadNeverShow() {

        firebaseFirestore.collection("Users")
                .document(firebaseAuth.getCurrentUser().getUid())
                .collection("NeverShow(Events)")
                .addSnapshotListener((documentSnapshots, e) -> {

                    if (documentSnapshots.size() > 0) {

                        for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {
                            if (doc.getType() == DocumentChange.Type.ADDED) {

                                String id = doc.getDocument().getId();

                                neverShow_list.add(id);

                            }

                        }
                    }

                });

    }

    private void loadMore() {

        canLoadMore = false;

        firebaseFirestore.collection("Events")
                .startAfter(lastVisible)
                .limit(10)
                .addSnapshotListener((documentSnapshots, e) -> {

                    if (documentSnapshots.size() > 0) {

                        for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {
                            if (doc.getType() == DocumentChange.Type.ADDED) {

                                String id = doc.getDocument().getId();

                                ModelEventAvailable modelEventAvailable = doc.getDocument().toObject(ModelEventAvailable.class).withId(id);
                                event_list.add(modelEventAvailable);
                                adapterAvailableEvents.notifyDataSetChanged();

                            }

                            event_list_rv.setLayoutManager(new LinearLayoutManager(getContext()));
                            event_list_rv.setAdapter(adapterAvailableEvents);

                        }

                        lastVisible = documentSnapshots.getDocuments().get(documentSnapshots.size() - 1);

                        canLoadMore = true;

                    }

                });

    }

    private void refreshItems() {

        neverShow_list.clear();
        event_list.clear();
        adapterAvailableEvents.notifyDataSetChanged();

        loadFirebase();

        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void openFull(String fragment, String id, long position) {
        FragmentAvailableEventsOpenedFull fragmentAvailableEventsOpenedFull = new FragmentAvailableEventsOpenedFull();
        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        bundle.putString("fragment", fragment);
        bundle.putLong("position", position);
        fragmentAvailableEventsOpenedFull.setArguments(bundle);
        fragmentAvailableEventsOpenedFull.setTargetFragment(FragmentAvailableEvents.this, 1);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left);
        fragmentTransaction.add(R.id.main_container, fragmentAvailableEventsOpenedFull, "FRAGMENT_AVAILABLE_EVENTS_OPENED_FULL");
        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();
    }

    @Override
    public void getStateOpenedFull(long positionInList, boolean thisState) {
        if (thisState) {

            deleteItem(positionInList);

        }
    }

}
