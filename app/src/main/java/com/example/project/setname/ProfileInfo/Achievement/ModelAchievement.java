package com.example.project.setname.ProfileInfo.Achievement;

public class ModelAchievement {

    public String icon_url, name_of_achievement, description_of_achievement, points_of_achievement, current_percent_of_achievement, max_percent_of_achievement;

    public ModelAchievement(String icon_url, String name_of_achievement, String description_of_achievement,
                            String points_of_achievement, String current_percent_of_achievement, String max_percent_of_achievement) {
        this.icon_url = icon_url;
        this.name_of_achievement = name_of_achievement;
        this.description_of_achievement = description_of_achievement;
        this.points_of_achievement = points_of_achievement;
        this.current_percent_of_achievement = current_percent_of_achievement;
        this.max_percent_of_achievement = max_percent_of_achievement;
    }

    public ModelAchievement() {
    }

    public ModelAchievement(String icon_url, String points_of_achievement) {
        this.icon_url = icon_url;
        this.points_of_achievement = points_of_achievement;
    }

    public String getIcon_url() {
        return icon_url;
    }

    public void setIcon_url(String icon_url) {
        this.icon_url = icon_url;
    }

    public String getName_of_achievement() {
        return name_of_achievement;
    }

    public void setName_of_achievement(String name_of_achievement) {
        this.name_of_achievement = name_of_achievement;
    }

    public String getDescription_of_achievement() {
        return description_of_achievement;
    }

    public void setDescription_of_achievement(String description_of_achievement) {
        this.description_of_achievement = description_of_achievement;
    }

    public String getPoints_of_achievement() {
        return points_of_achievement;
    }

    public void setPoints_of_achievement(String points_of_achievement) {
        this.points_of_achievement = points_of_achievement;
    }

    public String getCurrent_percent_of_achievement() {
        return current_percent_of_achievement;
    }

    public void setCurrent_percent_of_achievement(String current_percent_of_achievement) {
        this.current_percent_of_achievement = current_percent_of_achievement;
    }

    public String getMax_percent_of_achievement() {
        return max_percent_of_achievement;
    }

    public void setMax_percent_of_achievement(String max_percent_of_achievement) {
        this.max_percent_of_achievement = max_percent_of_achievement;
    }
}
