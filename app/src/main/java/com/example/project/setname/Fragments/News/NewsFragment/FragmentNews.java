package com.example.project.setname.Fragments.News.NewsFragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Toast;

import com.example.project.setname.Fragments.News.FragmentOpenLink;
import com.example.project.setname.Fragments.News.NewsFull.FragmentNewsFull;
import com.example.project.setname.Interfaces.GetFragment;
import com.example.project.setname.Interfaces.NewsOpenWeb;
import com.example.project.setname.Interfaces.NewsOpenFull;
import com.example.project.setname.Interfaces.OpenProfileInfo;
import com.example.project.setname.ProfileInfo.FragmentProfileInfo;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.novoda.merlin.Merlin;
import com.novoda.merlin.registerable.connection.Connectable;
import com.novoda.merlin.registerable.disconnection.Disconnectable;
import com.tuyenmonkey.mkloader.MKLoader;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FragmentNews extends Fragment implements NewsOpenFull, OpenProfileInfo, NewsOpenWeb, GetFragment {

    /**
      Я не знаю как сделать выборку по БД по-человечески, потому что, насколько я понимаю, он накидывает фильтр на фильтр и т.д..
      Но мне надо чтобы он накидывал все на один фильтр, поэтому пока что будет такое решение:
      Брать из файла user_id и смотреть если ли в подписках. Да это ппц как невыгодно по памяти, но другого варианта на данный момент, не вижу.
      Последняя адекватная версия: 0.0.44

      Версия 0.0.47
      Решил убрать обновление по-человечески
      Теперь тупо удаляю список и гружу заново
      Появилась идея, что сначала сгружать все в один список, потом загружать в основной и ждать немного, так скакать список не будет


      <p>
      Логика:
      Сгрузить подписки в список.
      Проходить по новостям и сгружать то, что подходит.
     */

    /*
     * Баги:
     * Весь код
     */

    private MKLoader mkLoader;

    //загрузка(как в YouTube)
    /*private SkeletonScreen skeletonScreen;*/

    private static final String TAG = "FragmentNews";

    private static ArrayList<String> subscriptionsList = new ArrayList<>();//список подписок

    private FirebaseAuth firebaseAuth;
    private Merlin merlin;//для провекрки на подключение
    private String current_user;
    private FirebaseFirestore firebaseFirestore;
    private CollectionReference ref;

    private View view;

    private RecyclerView news_list_view;
    private List<ModelNews> news_list;
    private List<ModelNews> news_list_for_load;
    private AdapterNews adapterNews;
    private SwipeRefreshLayout swipeRefreshLayout;

    private FragmentNewsFull fragmentNewsFull;
    private FragmentProfileInfo fragmentProfileInfo;
    private FragmentOpenLink fragmentOpenLink;

    private DocumentSnapshot lastVisible;//для подгрузки
    private DocumentSnapshot firstVisible;//для обновления

    private boolean firstLoad = false;//первая подгрузка
    private boolean canLoadMore = true;//чтобы грешники не сгрузили данные два раза


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {

            view = inflater.inflate(R.layout.fragment_news, container, false);

            setViews();

            setClasses();

            setFirebase();

            setRV();

            setDisconnection();

            setMerlin();

        }

        return view;
    }

    //можно удалить ибо у Firebase есть метод на проверку соединения
    private void setMerlin() {

        merlin = new Merlin.Builder()
                .withConnectableCallbacks()
                .withDisconnectableCallbacks()
                .build(getContext());

        merlin.registerConnectable(new Connectable() {
            @Override
            public void onConnect() {

                setConnection();

            }
        });

        merlin.registerDisconnectable(new Disconnectable() {
            @Override
            public void onDisconnect() {

                setDisconnection();

            }
        });

    }

    private void setClasses() {

        fragmentProfileInfo = new FragmentProfileInfo();
        fragmentNewsFull = new FragmentNewsFull();
        fragmentOpenLink = new FragmentOpenLink();

    }

    private void setFirebase() {

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        current_user = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

    }

    private void setViews() {

        swipeRefreshLayout = view.findViewById(R.id.fragment_news_swipe_refresh);
        news_list_view = view.findViewById(R.id.news_view_recycler);
        mkLoader = view.findViewById(R.id.mk_loader);

    }

    private void setRV() {

        ref = firebaseFirestore.collection("News");
        news_list = new ArrayList<>();
        news_list_for_load = new ArrayList<>();

        adapterNews = new AdapterNews(news_list, this, this, this);

        news_list_view.setLayoutManager(new LinearLayoutManager(getContext()));
        news_list_view.setAdapter(adapterNews);

        setLoadView();
        /*mkLoader.setVisibility(View.GONE);*/

        //прогрузка данных
        /*skeletonScreen = Skeleton.bind(news_list_view)
                .color(R.color.grey3)
                .adapter(adapterNews)
                .load(R.layout.item_news)
                .duration(800)
                .show();*/

        if (firebaseAuth.getCurrentUser() != null) {

            loadFirst();

        }

    }

    private void setLoadView() {

        news_list_view.setVisibility(View.INVISIBLE);
        mkLoader.setVisibility(View.VISIBLE);

    }

    private void setConnection() {

        swipeRefreshLayout.setOnRefreshListener(() -> {

            setLoadView();

            refreshItems();

        });

        news_list_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                //метод проверяет достиг ли пользователь bottom, если да, то подгружает еще данные(нужно чтобы не сгрузить всю БД за раз)
                if (!recyclerView.canScrollVertically(1)) {

                    if (canLoadMore) {

                        loadAdditionally();

                        Log.i(TAG, "Load more");

                    }

                }

            }
        });

    }

    private void setDisconnection() {

        swipeRefreshLayout.setOnRefreshListener(() -> {

            Toast.makeText(getContext().getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();

            swipeRefreshLayout.setRefreshing(false);

        });

        news_list_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });

    }

    private void loadFirst() {


        firebaseFirestore.collection("Users")
                .document(current_user)
                .collection("Subscriptions")
                .addSnapshotListener((documentSnapshots, e) -> {

                    for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {

                        if (doc.getType() == DocumentChange.Type.ADDED) {

                            subscriptionsList.add(doc.getDocument().getId());

                        }
                    }

                });

        ref.orderBy("timestamp", Query.Direction.DESCENDING)
                /*.limit(10)*///с выборкой
                .limit(50)//пока не сделал выборку
                .addSnapshotListener((documentSnapshots1, e1) -> {

                    if (!firstLoad) {

                        Log.i(TAG, "Size after: " + subscriptionsList.size());

                        for (DocumentChange doc1 : documentSnapshots1.getDocumentChanges()) {

                            if (news_list_for_load.size() < 10) {//без выборки

                                if (doc1.getType() == DocumentChange.Type.ADDED) {

                                    if (subscriptionsList.contains(doc1.getDocument().getString("user_id"))) {//без выборки

                                        String newsId = doc1.getDocument().getId();

                                        /*if (news_list.size() == 0) {
                                            firstVisible = doc1.getDocument();
                                            Log.i(TAG, firstVisible.toString());
                                        }*/

                                        ModelNews publicNews = doc1.getDocument().toObject(ModelNews.class).withId(newsId);
                                        news_list_for_load.add(publicNews);
                                        adapterNews.notifyDataSetChanged();

                                        lastVisible = doc1.getDocument();

                                    }
                                }

                            } else {

                                Log.i(TAG, "Complete first loaded");

                                firstLoad = true;

                                break;
                            }
                        }


                    }
                    news_list_view.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            /*skeletonScreen.hide();*/

                            news_list.addAll(news_list_for_load);
                            adapterNews.notifyDataSetChanged();
                            news_list_view.setHasFixedSize(true);
                            news_list_for_load.clear();

                            Log.i(TAG, "Size: " + news_list.size());

                            mkLoader.setVisibility(View.GONE);
                            news_list_view.setVisibility(View.VISIBLE);

                        }
                    }, 2000);


                });
    }

    private void loadAdditionally() {

        canLoadMore = false;

        news_list_view.setHasFixedSize(false);

        firebaseFirestore.collection("News")
                .orderBy("timestamp", Query.Direction.DESCENDING)
                .startAfter(lastVisible)
                /*.limit(10)*/
                .limit(50)
                .addSnapshotListener((documentSnapshots1, e1) -> {

                    assert documentSnapshots1 != null;

                    if (!documentSnapshots1.isEmpty()
                            && lastVisible != documentSnapshots1.getDocuments().get(0)) {

                        int sizeList = news_list.size() + 10;//без выборки

                        for (DocumentChange doc1 : documentSnapshots1.getDocumentChanges()) {

                            if (news_list.size() < sizeList) {//без выборки

                                if (doc1.getType() == DocumentChange.Type.ADDED) {

                                    String newsId = doc1.getDocument().getId();

                                    if (subscriptionsList.contains(doc1.getDocument().getString("user_id"))) {//без выборки

                                        ModelNews publicNews = doc1.getDocument().toObject(ModelNews.class).withId(newsId);
                                        news_list.add(publicNews);
                                        adapterNews.notifyDataSetChanged();

                                        lastVisible = doc1.getDocument();

                                    }
                                }

                            } else {

                                Log.i(TAG, "Complete additional load");

                                break;
                            }
                        }

                        news_list_view.setHasFixedSize(true);

                    }

                    canLoadMore = true;

                });

    }


    /**
     * @param bmp
     * @param username
     * @param text
     * @param news_id  Метод openFull нужен для открытия картинки на полный экран
     *                 Чтобы не обращаться снова к серверу преобразую в массив и передаю
     *                 в фрагмент, так обратно в картинку
     *                 <p>
     *                 Строка, поснение:
     *                 -439: Ловлю для того чтобы, если на новости не было картинки, преобразовать
     *                 PlaceHolder, для того чтобы в открывающимся фрагменте проверить на длинну(
     *                 т.к. элемент один и тот же, то и длина одна и та же) и в случае соответствия
     *                 скрыть пикчу
     */

    @Override
    public void openFull(Bitmap bmp,
                         String username,
                         String text,
                         String news_id) {

        assert getFragmentManager() != null;
        Bundle bundle = new Bundle();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        try {
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            bundle.putByteArray("image", byteArray);

        } catch (NullPointerException ignored) {

            byte[] byteArray = new byte[0];
            bundle.putByteArray("image", byteArray);

        }


        bundle.putString("username", username);
        bundle.putString("text", text);
        bundle.putString("news_id", news_id);

        fragmentNewsFull.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left);
        fragmentTransaction.add(R.id.main_container, fragmentNewsFull, "NEWS_FULL");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    @Override
    public void openProfile(String user) {
        assert getFragmentManager() != null;
        Bundle bundle = new Bundle();
        bundle.putString("user", user);
        fragmentProfileInfo.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left);
        fragmentTransaction.add(R.id.main_container, fragmentProfileInfo, "FRAGMENT_PROFILE_INFO");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void refreshItems() {

        news_list.clear();
        adapterNews.notifyDataSetChanged();

        subscriptionsList.clear();

        firstLoad = false;
        loadFirst();

        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onDestroyView() {
        if (view.getParent() != null) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        merlin.bind();
    }

    @Override
    public void onPause() {
        merlin.unbind();
        super.onPause();
    }

    @Override
    public void openWeb(String link) {
        assert getFragmentManager() != null;
        Bundle bundle = new Bundle();
        bundle.putString("link", link);
        fragmentOpenLink.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left);
        fragmentTransaction.add(R.id.main_container, fragmentOpenLink, "FRAGMENT_WEB");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public String getCustomFragment() {
        return "FragmentNews";
    }
}
