package com.example.project.setname.Fragments.Jobs.FragmentJobAvailable;

import java.util.Date;

public class ModelJobAvailable extends ModelJobAvailableId {

    public String user_id, language, type, price, description, payment, level;
    public Date timestamp, deadline;

    public ModelJobAvailable() {
    }

    public ModelJobAvailable(String user_id, String language,
                             String type, String price,
                             String description, String payment,
                             String level, Date timestamp,
                             Date deadline) {
        this.user_id = user_id;
        this.language = language;
        this.type = type;
        this.price = price;
        this.description = description;
        this.payment = payment;
        this.level = level;
        this.timestamp = timestamp;
        this.deadline = deadline;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }
}
