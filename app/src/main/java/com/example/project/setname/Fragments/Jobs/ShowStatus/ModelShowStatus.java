package com.example.project.setname.Fragments.Jobs.ShowStatus;

import java.util.Date;

public class ModelShowStatus {

    String text;
    Date timestamp;

    public ModelShowStatus() {
    }

    public ModelShowStatus(String text, Date timestamp) {
        this.text = text;
        this.timestamp = timestamp;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
