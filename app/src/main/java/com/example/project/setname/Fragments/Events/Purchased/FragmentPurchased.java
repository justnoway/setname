package com.example.project.setname.Fragments.Events.Purchased;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.project.setname.Fragments.Events.AvailableEvents.AvailableEventsOpenFull.FragmentAvailableEventsOpenedFull;
import com.example.project.setname.Fragments.Events.AvailableEvents.FragmentAvailableEvents;
import com.example.project.setname.Fragments.Events.Purchased.ShowTicker.FragmentPassCode;
import com.example.project.setname.Fragments.Events.Purchased.ShowTicker.FragmentShowTicket;
import com.example.project.setname.Interfaces.OpenFull;
import com.example.project.setname.R;
import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class FragmentPurchased extends Fragment implements OpenFull {

    //views
    private View view;
    private SwipeRefreshLayout swipeRefreshLayout;

    //service
    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;
    private DocumentSnapshot lastVisible;

    //forRV
    private AdapterPurchased adapterPurchased;
    private RecyclerView purchasedRV;
    private List<ModelPurchased> purchased_list;

    //vars
    private boolean canLoadMore = true;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_purchased, container, false);

        setFirebase();

        setRV();

        setScrollListener();

        firstLoadFirebase();

        return view;

    }

    private void setFirebase() {

        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();

    }

    private void setRV() {

        swipeRefreshLayout = view.findViewById(R.id.fragment_purchased_srl);

        purchased_list = new ArrayList<>();

        purchasedRV = view.findViewById(R.id.fragment_purchased_rv);

        adapterPurchased = new AdapterPurchased(purchased_list, this);

        purchasedRV.setLayoutManager(new LinearLayoutManager(getContext()));
        purchasedRV.setAdapter(adapterPurchased);

        swipeRefreshLayout.setOnRefreshListener(() -> {

            refreshItems();

        });


    }

    private void setScrollListener() {

        purchasedRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                //метод проверяет достиг ли пользователь bottom, если да, то подгружает еще данные(нужно чтобы не сгрузить всю БД за раз)
                if (!recyclerView.canScrollVertically(1)) {

                    if (canLoadMore) {

                        loadMore();

                    }

                }

            }
        });

    }

    private void refreshItems() {

        purchased_list.clear();
        adapterPurchased.notifyDataSetChanged();

        firstLoadFirebase();

        swipeRefreshLayout.setRefreshing(false);

    }

    private void firstLoadFirebase() {

        firebaseFirestore.collection("Users")
                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .collection("MyTickets")
                .limit(10)
                .addSnapshotListener((documentSnapshots, e) -> {

                    if (documentSnapshots.size() > 0) {

                        for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {
                            if (doc.getType() == DocumentChange.Type.ADDED) {

                                String id = doc.getDocument().getId();

                                ModelPurchased modelPurchased = doc.getDocument().toObject(ModelPurchased.class).withId(id);
                                purchased_list.add(modelPurchased);
                                adapterPurchased.notifyDataSetChanged();

                            }

                        }

                        lastVisible = documentSnapshots.getDocuments().get(documentSnapshots.size() - 1);

                    }

                });

    }

    private void loadMore() {

        canLoadMore = false;

        firebaseFirestore.collection("Users")
                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .collection("MyTickets")
                .startAfter(lastVisible)
                .limit(10)
                .addSnapshotListener((documentSnapshots, e) -> {

                    if (documentSnapshots.size() > 0) {

                        for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {
                            if (doc.getType() == DocumentChange.Type.ADDED) {

                                String id = doc.getDocument().getId();

                                ModelPurchased modelPurchased = doc.getDocument().toObject(ModelPurchased.class).withId(id);
                                purchased_list.add(modelPurchased);
                                adapterPurchased.notifyDataSetChanged();

                            }

                        }

                        lastVisible = documentSnapshots.getDocuments().get(documentSnapshots.size() - 1);

                    }

                });

        canLoadMore = true;

    }

    @Override
    public void openFull(String fragment, String id) {

        FragmentAvailableEventsOpenedFull fragmentAvailableEventsOpenedFull = new FragmentAvailableEventsOpenedFull();
        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        bundle.putString("fragment", fragment);
        fragmentAvailableEventsOpenedFull.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left);
        fragmentTransaction.add(R.id.main_container, fragmentAvailableEventsOpenedFull, "FRAGMENT_AVAILABLE_EVENTS_OPENED_FULL");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    @Override
    public void openFull(String id) {

        FragmentPassCode fragmentPassCode = new FragmentPassCode();
        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        fragmentPassCode.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left);
        fragmentTransaction.add(R.id.main_container, fragmentPassCode, "FRAGMENT_PASSCODE");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

}
