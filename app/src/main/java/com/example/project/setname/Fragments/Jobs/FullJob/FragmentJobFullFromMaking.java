package com.example.project.setname.Fragments.Jobs.FullJob;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.project.setname.Chat.FragmentChat;

import com.example.project.setname.Fragments.Jobs.SendCode.FragmentSendCode;
import com.example.project.setname.R;
import com.gigamole.navigationtabstrip.NavigationTabStrip;

import me.yokeyword.fragmentation_swipeback.SwipeBackFragment;

public class FragmentJobFullFromMaking extends SwipeBackFragment {

    private ViewPager mViewPager;

    private FragmentChat fragmentChat;
    private FragmentJobFullOpenedItem jobFullOpenedItem;
    private FragmentSendCode fragmentSendCode;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_with_job, container, false);

        Bundle bundle = new Bundle();
        bundle.putString("companion", getArguments().getString("companion"));

        fragmentChat = new FragmentChat();
        fragmentChat.setArguments(bundle);

        jobFullOpenedItem = new FragmentJobFullOpenedItem();
        jobFullOpenedItem.setArguments(getArguments());

        fragmentSendCode = new FragmentSendCode();
        fragmentSendCode.setArguments(getArguments());

        mViewPager = view.findViewById(R.id.fragment_chat_with_job_vp);

        final NavigationTabStrip navigationTabStrip = (NavigationTabStrip) view.findViewById(R.id.fragment_chat_with_job_nts);
        navigationTabStrip.setTitles("FullItem", "Chat", "CodeList");

        navigationTabStrip.setStripWeight(3);

        mViewPager.setAdapter(new MyPagerAdapter(getChildFragmentManager()));

        navigationTabStrip.setViewPager(mViewPager);

        return attachToSwipeBack(view);
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch(pos) {
                case 0: return jobFullOpenedItem;
                case 1: return fragmentChat;
                case 2: return fragmentSendCode;
                default: return null;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
