package com.example.project.setname.Fragments.Jobs.Reconciliations;

import android.support.annotation.NonNull;

import com.google.firebase.firestore.Exclude;

public class ModelJobReconciliationId {

    @Exclude
    public String ConstructorJobFromMakerId;

    public <T extends ModelJobReconciliationId> T withId(@NonNull final String id){
        this.ConstructorJobFromMakerId = id;
        return (T) this;
    }

}