package com.example.project.setname.Fragments.Jobs.SendCode;

import android.support.annotation.NonNull;

import com.google.firebase.firestore.Exclude;

public class ModelIdSendCode {

    @Exclude
    public String ModelIdSendCode;

    public <T extends ModelIdSendCode> T withId(@NonNull final String id){
        this.ModelIdSendCode = id;
        return (T) this;
    }

}
