package com.example.project.setname.Fragments.Events.Purchased.ShowTicker;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.project.setname.Fragments.Events.AvailableEvents.FragmentAvailableEvents;
import com.example.project.setname.Fragments.Events.AvailableEvents.FragmentAvailableForViewPager;
import com.example.project.setname.Fragments.Events.Purchased.FragmentPurchased;
import com.example.project.setname.Fragments.Events.Purchased.FragmentPurchasedForViewPager;
import com.example.project.setname.R;
import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import net.glxn.qrgen.android.QRCode;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import me.yokeyword.fragmentation_swipeback.SwipeBackFragment;

public class FragmentShowTicket extends SwipeBackFragment {

    private View view;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    private String currentUser;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();

        currentUser = firebaseAuth.getCurrentUser().getUid();

        view = inflater.inflate(R.layout.fragment_show_ticket, container, false);

        assert getArguments() != null;

        firebaseFirestore.collection("Users")
                .document(currentUser)
                .collection("MyTickets")
                .document(Objects.requireNonNull(getArguments().getString("id")))
                .get().addOnCompleteListener(task -> {

            if (task.isComplete()) {
                Bitmap myBitmap = QRCode.from(task.getResult().getString("key")).bitmap();
                ImageView myImage = view.findViewById(R.id.fragment_show_ticker_image);
                myImage.setImageBitmap(myBitmap);
            }
        });

        return attachToSwipeBack(view);

    }
}
