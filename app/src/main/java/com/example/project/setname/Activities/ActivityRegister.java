package com.example.project.setname.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.project.setname.MainActivity.MainActivity;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import in.shadowfax.proswipebutton.ProSwipeButton;
import me.yokeyword.fragmentation_swipeback.SwipeBackActivity;

public class ActivityRegister extends SwipeBackActivity {

    //views
    private EditText reg_email_field;
    private EditText reg_pass_field;
    private EditText reg_confirm_pass_field;
    private ProSwipeButton reg_btn;

    //service
    private FirebaseAuth mAuth;
    private FirebaseFirestore firebaseFirestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        setFirebase();

        setViews();

        setListeners();

    }

    private void setListeners() {

        reg_btn.setOnSwipeListener(() -> {
            new Handler().postDelayed(() -> {

                String email = reg_email_field.getText().toString();
                String pass = reg_pass_field.getText().toString();
                String confirm_pass = reg_confirm_pass_field.getText().toString();

                if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(pass) & !TextUtils.isEmpty(confirm_pass)) {

                    if (pass.equals(confirm_pass)) {

                        mAuth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(task -> {

                            if (task.isSuccessful()) {

                                Map<String, Object> map = new HashMap<>();
                                map.put("timestamp", FieldValue.serverTimestamp());

                                //подписка самого на себя
                                firebaseFirestore.collection("Users")
                                        .document(Objects.requireNonNull(mAuth.getCurrentUser()).getUid())
                                        .collection("Subscriptions")
                                        .document(Objects.requireNonNull(mAuth.getCurrentUser()).getUid())
                                        .set(map).addOnCompleteListener(task1 -> {



                                });

                                Intent setupIntent = new Intent(ActivityRegister.this, ActivitySetup.class);
                                startActivity(setupIntent);
                                finish();

                                map.clear();
                                map.put("icon_url", "https://www.shareicon.net/download/2016/12/19/863777_win_512x512.png");//ачивка
                                map.put("points_of_achievement", "100");

                                firebaseFirestore.collection("Users")
                                        .document(Objects.requireNonNull(mAuth.getCurrentUser()).getUid())
                                        .collection("Achievement")
                                        .add(map)
                                        .addOnCompleteListener(task1 -> {

                                        });

                                reg_btn.showResultIcon(true);


                            } else {

                                String errorMessage = task.getException().getMessage();
                                Toast.makeText(ActivityRegister.this, "Error : " + errorMessage, Toast.LENGTH_LONG).show();

                                reg_btn.showResultIcon(false);

                            }

                        });

                    } else {

                        Toast.makeText(ActivityRegister.this, "Confirm Password and Password Field doesn't match.", Toast.LENGTH_LONG).show();

                        reg_btn.showResultIcon(false);

                    }
                }
            }, 2000);
        });

    }

    private void setViews() {

        reg_email_field = findViewById(R.id.reg_email);
        reg_pass_field = findViewById(R.id.reg_pass);
        reg_confirm_pass_field = findViewById(R.id.reg_confirm_pass);
        reg_btn = findViewById(R.id.reg_btn);

    }

    private void setFirebase() {

        mAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {

            sendToMain();

        }

    }

    private void sendToMain() {

        Intent mainIntent = new Intent(ActivityRegister.this, MainActivity.class);
        startActivity(mainIntent);
        finish();

    }
}

