package com.example.project.setname.Fragments.News.NewsNew;

import android.annotation.SuppressLint;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.support.v4.app.Fragment;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.example.project.setname.Dialogs.DialogNewsNewShowImage;
import com.example.project.setname.Dialogs.DialogNewsNewShowWeb;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import in.shadowfax.proswipebutton.ProSwipeButton;
import me.yokeyword.fragmentation_swipeback.SwipeBackFragment;

import static android.content.Context.CLIPBOARD_SERVICE;

public class FragmentNewNews extends Fragment {

    private static final String TAG = "FragmentNewNews";

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    private String image_url;
    private DialogNewsNewShowImage dialogNewsNewShowImage;

    private String web_link;
    private DialogNewsNewShowWeb dialogNewsNewShowWeb;

    private ProSwipeButton proSwipeBtn;
    private Button saveImage;
    private Button saveLink;
    private EditText field;

    private ClipboardManager clipboard;

    public static AtomicReference<String> imageList = new AtomicReference<>();
    private AtomicReference<String> linkList = new AtomicReference<>();

    private HashMap<String, Object> map = new HashMap<>();

    //чтобы упростить задачу
    private boolean image = false;
    private boolean text = false;
    private boolean link = false;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();

        imageList.set("");
        linkList.set("");

        View view = inflater.inflate(R.layout.fragment_news_new_post, container, false);

        saveImage = view.findViewById(R.id.fragment_news_new_post_btn);
        saveLink = view.findViewById(R.id.fragment_news_new_post_link);

        proSwipeBtn = (ProSwipeButton) view.findViewById(R.id.fragment_news_new_post_sb);

        field = view.findViewById(R.id.fragment_news_new_post_et);

        clipboard = (ClipboardManager) Objects.requireNonNull(getActivity()).getSystemService(CLIPBOARD_SERVICE);

        assert clipboard != null;

        if (isConnected(Objects.requireNonNull(getContext()))) {

            saveImageListener();

            saveLinkListener();

            setProSwipe();

        }else {

            proSwipeBtn.setFocusable(false);
            proSwipeBtn.setClickable(false);
            saveLink.setFocusable(false);
            saveLink.setClickable(false);
            saveImage.setFocusable(false);
            saveImage.setClickable(false);

        }


        return view;
    }

    public static boolean isConnected(@NonNull Context context) {
        ConnectivityManager
                cm = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
    }

    private void setProSwipe() {

        proSwipeBtn.setOnSwipeListener(() -> {
            new Handler().postDelayed(() -> {

                map.put("user_id", firebaseAuth.getCurrentUser().getUid());
                map.put("timestamp", FieldValue.serverTimestamp());

                if (!imageList.get().equals("None")&&!imageList.get().equals("")) {
                    image = true;
                    map.put("image_url", imageList.get());
                    Log.i(TAG, "Image");
                } else {
                    map.put("image_url", "");
                }
                if (!field.getText().toString().equals("")) {
                    text = true;
                    map.put("text", field.getText().toString());
                    Log.i(TAG, "Text");
                } else {
                    map.put("text", "");
                }
                if (URLUtil.isValidUrl(web_link)) {
                    link = true;
                    map.put("link", linkList.get());
                    Log.i(TAG, "Link");
                } else {
                    map.put("link", "");
                }
                if (image && text && link){

                    Log.i(TAG, "Complete: " + "image&&text&&link");

                    postData();

                }

                else if (image && text) {

                    Log.i(TAG, "Complete: " + "image&&text");

                    postData();


                } else if (image && link) {

                    Log.i(TAG, "Complete: " + "image&&link");

                    postData();

                } else if (text && link) {

                    Log.i(TAG, "Complete: " + "text&&link");

                    postData();

                } else if (text) {

                    Log.i(TAG, "Complete: " + "text");

                    postData();

                } else if (image) {

                    Log.i(TAG, "Complete: " + "image");

                    postData();

                } else {

                    Log.i(TAG, "Not this time");

                    map.clear();

                    Toast.makeText(getContext(), "No news", Toast.LENGTH_SHORT).show();

                    proSwipeBtn.showResultIcon(false);
                }

            }, 2000);
        });

    }

    @SuppressLint("ClickableViewAccessibility")
    private void saveLinkListener() {

        saveLink.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    web_link = clipboard.getText().toString();

                    dialogNewsNewShowWeb = new DialogNewsNewShowWeb();

                    assert getFragmentManager() != null;
                    Bundle bundle = new Bundle();
                    bundle.putString("link", web_link);
                    dialogNewsNewShowWeb.setArguments(bundle);
                    dialogNewsNewShowWeb.show(getFragmentManager(), "DIALOG_NEWS_NEW_SHOW_WEB");

                    linkList.set(web_link);

                    Log.i(TAG, "Link is: " + linkList.get());

                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {

                    dialogNewsNewShowWeb.hideIt();

                    return true;
                }
                return false;
            }
        });

    }

    @SuppressLint("ClickableViewAccessibility")
    private void saveImageListener() {

        saveImage.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    image_url = clipboard.getText().toString();

                    dialogNewsNewShowImage = new DialogNewsNewShowImage();

                    assert getFragmentManager() != null;
                    Bundle bundle = new Bundle();
                    bundle.putString("image_url", image_url);
                    dialogNewsNewShowImage.setArguments(bundle);
                    dialogNewsNewShowImage.show(getFragmentManager(), "DIALOG_NEWS_NEW_SHOW_IMAGE");

                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {

                    dialogNewsNewShowImage.hideIt();


                    return true;
                }
                return false;
            }
        });

    }

    void postData() {

        firebaseFirestore.collection("News")
                .add(map).addOnCompleteListener(task -> {

            if (task.isComplete()) {

                proSwipeBtn.showResultIcon(true);

                field.setText("");
                imageList.set("");
                linkList.set("");

                link = false;
                image = false;
                text = false;

            } else {

                proSwipeBtn.showResultIcon(false);
                Toast.makeText(getContext(), "Error:(", Toast.LENGTH_SHORT).show();

            }
        });

        map.clear();

    }

}