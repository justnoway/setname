package com.example.project.setname.Dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import fr.tvbarthel.lib.blurdialogfragment.SupportBlurDialogFragment;

public class DialogSendCode extends SupportBlurDialogFragment {

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;
    private String current_user;

    private static final String BUNDLE_KEY_DOWN_SCALE_FACTOR = "bundle_key_down_scale_factor";

    private static final String BUNDLE_KEY_BLUR_RADIUS = "bundle_key_blur_radius";

    private static final String BUNDLE_KEY_DIMMING = "bundle_key_dimming_effect";

    private static final String BUNDLE_KEY_DEBUG = "bundle_key_debug_effect";

    public static DialogSendFromAvailableToRequests newInstance(int radius,
                                                                float downScaleFactor,
                                                                boolean dimming,
                                                                boolean debug) {
        DialogSendFromAvailableToRequests fragment = new DialogSendFromAvailableToRequests();
        Bundle args = new Bundle();
        args.putInt(
                BUNDLE_KEY_BLUR_RADIUS,
                radius
        );
        args.putFloat(
                BUNDLE_KEY_DOWN_SCALE_FACTOR,
                downScaleFactor
        );
        args.putBoolean(
                BUNDLE_KEY_DIMMING,
                dimming
        );
        args.putBoolean(
                BUNDLE_KEY_DEBUG,
                debug
        );

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();
        current_user = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        @SuppressLint("InflateParams") View view = Objects.requireNonNull(getActivity()).getLayoutInflater().inflate(R.layout.dialog_send_code, null);

        String employer_data = getArguments().getString("companion");
        String id_data = getArguments().getString("id");

        String current_user = firebaseAuth.getCurrentUser().getUid();

        EditText field = view.findViewById(R.id.dialog_send_code_field_for_input);
        Button sendCode = view.findViewById(R.id.dialog_send_code_btn);
        sendCode.setOnClickListener(v -> {

            sendCode.setClickable(false);

            HashMap<String, Object> map = new HashMap<>();
            map.put("link", field.getText().toString());
            map.put("timestamp", FieldValue.serverTimestamp());

            assert id_data != null;
            firebaseFirestore.collection("Users")
                    .document(current_user)
                    .collection("Making(Maker)")
                    .document(id_data)
                    .collection("CodeList")
                    .add(map).addOnCompleteListener(task -> {

                if (task.isComplete()) {

                    assert employer_data != null;
                    firebaseFirestore
                            .collection("Users")
                            .document(employer_data)
                            .collection("Making(Maker)")
                            .document(id_data)
                            .collection("CodeList").add(map).addOnCompleteListener(task1 -> {

                        if (task1.isComplete()) {

                            Toast.makeText(getContext(), "Complete send code", Toast.LENGTH_SHORT).show();

                            Map<String, Object> map1 = new HashMap<>();
                            map1.put("text", "Code was send");
                            map1.put("timestamp", FieldValue.serverTimestamp());

                            firebaseFirestore.collection("Users")
                                    .document(current_user)
                                    .collection("Making(Maker)")
                                    .document(id_data)
                                    .collection("Status")
                                    .add(map1).addOnCompleteListener(task12 -> {
                            });

                            firebaseFirestore.collection("Users")
                                    .document(employer_data)
                                    .collection("WaitingOfMaking(Employer)")
                                    .document(id_data)
                                    .collection("Status")
                                    .add(map1).addOnCompleteListener(task12 -> {
                            });


                            getDialog().dismiss();

                        } else {
                            Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                            sendCode.setClickable(true);
                        }
                    });

                } else {
                    Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                    sendCode.setClickable(true);
                }

            });


        });


        builder.setView(view);
        return builder.create();
    }


}

