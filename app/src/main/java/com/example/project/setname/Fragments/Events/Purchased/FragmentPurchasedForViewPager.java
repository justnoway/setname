package com.example.project.setname.Fragments.Events.Purchased;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.project.setname.Fragments.Events.AvailableEvents.AvailableEventsOpenFull.FragmentAvailableEventsOpenedFull;
import com.example.project.setname.Fragments.Events.AvailableEvents.FragmentAvailableEvents;
import com.example.project.setname.Interfaces.OpenFull;
import com.example.project.setname.R;
import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;


public class FragmentPurchasedForViewPager extends Fragment {

    private FragmentPurchased fragmentPurchased;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_events_purchased_vp, container, false);

        fragmentPurchased = new FragmentPurchased();

        replaceFragment(R.id.fragment_event_purchased_vp_cl, fragmentPurchased, "TESTFA");

        return view;
    }

    public void replaceFragment(int R_view, android.support.v4.app.Fragment fragment, String tag){

        assert getFragmentManager() != null;
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R_view, fragment, tag);
        fragmentTransaction.commit();

    }

}
