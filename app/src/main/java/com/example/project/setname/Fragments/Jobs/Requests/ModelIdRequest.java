package com.example.project.setname.Fragments.Jobs.Requests;

import android.support.annotation.NonNull;

import com.google.firebase.firestore.Exclude;

public class ModelIdRequest {

    @Exclude
    public String ModelComeRequestId;

    public <T extends ModelIdRequest> T withId(@NonNull final String id){
        this.ModelComeRequestId = id;
        return (T) this;
    }

}
