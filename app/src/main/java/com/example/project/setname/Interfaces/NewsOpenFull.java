package com.example.project.setname.Interfaces;

import android.graphics.Bitmap;

public interface NewsOpenFull {
    void openFull(Bitmap bmp,
                  String username,
                  String text,
                  String news_id);
}
