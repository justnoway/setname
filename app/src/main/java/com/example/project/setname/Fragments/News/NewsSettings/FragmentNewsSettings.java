package com.example.project.setname.Fragments.News.NewsSettings;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

public class FragmentNewsSettings extends Fragment {

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    private View backgroundOfTextForBtn;
    private TextView countSubscriptions;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.item_news_settings, container, false);

        /*firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();

        backgroundOfTextForBtn = view.findViewById(R.id.item_news_settings_bg);
        countSubscriptions = view.findViewById(R.id.item_news_settings_tv_status);

        String user = "ngyFOM0QXGdjkzaycLimTg8PW8c2";

        Button sub = view.findViewById(R.id.item_news_settings_btn_subscription);
        AtomicBoolean subscribed = new AtomicBoolean(false);

        final String current_user = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        HashMap<String, Object> empty = new HashMap<>();
        empty.put("timestamp", FieldValue.serverTimestamp());

        firebaseFirestore.collection("Users")
                .document(current_user)
                .collection("Subscriptions")
                .document(user).addSnapshotListener((documentSnapshot, e) -> {

            if (documentSnapshot.exists()) {

                setSubscribe(subscribed, backgroundOfTextForBtn, countSubscriptions);

                Toast.makeText(getContext(), "subscribed", Toast.LENGTH_SHORT).show();

            } else {

                setSubscribe(subscribed, backgroundOfTextForBtn, countSubscriptions);
                Toast.makeText(getContext(), "not subscribed", Toast.LENGTH_SHORT).show();

            }

        });


        sub.setOnClickListener(v -> {

            if (!subscribed.get()) {

                firebaseFirestore.collection("Users").document(current_user).collection("Subscriptions").document(user).set(empty).addOnCompleteListener(task -> {
                });

                firebaseFirestore.collection("Users").document(user).collection("Subscribers").document(current_user).set(empty).addOnCompleteListener(task -> {
                });

            } else {
                firebaseFirestore.collection("Users").document(current_user).collection("Subscriptions").document(user).delete().addOnCompleteListener(task -> {
                });

                firebaseFirestore.collection("Users").document(user).collection("Subscribers").document(current_user).delete().addOnCompleteListener(task -> {
                });

            }

        });*/


        return view;
    }

    void setSubscribe(AtomicBoolean subscribed, View view, TextView textView) {
        if (subscribed.get()) {
            view.setBackgroundResource(R.drawable.field_subscribe_btn_selected);
            textView.setText("Subscribed");
            textView.setTextColor(Color.parseColor("#F5F5F5"));
            subscribed.set(false);
        } else {
            view.setBackgroundResource(R.drawable.field_subscribe_btn_not_selected);
            textView.setText("Subscribe");
            textView.setTextColor(Color.parseColor("#90A4AE"));
            subscribed.set(true);
        }
    }
}
