package com.example.project.setname.Chat;

import android.support.annotation.NonNull;

import com.example.project.setname.Fragments.News.NewsFull.Comments.ModelComments;
import com.google.firebase.firestore.Exclude;

public class ModelChatId{

    @Exclude
    public String ModelChatId;
    public String ModeChatModelChatPrevious;

    public <T extends ModelChatId> T withId(@NonNull final String id, @NonNull final String previous){
        this.ModelChatId = id;
        this.ModeChatModelChatPrevious = previous;
        return (T) this;
    }

    public <T extends ModelChatId> T withId(@NonNull final String id){
        this.ModelChatId = id;
        return (T) this;
    }
}
