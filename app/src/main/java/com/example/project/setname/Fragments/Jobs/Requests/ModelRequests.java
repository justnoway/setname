package com.example.project.setname.Fragments.Jobs.Requests;

import java.util.Date;

public class ModelRequests extends ModelIdRequest {

    String price, maker;
    Date deadline;

    public ModelRequests(String price, String maker, Date deadline) {
        this.price = price;
        this.maker = maker;
        this.deadline = deadline;
    }

    public ModelRequests(String price, Date deadline) {
        this.price = price;
        this.deadline = deadline;
    }

    public ModelRequests() {
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }
}
