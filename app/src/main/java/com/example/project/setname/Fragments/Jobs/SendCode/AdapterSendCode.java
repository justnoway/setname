package com.example.project.setname.Fragments.Jobs.SendCode;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.project.setname.Interfaces.NewsOpenFull;
import com.example.project.setname.Interfaces.OpenProfileInfo;
import com.example.project.setname.ProfileInfo.FragmentProfileInfo;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import xyz.hanks.library.bang.SmallBangView;

public class AdapterSendCode extends RecyclerView.Adapter<AdapterSendCode.ViewHolder>{

    private List<ModelSendCode> sendCodes_list;
    public Context context;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    public AdapterSendCode(List<ModelSendCode> sendCodes_list) {
        this.sendCodes_list = sendCodes_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_send_code, parent, false);
        context = parent.getContext();
        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        String link = sendCodes_list.get(position).getLink();
        holder.setLink(link);

        try {
            long millisecond = sendCodes_list.get(position).getTimestamp().getTime();
            String dateString = DateFormat.format("dd MMMM, hh:mm", new Date(millisecond)).toString();
            holder.setDate(dateString);
        } catch (Exception e) {
            Toast.makeText(context, "Exception : " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public int getItemCount() {
        return sendCodes_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private View mView;

        private TextView link;
        private TextView date;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        void setLink(String str){
            link = mView.findViewById(R.id.item_send_code_link);
            link.setText(str);
        }

        void setDate(String str){
            date = mView.findViewById(R.id.item_send_code_date);
            date.setText(str);
        }

    }
}
