package com.example.project.setname.Fragments.News.NewsFragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.project.setname.Interfaces.NewsOpenFull;
import com.example.project.setname.Interfaces.NewsOpenWeb;
import com.example.project.setname.Interfaces.OpenProfileInfo;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import xyz.hanks.library.bang.SmallBangView;

public class AdapterNews extends RecyclerView.Adapter<AdapterNews.ViewHolder> {

    private static final String TAG = "AdapterNews";

    private List<ModelNews> news_list;
    public Context context;

    //Service
    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    //interfaces
    private NewsOpenFull mNewsOpenFull;
    private OpenProfileInfo mOpenProfileInfo;
    private NewsOpenWeb mNewsOpenWeb;

    public AdapterNews(List<ModelNews> news_list,
                       NewsOpenFull newsOpenFull,
                       OpenProfileInfo openProfileInfo,
                       NewsOpenWeb newsOpenWeb) {
        this.news_list = news_list;
        this.mNewsOpenFull = newsOpenFull;
        this.mOpenProfileInfo = openProfileInfo;
        this.mNewsOpenWeb = newsOpenWeb;
    }

    public AdapterNews(List<ModelNews> news_list,
                       NewsOpenFull newsOpenFull,
                       NewsOpenWeb newsOpenWeb) {
        this.news_list = news_list;
        this.mNewsOpenFull = newsOpenFull;
        this.mNewsOpenWeb = newsOpenWeb;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, parent, false);
        context = parent.getContext();
        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final String newsId = news_list.get(position).NewsConstructorId;
        final String current_user = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        String image_data = news_list.get(position).getImage_url();
        holder.setImage(image_data);

        String text_data = news_list.get(position).getText();
        holder.setText(text_data);

        holder.setLink();

        try {
            if (!news_list.get(position).getLink().equals("")) {
                holder.setLink(news_list.get(position).getLink());
                Log.i(TAG, "ID is: " + newsId);
            } else {
                holder.setLink();
            }
        } catch (NullPointerException ignored) {
            holder.setLink();
        }

        try {
            //getters
            firebaseFirestore.collection("News").document(newsId).collection("Likes").document(current_user).addSnapshotListener((documentSnapshot, e) -> {

                assert documentSnapshot != null;
                if (documentSnapshot.exists()) {

                    holder.likeBtn.setSelected(true);

                } else {

                    holder.likeBtn.setSelected(false);

                }

            });

            firebaseFirestore.collection("News").document(newsId).collection("Dislikes").document(current_user).addSnapshotListener((documentSnapshot, e) -> {

                assert documentSnapshot != null;
                if (documentSnapshot.exists()) {

                    holder.dislikeBtn.setSelected(true);

                } else {

                    holder.dislikeBtn.setSelected(false);
                }

            });

            //counters
            firebaseFirestore.collection("News").document(newsId).collection("Likes").addSnapshotListener((documentSnapshots, e) -> {

                assert documentSnapshots != null;
                if (!documentSnapshots.isEmpty()) {

                    int count = documentSnapshots.size();

                    holder.updateCount(true, count);

                } else {

                    holder.updateCount(true, 0);

                }

            });

            firebaseFirestore.collection("News").document(newsId).collection("Dislikes").addSnapshotListener((documentSnapshots, e) -> {

                assert documentSnapshots != null;
                if (!documentSnapshots.isEmpty()) {

                    int count = documentSnapshots.size();

                    holder.updateCount(false, count);

                } else {

                    holder.updateCount(false, 0);

                }

            });

            firebaseFirestore.collection("News").document(newsId).collection("Comments").addSnapshotListener((documentSnapshots, e) -> {

                assert documentSnapshots != null;
                if (!documentSnapshots.isEmpty()) {

                    int count = documentSnapshots.size();

                    holder.updateComments(count);

                } else {

                    holder.updateComments(0);

                }

            });

            //setters
            holder.likeBtn.setOnClickListener(v -> {

                firebaseFirestore.collection("News")
                        .document(newsId)
                        .collection("Likes")
                        .document(current_user)
                        .get().addOnCompleteListener(task -> {


                    if (!holder.likeBtn.isSelected()) {

                        holder.likeBtn.setSelected(false);
                        firebaseFirestore.collection("News").document(newsId).collection("Likes").document(current_user).delete();

                    } else {

                        holder.dislikeBtn.setSelected(false);
                        holder.likeBtn.setSelected(true);
                        firebaseFirestore.collection("News").document(newsId).collection("Dislikes").document(current_user).delete();

                        HashMap<String, Object> likes = new HashMap<>();
                        likes.put("timetamps", FieldValue.serverTimestamp());
                        firebaseFirestore.collection("News").document(newsId).collection("Likes").document(current_user).set(likes);

                    }


                });

                if (holder.likeBtn.isSelected()) {
                    holder.likeBtn.setSelected(false);
                } else {
                    holder.likeBtn.setSelected(true);
                    holder.likeBtn.likeAnimation(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                        }
                    });
                }

            });

            holder.dislikeBtn.setOnClickListener(v -> {

                firebaseFirestore.collection("News")
                        .document(newsId)
                        .collection("Dislikes")
                        .document(current_user)
                        .get().addOnCompleteListener(task -> {


                    if (!holder.dislikeBtn.isSelected()) {

                        Log.i(TAG, "Dislike is " + holder.dislikeBtn.isSelected());

                        holder.dislikeBtn.setSelected(false);
                        firebaseFirestore.collection("News").document(newsId).collection("Dislikes").document(current_user).delete();

                    } else {

                        Log.i(TAG, "Dislike is " + holder.dislikeBtn.isSelected());

                        holder.likeBtn.setSelected(false);
                        holder.dislikeBtn.setSelected(true);

                        firebaseFirestore.collection("News").document(newsId).collection("Likes").document(current_user).delete();
                        HashMap<String, Object> likes = new HashMap<>();
                        likes.put("timetamps", FieldValue.serverTimestamp());

                        firebaseFirestore.collection("News").document(newsId).collection("Dislikes").document(current_user).set(likes);

                    }


                });

                if (holder.dislikeBtn.isSelected()) {
                    holder.dislikeBtn.setSelected(false);
                } else {
                    holder.dislikeBtn.setSelected(true);
                    holder.dislikeBtn.likeAnimation(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                        }
                    });
                }

            });

            try {
                String username_data = news_list.get(position).getUser_id();
                firebaseFirestore.collection("Users").document(username_data).get().addOnCompleteListener(task -> {

                    if (task.isSuccessful()) {

                        String userName = task.getResult().getString("name");
                        String userRank = task.getResult().getString("rank");
                        String userImage = task.getResult().getString("image");

                        holder.setUser(userName, userRank, userImage, username_data);

                    }
                });

                holder.newsView.setOnClickListener(v -> {
                    mNewsOpenFull.openFull(holder.image.getDrawingCache(), username_data, text_data, newsId);
                });

            } catch (NullPointerException e) {
            }

        } catch (NullPointerException ignored) {

            Log.i(TAG, ignored.getMessage());

        }

    }

    @Override
    public int getItemCount() {
        return news_list.size();
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager
                cm = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private View mView;
        private View newsView;

        private CircleImageView userImage;

        private ImageView link;
        private ImageView image;

        private TextView username;
        private TextView userRank;
        private TextView text;
        private TextView counterLikes;
        private TextView counterDislikes;
        private TextView counterComments;

        private SmallBangView likeBtn;
        private SmallBangView dislikeBtn;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;

            likeBtn = mView.findViewById(R.id.item_news_like_heart);
            dislikeBtn = mView.findViewById(R.id.item_news_dislike_heart);
            newsView = mView.findViewById(R.id.news_item_view);
        }

        public void setText(String str) {
            text = mView.findViewById(R.id.item_news_text);
            text.setVisibility(View.VISIBLE);
            text.setText(str);
        }

        public void setCounterLikes(String str) {
            counterLikes = mView.findViewById(R.id.item_news_counter_likes);
            counterLikes.setText(str);
        }

        public void setCounterDislikes(String likes) {
            counterDislikes = mView.findViewById(R.id.item_news_counter_dislikes);
            counterDislikes.setText(likes);
        }

        public void setCounterComments(String dislikes) {
            counterComments = mView.findViewById(R.id.item_news_counter_comments);
            counterComments.setText(dislikes);
        }

        public void setImage(String downloadUri) {
            image = mView.findViewById(R.id.item_news_image);
            image.setVisibility(View.VISIBLE);
            Glide.with(context.getApplicationContext())
                    .load(downloadUri)
                    .listener(new RequestListener<Drawable>() {
                                  @Override
                                  public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {

                                      image.setVisibility(View.GONE);

                                      return false;
                                  }

                                  @Override
                                  public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                                      return false;
                                  }
                              }
                    )
                    .into(image);
            image.setDrawingCacheEnabled(true);

        }

        public void setUser(String name, String rank, String image, String userForProfileInfo) {

            username = mView.findViewById(R.id.item_news_username);
            username.setText(name);
            username.setOnClickListener(v -> {
                mOpenProfileInfo.openProfile(userForProfileInfo);
            });

            userRank = mView.findViewById(R.id.item_news_rank);
            userRank.setText(rank);
            userRank.setOnClickListener(v -> {
                mOpenProfileInfo.openProfile(userForProfileInfo);
            });

            userImage = mView.findViewById(R.id.item_news_user_image);
            userImage.setOnClickListener(v -> {
                mOpenProfileInfo.openProfile(userForProfileInfo);
            });

            //без placeholderOption не появляются мигающие круги при прокрутки, но не появляются некоторые картинки
            RequestOptions placeholderOption = new RequestOptions();
            placeholderOption.placeholder(R.drawable.profile_placeholder);
            Glide.with(context.getApplicationContext())
                    .load(image)
                    /*.apply(placeholderOption)*/
                    .into(userImage);

        }

        void updateCount(boolean like, int count) {

            if (like) {

                setCounterLikes(String.valueOf(count));

            } else {

                setCounterDislikes(String.valueOf(count));

            }
        }

        void updateComments(int count) {

            setCounterComments(String.valueOf(count));

        }

        void setLink(String browserLink) {
            link = mView.findViewById(R.id.item_news_user_link);
            link.setVisibility(View.VISIBLE);
            link.setOnClickListener(v -> {

                mNewsOpenWeb.openWeb(browserLink);

            });

        }

        void setLink() {

            link = mView.findViewById(R.id.item_news_user_link);
            link.setVisibility(View.GONE);

        }

    }

}
