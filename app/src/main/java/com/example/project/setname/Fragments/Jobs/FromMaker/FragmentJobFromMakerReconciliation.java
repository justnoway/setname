package com.example.project.setname.Fragments.Jobs.FromMaker;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.project.setname.Chat.FragmentChat;
import com.example.project.setname.Fragments.AdditionalViews.FragmentEmptyList;
import com.example.project.setname.Fragments.Jobs.FullJob.FragmentJobFullFromReconciliation;
import com.example.project.setname.Fragments.Jobs.Reconciliations.AdapterJobReconciliation;
import com.example.project.setname.Fragments.Jobs.Reconciliations.ModelJobReconciliation;
import com.example.project.setname.Interfaces.JobOpenChat;
import com.example.project.setname.Interfaces.GetFragment;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

public class FragmentJobFromMakerReconciliation extends android.support.v4.app.Fragment implements JobOpenChat, GetFragment {

    private RecyclerView jobs_list_view;
    private List<ModelJobReconciliation> jobs_list;

    private AdapterJobReconciliation adapterJobs;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    private FragmentJobFullFromReconciliation fragmentJobFullFromReconciliation;

    private String current_user;

    private DocumentSnapshot lastVisible;

    private AtomicBoolean emptyIsLoaded = new AtomicBoolean(false);

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_from_maker_reconciliation, container, false);
        fragmentJobFullFromReconciliation = new FragmentJobFullFromReconciliation();

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        current_user = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        jobs_list = new ArrayList<>();
        jobs_list_view = view.findViewById(R.id.job_from_maker_r_recyclerview);

        adapterJobs = new AdapterJobReconciliation(jobs_list, this, this);

        jobs_list_view.setLayoutManager(new LinearLayoutManager(container.getContext()));
        jobs_list_view.setAdapter(adapterJobs);

        if (firebaseAuth.getCurrentUser() != null) {

            loadData();
        }


        return view;
    }

    public void refreshData() {

        jobs_list.clear();
        adapterJobs.notifyDataSetChanged();
        loadData();

    }

    private void loadData() {

        firebaseFirestore.collection("Users").document(current_user).collection("Reconciliations(Maker)").addSnapshotListener((documentSnapshots, e) -> {

            assert documentSnapshots != null;
            if (documentSnapshots.size() > 0) {

                if (emptyIsLoaded.get()) {

                    assert getFragmentManager() != null;
                    Objects.requireNonNull(getFragmentManager().findFragmentByTag("FRAGMENT_EMPTY_LIST_FROM_MAKER_REC").getView()).setVisibility(View.GONE);

                }

                if (lastVisible != documentSnapshots.getDocuments().get(documentSnapshots.size() - 1)) {

                    for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {

                        if (doc.getType() == DocumentChange.Type.ADDED) {

                            if (lastVisible != doc.getDocument()) {

                                String ID = doc.getDocument().getId();

                                ModelJobReconciliation modelJobReconciliation = doc.getDocument().toObject(ModelJobReconciliation.class).withId(ID);
                                jobs_list.add(modelJobReconciliation);
                                adapterJobs.notifyDataSetChanged();

                                lastVisible = doc.getDocument();

                            }

                        }
                    }

                    lastVisible = documentSnapshots.getDocuments().get(documentSnapshots.size() - 1);
                }
            } else {
                setEmptyList();
            }
        });

    }

    void setEmptyList() {

        if (!emptyIsLoaded.get()) {

            FragmentEmptyList fragmentEmptyList = new FragmentEmptyList();
            assert getFragmentManager() != null;
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment_from_maker_reconciliation_cl, fragmentEmptyList, "FRAGMENT_EMPTY_LIST_FROM_MAKER_REC");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

            emptyIsLoaded.set(true);

        }

    }

    @Override
    public void openChat(String companion) {

    }

    @Override
    public void openChat(String companion, String id, String fragmentTag) {
        assert getFragmentManager() != null;
        Bundle bundle = new Bundle();
        bundle.putString("companion", companion);
        bundle.putString("id", id);
        bundle.putString("fragmentTag", fragmentTag);
        fragmentJobFullFromReconciliation.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left);
        if (fragmentJobFullFromReconciliation.isAdded()) {
            fragmentTransaction.show(fragmentJobFullFromReconciliation);
        } else {
            fragmentTransaction.add(R.id.main_container, fragmentJobFullFromReconciliation, "OPEN_FRAGMENT_FULL_FROM_RECONCILIATION");
        }
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public String getCustomFragment() {
        return "FRAGMENT_FROM_MAKER_RECONCILIATIONS";
    }
}