package com.example.project.setname.Fragments.News.NewsFull.Comments;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.project.setname.Interfaces.OpenProfileInfo;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder>{

    private List<ModelComments> news_list;
    public Context context;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    private OpenProfileInfo mOpenProfileInfo;

    public CommentAdapter(List<ModelComments> news_list,
                          OpenProfileInfo openProfileInfo) {
        this.news_list = news_list;
        this.mOpenProfileInfo = openProfileInfo;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news_comment, parent, false);
        context = parent.getContext();
        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        String text_data = news_list.get(position).getMessage();
        holder.setText(text_data);

        String username_data = news_list.get(position).getUser_id();
        firebaseFirestore.collection("Users").document(username_data).get().addOnCompleteListener(task -> {

            if(task.isSuccessful()){

                String userName = task.getResult().getString("name");
                String userRank = task.getResult().getString("rank");
                String userImage = task.getResult().getString("image");

                holder.setUser(userName, userRank, userImage, username_data);

            }
        });

    }

    @Override
    public int getItemCount() {
        return news_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private View mView;

        private CircleImageView userImage;

        private TextView username;
        private TextView userRank;
        private TextView text;


        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setText(String str){
            text = mView.findViewById(R.id.item_news_comment_text);
            text.setText(str);
        }

        public void setUser(String name, String rank, String image, String userForProfileInfo){

            username = mView.findViewById(R.id.item_news_comment_username);
            username.setText(name);
            username.setOnClickListener(v->{
                mOpenProfileInfo.openProfile(userForProfileInfo);
            });

            userRank = mView.findViewById(R.id.item_news_comment_rank);
            userRank.setText(rank);
            userRank.setOnClickListener(v->{
                mOpenProfileInfo.openProfile(userForProfileInfo);
            });

            userImage = mView.findViewById(R.id.item_news_comment_user_image);
            userImage.setOnClickListener(v->{
                mOpenProfileInfo.openProfile(userForProfileInfo);
            });
            RequestOptions placeholderOption = new RequestOptions();
            placeholderOption.placeholder(R.drawable.profile_placeholder);
            Glide.with(context).applyDefaultRequestOptions(placeholderOption).load(image).into(userImage);

        }
    }
}
