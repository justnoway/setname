package com.example.project.setname.Fragments.Events.AvailableEvents.AvailableEventsOpenFull;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.example.project.setname.Interfaces.EventsOpenFull;
import com.example.project.setname.R;
import com.google.android.gms.maps.MapView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;

import me.yokeyword.fragmentation_swipeback.SwipeBackFragment;

public class FragmentAvailableEventsOpenedFull extends SwipeBackFragment {

    private static final String TAG = "FragmentAEventsOF";

    public interface DeleteListenerOpenedFull {
        void getStateOpenedFull(long positionInList, boolean thisState);
    }

    DeleteListenerOpenedFull mDeleteListener;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;
    private String currentUser;

    private View view;

    private View getTicket;

    private ImageView image;

    private SwipeRevealLayout swipeRevealLayout;

    private TextView languages;
    private TextView type;
    private TextView direction;
    private TextView price;
    private TextView date;
    private TextView geoposition;

    private TextView description;

    private String id;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mDeleteListener = (DeleteListenerOpenedFull) getTargetFragment();

        } catch (ClassCastException e) {
            Log.e(TAG, "onAttach: ClassCastException: " + e.getMessage());
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        setFirebase();

        view = inflater.inflate(R.layout.item_events_opened_full, container, false);

        setViews();

        setData();

        assert getArguments() != null;
        if (Objects.equals(getArguments().getString("fragment"), "FRAGMENT_AVAILABLE_EVENTS")) {

            setClickListeners();

        }else if (Objects.equals(getArguments().getString("fragment"), "FRAGMENT_PURCHASED")) {

            swipeRevealLayout.setLockDrag(true);

        }

        return attachToSwipeBack(view);
    }

    private void setClickListeners() {

        final int[] count = {0};

        getTicket.setOnClickListener(v->{

            if (count[0] !=3) {

                Toast.makeText(getContext(), "Click " + (3 - count[0]) + " times", Toast.LENGTH_SHORT).show();

                count[0]++;

            }
            else {

                String uuid = UUID.randomUUID().toString();


                Map<String, Object> map = new HashMap<>();
                map.put("timestamp", FieldValue.serverTimestamp());
                map.put("key", uuid);

                firebaseFirestore.collection("Users")
                        .document(currentUser)
                        .collection("MyTickets")
                        .document(id)
                        .set(map).addOnCompleteListener(task -> {

                            if (task.isComplete()){

                                notShowAgain();

                                Toast.makeText(getContext(), "Complete", Toast.LENGTH_SHORT).show();

                                mDeleteListener.getStateOpenedFull(getArguments().getLong("position"), true);

                                getFragmentManager().popBackStack();

                            }else {

                                Toast.makeText(getContext(), "Not this time", Toast.LENGTH_SHORT).show();
                                
                            }

                });

            }

        });

    }

    private void notShowAgain(){

        Map<String, Object> map1 = new HashMap<>();

        map1.put("timestamp", FieldValue.serverTimestamp());

        firebaseFirestore.collection("Users")
                .document(currentUser)
                .collection("NeverShow(Events)")
                .document(id)
                .set(map1).addOnCompleteListener(task -> {



        });

    }

    private void setData() {

        assert getArguments() != null;
        id = getArguments().getString("id");

        firebaseFirestore.collection("Events")
                .document(id).get().addOnCompleteListener(task -> {

            if (task.isComplete()){

                setImage(task.getResult().getString("image_url"));
                setLanguages(task.getResult().getString("languages"));
                setDirection(task.getResult().getString("direction"));
                setPrice(task.getResult().getString("price"));
                setType(task.getResult().getString("type"));
                setDescription(task.getResult().getString("description"));

                long millisecondStart = Objects.requireNonNull(task.getResult().getDate("timeStart")).getTime();
                long millisecondEnd = Objects.requireNonNull(task.getResult().getDate("timeEnd")).getTime();
                String dateStringStart = DateFormat.format("MMMM dd", new Date(millisecondStart)).toString();
                String dateStringEnd = DateFormat.format("MMMM dd", new Date(millisecondEnd)).toString();
                setDate(dateStringStart + " - " + dateStringEnd);

            }
        });

    }

    private void setFirebase() {
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        currentUser = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
    }

    private void setViews() {

        swipeRevealLayout = view.findViewById(R.id.fragment_events_opened_full_sl);

        getTicket = view.findViewById(R.id.item_events_opened_full_get_ticket_button);

        image = view.findViewById(R.id.item_events_opened_full_image);

        languages = view.findViewById(R.id.item_events_opened_full_languages);
        type = view.findViewById(R.id.item_events_opened_full_type);
        direction = view.findViewById(R.id.item_events_opened_full_direction);
        price = view.findViewById(R.id.item_events_opened_full_price);
        date = view.findViewById(R.id.item_events_opened_full_date);
        geoposition = view.findViewById(R.id.item_events_opened_full_geoposition);

        description = view.findViewById(R.id.item_events_opened_full_description);

    }

    void setImage(String url){

        Glide.with(getContext())
                .load(url)
                /*.listener(new RequestListener<Drawable>() {
                              @Override
                              public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {

                                  return false;
                              }

                              @Override
                              public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                                  return false;
                              }
                          }
                )*/
                .into(image);

    }

    void setLanguages(String str){

        languages.setText(str);

    }

    void setType(String str){

        type.setText(str);

    }

    void setDirection(String str){

        direction.setText(str);

    }

    void setPrice(String str){

        price.setText(str);

    }

    void setDate(String str){

        date.setText(str);

    }

    void setGeoposition(String str){

        geoposition.setText(str);

    }

    void setDescription(String str){

        description.setText(str);

    }

}
