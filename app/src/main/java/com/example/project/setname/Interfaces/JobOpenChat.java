package com.example.project.setname.Interfaces;

public interface JobOpenChat {
    void openChat(String companion);
    void openChat(String companion, String id, String fragment_tag);
}
