package com.example.project.setname.Fragments.Jobs.Reconciliations;

import java.util.Date;

public class ModelJobReconciliation extends ModelJobReconciliationId {

    String maker_id, employer_id, language, mustToDo, name;
    Date deadline;

    public ModelJobReconciliation() {
    }

    public ModelJobReconciliation(String employer_id, String language, String mustToDo, String name, Date deadline) {
        this.employer_id = employer_id;
        this.language = language;
        this.mustToDo = mustToDo;
        this.name = name;
        this.deadline = deadline;
    }

    public ModelJobReconciliation(String employer_id, String language, String mustToDo, Date deadline) {
        this.employer_id = employer_id;
        this.language = language;
        this.mustToDo = mustToDo;
        this.deadline = deadline;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMustToDo() {
        return mustToDo;
    }

    public void setMustToDo(String mustToDo) {
        this.mustToDo = mustToDo;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMaker_id() {
        return maker_id;
    }

    public void setMaker_id(String maker_id) {
        this.maker_id = maker_id;
    }

    public String getEmployer_id() {
        return employer_id;
    }

    public void setEmployer_id(String employer_id) {
        this.employer_id = employer_id;
    }
}
