package com.example.project.setname.Chat;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.project.setname.Interfaces.OpenProfileInfo;
import com.example.project.setname.ProfileInfo.FragmentProfileInfo;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;


import com.google.firebase.firestore.Query;

public class FragmentChat extends Fragment implements OpenProfileInfo {

    //for RV
    private RecyclerView chat_list_view;
    private List<ModelChat> chat_list;
    private AdapterChat adapterChat;

    //services
    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;
    private String currentUser;

    //vies
    private View view;
    private EditText inputField;
    private ImageView sendMessage;

    //vars
    private String companion;
    private int count = 0;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_chat, container, false);

        setFirebase();

        setDataForArguments();

        setViews();

        setRV();

        assert companion != null;

        if (firebaseAuth.getCurrentUser() != null) {

            setListeners();

            getMessages();
        }

        inputField.addTextChangedListener(loginTextWatcher);

        return view;
    }

    private void getMessages() {

        firebaseFirestore.collection("Users")
                .document(currentUser)
                .collection("InteractionWithUsers")
                .document(companion)
                .collection("Messages")
                .orderBy("timestamp", Query.Direction.ASCENDING)
                .addSnapshotListener((documentSnapshots, e) -> {

                    for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {

                        String currentId = doc.getDocument().getId();

                        if (chat_list.size() > 0) {
                            ModelChat modelChat = doc.getDocument().toObject(ModelChat.class).withId(currentId, chat_list.get(chat_list.size() - 1).getSender());
                            if (!chat_list.get(chat_list.size() - 1).ModelChatId.equals(currentId)) {
                                chat_list.add(modelChat);
                            }
                        } else {
                            ModelChat modelChat = doc.getDocument().toObject(ModelChat.class).withId(currentId);
                            chat_list.add(modelChat);
                        }

                        adapterChat.notifyDataSetChanged();


                    }
                    if (chat_list.size() > 0) {
                        chat_list_view.scrollToPosition(chat_list.size() - 1);
                    }

                });

    }

    private void setListeners() {

        sendMessage.setOnClickListener(v -> {

            sendMessage.setClickable(false);

            if (!inputField.getText().toString().equals("")) {

                HashMap<String, Object> map = new HashMap<>();
                map.put("text", inputField.getText().toString());
                map.put("sender", currentUser);
                map.put("timestamp", FieldValue.serverTimestamp());

                assert companion != null;
                firebaseFirestore.collection("Users")
                        .document(currentUser)
                        .collection("InteractionWithUsers")
                        .document(companion)
                        .collection("Messages")
                        .add(map).addOnSuccessListener(task -> {

                    firebaseFirestore.collection("Users")
                            .document(companion)
                            .collection("InteractionWithUsers")
                            .document(currentUser)
                            .collection("Messages")
                            .add(map).addOnSuccessListener(task1 -> {

                    });

                });

                inputField.setText("");

            }

            sendMessage.setClickable(true);

        });

    }

    private void setRV() {

        chat_list = new ArrayList<>();

        adapterChat = new AdapterChat(chat_list, this);

        chat_list_view.setLayoutManager(new LinearLayoutManager(getContext()));
        chat_list_view.setAdapter(adapterChat);

    }

    private void setViews() {

        inputField = view.findViewById(R.id.fragment_chat_form_for_message);
        sendMessage = view.findViewById(R.id.fragment_chat_send);
        chat_list_view = view.findViewById(R.id.fragment_chat_rv_chat);

    }

    private void setDataForArguments() {

        assert getArguments() != null;
        companion = getArguments().getString("companion");
        currentUser = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

    }

    private void setFirebase() {

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

    }

    private TextWatcher loginTextWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            String fieldInput = inputField.getText().toString().trim();

            if (!fieldInput.isEmpty()) {

                sendMessage.setImageResource(R.drawable.ic_send);

            } else {

                sendMessage.setImageResource(R.drawable.ic_send_disable);

            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    public void openProfile(String user) {
        FragmentProfileInfo fragmentProfileInfo = new FragmentProfileInfo();
        assert getFragmentManager() != null;
        Bundle bundle = new Bundle();
        bundle.putString("user", user);
        fragmentProfileInfo.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left);
        fragmentTransaction.add(R.id.main_container, fragmentProfileInfo, "FRAGMENT_PROFILE_INFO");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
