package com.example.project.setname.Fragments.Events.AvailableEvents;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.example.project.setname.Interfaces.EventsOpenFull;
import com.example.project.setname.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import java.util.Date;
import java.util.List;


public class AdapterAvailableEvents extends RecyclerView.Adapter<AdapterAvailableEvents.ViewHolder> {

    private static final String TAG = "AdapterJobAvailable";

    /*private GoogleMap mGoogleMap;*/

    private List<ModelEventAvailable> events_list;
    public Context context;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    private EventsOpenFull mEventsOpenFull;

    public AdapterAvailableEvents(List<ModelEventAvailable> events_list,
                                  EventsOpenFull eventsOpenFull) {
        this.events_list = events_list;
        this.mEventsOpenFull = eventsOpenFull;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_events_part, parent, false);
        context = parent.getContext();
        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        String id = events_list.get(position).ModelEventAvailableId;

        String price_data = events_list.get(position).getPrice();
        String headline_data = events_list.get(position).getHeadline();
        String desc_data = events_list.get(position).getDescription();
        String imageUrl_data = events_list.get(position).getImage_url();

        holder.setPrice(price_data);
        holder.setHeadline(headline_data);
        if (desc_data.length()>190) {
            holder.setDescription(desc_data.substring(0, 190) + "...");
        }else {
            holder.setDescription(desc_data);
        }
        holder.setImage(imageUrl_data);

        long millisecondStart = events_list.get(position).getTimeStart().getTime();
        long millisecondEnd = events_list.get(position).getTimeEnd().getTime();
        String dateStringStart = DateFormat.format("MMMM dd", new Date(millisecondStart)).toString();
        String dateStringEnd = DateFormat.format("MMMM dd", new Date(millisecondEnd)).toString();
        holder.setDate(dateStringStart + " - " + dateStringEnd);

        holder.constraintLayout.setOnClickListener(v->{

            mEventsOpenFull.openFull("FRAGMENT_AVAILABLE_EVENTS", id, position);

        });

    }

    @Override
    public int getItemCount() {
        return events_list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private View mView;

        private ConstraintLayout constraintLayout;

        private ImageView image;

        private TextView price;
        private TextView headline;
        private TextView type;
        private TextView date;

        ViewHolder(View itemView) {

            super(itemView);
            mView = itemView;

            constraintLayout = mView.findViewById(R.id.item_events_part_cl);

        }

        void setPrice(String str){

            price = mView.findViewById(R.id.item_events_part_price_tv);
            price.setText(str);

        }

        void setHeadline(String str){

            headline = mView.findViewById(R.id.item_events_part_headline);
            headline.setText(str);

        }

        void setDescription(String str){

            type = mView.findViewById(R.id.item_events_part_type);
            type.setText(str);

        }

        void setDate(String str){

            date = mView.findViewById(R.id.item_events_part_date);
            date.setText(str);

        }

        void setImage(String downloadUri){

            image = mView.findViewById(R.id.imageView3);
            Glide.with(context.getApplicationContext())
                    .load(downloadUri)
                    /*.listener(new RequestListener<Drawable>() {
                                  @Override
                                  public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {

                                      return false;
                                  }

                                  @Override
                                  public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                                      return false;
                                  }
                              }
                    )*/
                    .into(image);

        }
    }
}
